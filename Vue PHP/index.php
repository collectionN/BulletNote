<?php
// 检查是否存在名为“jwt”的cookie
if (isset($_COOKIE['jwt_token'])) {
	// 如果存在，设置href属性为首页
	$href = './manage/';
} else {
	// 如果不存在，设置href属性为注册页面
	$href = 'register.php';
}
?>
<!DOCTYPE html>
<html lang="zh-CN">

<head>
	<title>弹幕笔记</title>
	<meta charset="utf-8">
	<meta name="renderer" content="webkit">
	<meta name="keywords" content="">
	<meta name="description" content="弹幕你的笔记">
	<meta name="viewport" content="width=device-width, viewport-fit=cover, initial-scale=1, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
	<link rel="stylesheet" href="css/index.css">
</head>

<body>
	<div id="site-header">
		<div class="site-nav">
			<div class="site-nav-left">
				<a href="index.php"> <img src="https://pic.imgdb.cn/item/654dbe5ac458853aef3328a9.png" width=" 50px" height="50px" /></a>
			</div>
			<a class="site-nav-item" href="#"> 首页 </a>
			<a class="site-nav-item" href="https://support.qq.com/products/427645/faqs/126272"> 帮助 </a>
			<a class="site-nav-item" href="http://pan.nrhs.eu.org/K%E7%9A%84%E8%89%BA%E6%9C%AF/Resource/DeskBullet"> 产品 </a>
			<a class="site-nav-item" href="https://blog.nrhs.eu.org/2022/11/12/BulletNote_open-source/"> 开源 </a>
			<div id="site-nav-app">
				<div class="btn-grad-app"><a href="./app">🎃下载中心</a></div>
				<div class="btn-grad"><a href="<?php echo htmlspecialchars($href); ?>">✨个人中心</a></div>
			</div>
		</div>
	</div>
	<div class="index-banner">
		<div class="banner">
			<div class="start-bg"></div>
			<div class="start">
				<h1 class="slogan">弹幕笔记</h1>
				<h1 class="slogan slogan-desc"><span>Ctrl+CCC =></span><span id="box">弹幕你的笔记</span></h1>
				<span id="login-btn" class="button primary size-xl"><a href="register.php" style="color: #fff;">快速开始</a></span>
				<div class="product-desc">
					PC全局划词，按下CTRL+3次C<br>自动翻译，手机同步，自动弹幕<br>🎉耳目一新的碎片化复习新方式\^o^/🎉
				</div>
			</div>
		</div>
	</div>
	<div class="index-blocks">
		<div class="product-notice">
			<div class="notice-content">
				<div class="content-left">
					<h2><span>全局划词</span>&免费翻译</h2>
					<div class="content-text">
						在PC上开机自启，享受快捷人生<br>全局划词，支持中文与英文自动识别，并转换翻译。同时支持单词模式与长句翻译
					</div>
				</div>
				<div class="content-right">
					<img src="https://pic.imgdb.cn/item/663e40b20ea9cb140379519c.png" alt="">
				</div>
			</div>
		</div>
		<div class="feature-block feature-preview">
			<div class="preview-content">
				<div class="feature-desc">
					<h2>自动同步&屏幕弹幕</h2>
					<div class="desc">
						<div>
							<b>同步笔记</b> 每次翻译与笔记，多端共享，即时同步
						</div>
						<div>
							<b>屏幕弹幕</b> 开启弹幕，随时随地，弹幕笔记~Biu！
						</div>
						<div>
							<b>便捷管理</b> 随时随地，管理我的笔记
						</div>
					</div>
				</div>
				<div class="preview-img-wrapper">
					<img class="preview-web" src="https://pic.imgdb.cn/item/663d88250ea9cb1403fbf37a.png" style="border-radius: 10px;height: 108%;" alt="">
				</div>
			</div>
		</div>

		<div class="feature-block main-block">
			<div class="feature-shot">
				<div class="shot-wrapper">
					<img src="https://pic.imgdb.cn/item/663e40b10ea9cb1403795024.jpg" alt="">
					<div class="float-card">
						<div class="card-title">伴侣设备
						</div>
						<div class="card-content">
							时间笔记两不误，墨水省电不伤眼
						</div>
					</div>
				</div>
			</div>
			<div class="feature-desc">
				<h2>时间摆件，笔记提醒</h2>
				<div>
					小巧优雅、墨水屏幕，超长续航，网络连接，便捷方便<br>支持天气时钟，倒计时，翻译同步显示，电子书......
				</div>
			</div>
		</div>

		<div class="feature-block secure-block">
			<div class="feature-desc">
				<h2>弹幕笔记</h2>
				<div>
					完全免费的笔记，事件提醒新方式。我们在尝试，更多新的可能
				</div>
			</div>
			<div class="secure-details">
				<section>
					<div class="secure-ico secure1">
						<img src="https://pic.imgdb.cn/item/654dbf53c458853aef35eae2.png" width="40px" height="40px" />
					</div>
					<div class="secure-text">
						小内存，低延迟，服务小帮手
					</div>
				</section>
				<section>
					<div class="secure-ico secure2">
						<img src="https://pic.imgdb.cn/item/654dbf67c458853aef36230b.png" width="40px" height="40px">
					</div>
					<div class="secure-text">
						No敏感数据，黑客，他拿去没用
					</div>
				</section>
				<section>
					<div class="secure-ico secure3">
						<img src="https://pic.imgdb.cn/item/654dbf80c458853aef366eee.png" width="40px" height="40px">
					</div>
					<div class="secure-text">
						多端同步共享，少一步，更优雅
					</div>
				</section>
			</div>
		</div>
	</div>

	<div id="site-footer" style="border-top:1px solid #eeeff0;background:#fff;color:#303032;padding:50px 10px;text-align:center;font-size:14px;">
		Design In 2024-5-10
		<div style="padding-top:5px;">
			<a style="margin-left:10px;" target="_blank">Hua</a>
		</div>
		<div style="padding-top:10px;display:flex;justify-content:space-between;text-align:left;line-height:28px;max-width:320px;margin:15px auto;">
			<div>
				<div>
					<a href="https://oshwhub.com/lxu0423/ESP32-duo-gong-neng-mo-shui-ping">关于伴侣终端</a>
				</div>

				<div>
					<a href="http://pan.nrhs.eu.org/K%E7%9A%84%E8%89%BA%E6%9C%AF/Resource/DeskBullet">开源</a>
				</div>
				<div>
					<a href="https://support.qq.com/products/427645/faqs/126272">社区</a>
				</div>
			</div>
			<div>
				<div>
					<div>
						<a href="https://blog.nrhs.eu.org/2022/11/12/BulletNote_open-source/">帮助中心</a>
					</div>
				</div>
				<div>
					<a href="javascript:;">隐私政策</a>
				</div>
				<div>
					<a href="javascript:;">用户协议</a>
				</div>

			</div>

		</div>
	</div>
</body>
<script>
	const arr = ['Translate', 'Record', 'Sync', 'Biu your Notes']; //显示的文本
	const dom = document.getElementById('box')
	let j = 0; //从数组第一个开始展示
	// 递归函数
	const func = (j) => {
		if (j < arr.length) { // 当达到数组长度时，就从头开始继续
			const item = arr[j]
			const itemLen = item === '弹幕笔记' ? 8 : item.length; // 汉字是占两个ch
			dom.innerHTML = item; // 显示文字
			for (var i = 0, len = itemLen; i < len; i++) { // 添加文本效果
				var textLen = dom.textContent.length,
					s = dom.style;
				s.animationTimingFunction = "steps(" + textLen + "),steps(1)";; //动态设置steps
				s.animationName = `typing${itemLen}`; //文本长度不同，展示的宽度就不同，所以需要动态设置
				s.animationDuration = `${itemLen}s, 0.5s`; //这儿设置速度
			}
			setTimeout(() => {
				func(j + 1)
			}, itemLen * 1000) //这儿和上面的animationDuration速度一致，只不过这儿是毫秒，所以要乘以1000
		} else {
			func(0); //就从头开始继续
		}
	}
	func(j);
</script>

</html>