<?php
require_once '../vendor/autoload.php';  // 引入Composer自动加载文件
use Firebase\JWT\JWT;  // 引入JWT类
use Firebase\JWT\Key;

class JWTService
{
    private const JWT_SECRET_KEY = '2983717613';  // 替换为您的实际密钥

    public function generateToken(array $payload): string
    {
        $issuedAt   = time();
        $expiration = $issuedAt + 72000;  // 设置过期时间为1小时（可自定义）

        $payload = array_merge($payload, [
            'iat' => $issuedAt,  // 发布时间
            'exp' => $expiration,  // 过期时间
        ]);

        return JWT::encode($payload, self::JWT_SECRET_KEY, 'HS256');  // 使用HS256算法加密
    }

    public function validateToken(string $token): array
    {
        try {
            $decoded = JWT::decode($token, new Key(self::JWT_SECRET_KEY, 'HS256'));
            return (array) $decoded;
        } catch (Exception $e) {
            // 如果令牌过期或者校验失败，返回 false
            return [];
        }
    }
}

# SHA256加密
function sha256($str)
{
    return hash('sha256', $str);
}

# 6位KEY产生
function get_key($length = 8)
{
    $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ!@$&';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

class MySQL
{
    // 本地测试地址
    private $servername = "10.157.3.87";
    private $username = "root";
    private $password = "pie2983";
    private $dbname = "test";

    # 连接Mysql数据库
    function connect()
    {
        $conn = new mysqli($this->servername, $this->username, $this->password, $this->dbname);
        if ($conn->connect_error) {
            die("连接失败： " . $conn->connect_error);
        }
        return $conn;
    }

    # 查询用户是否存在
    public function is_user($key)
    {
        $conn = $this->connect();
        // 执行查询的代码
        $sql = "select ID from Users where user_key='$key';";
        $result = $conn->query($sql);
        if ($result) {
            return true;
        } else {
            echo "失败： " . $conn->error;
            return false;
        }
        $conn->close();
    }

    #用户登录逻辑
    public function login($account, $password)
    {
        $conn = $this->connect();
        // 执行查询的代码
        $sql = "select ID from Users where links='$account' and user_key='$password';";
        $result = $conn->query($sql);
        if ($result->num_rows > 0) {
            // 获取第一条记录
            $row = $result->fetch_assoc();
            // 输出记录
            return $row["ID"];
        } else {
            return false;
        }
        $conn->close();
    }

    # 用户插入笔记逻辑
    # 需要判别用户正确且笔记本属于用户，否则返回false
    public function verify_user_key($account, $password, $key)
    {
        $conn = $this->connect();
        // 执行查询的代码
        $sql = "select U.ID from Users U JOIN listNote l on U.ID=l.user_id where links='$account' and user_key='$password' and l.note_key='$key';";
        $result = $conn->query($sql);
        if ($result->num_rows > 0) {
            // 获取第一条记录
            $row = $result->fetch_assoc();
            // 输出记录
            return $row["ID"];
        } else {
            return false;
        }
        $conn->close();
    }

    # 查询用户ID
    public function get_userId($key)
    {
        $conn = $this->connect();
        // 执行查询的代码
        $sql = "select ID from Users where user_key='$key';";
        $result = $conn->query($sql);
        if ($result->num_rows > 0) {
            // 获取第一条记录
            $row = $result->fetch_assoc();
            // 输出记录
            return $row["ID"];
        } else {
            return false;
        }
        $conn->close();
    }

    # 查询用户名称
    public function get_username($ID)
    {
        $conn = $this->connect();
        // 执行查询的代码
        $sql = "select name from Users where ID='$ID';";
        $result = $conn->query($sql);
        if ($result->num_rows > 0) {
            // 获取第一条记录
            $row = $result->fetch_assoc();
            // 输出记录
            return $row["name"];
        } else {
            return false;
        }
        $conn->close();
    }

    # 查询用户手机号是否已经注册
    public function get_phone($phone)
    {
        $conn = $this->connect();
        // 执行查询的代码
        $sql = "select ID from Users where links='$phone';";
        $result = $conn->query($sql);
        if ($result->num_rows > 0) {
            return true;
        } else {
            return false;
        }
        $conn->close();
    }

    # 查询笔记本是否存在
    public function get_Note_ID($KEY)
    {
        $conn = $this->connect();
        // 执行查询的代码
        $sql = "SELECT l.*,U.`name` FROM listNote l LEFT JOIN Users U ON l.user_id=U.ID WHERE note_key='$KEY';";
        $result = $conn->query($sql);
        if ($result->num_rows > 0) {
            $row = $result->fetch_assoc();
            $data[] = array(
                'ID' => $row['id'],
                'user_id' => $row['user_id'],
                'note_key' => $row['note_key'],
                'note_name' => $row['note_name'],
                'create_date' => $row['create_date'],
                'abstract' => $row['abstract'],
                'state' => $row['state'],
                'name' => $row['name']
            );
            return $data;
        } else {
            return false;
        }
        $conn->close();
    }

    # 查询用户信息
    public function get_user_info($userID)
    {
        $conn = $this->connect();
        // 执行查询的代码
        $sql = "SELECT Users.`name`,Users.addrss, COUNT(listNote.id) AS note_count FROM Users LEFT JOIN listNote ON Users.id = listNote.user_id WHERE Users.id = '$userID' GROUP BY Users.id;";
        $result = $conn->query($sql);
        if ($result->num_rows > 0) {
            $row = $result->fetch_assoc();
            $data[] = array(
                'name' => $row['name'],
                'addrss' => $row['addrss'],
                'note_count' => $row['note_count']
            );
            return $data;
        } else {
            return false;
        }
        $conn->close();
    }

    # 新增一个用户
    function add_user($name, $age, $address, $links, $key)
    {

        $conn = $this->connect();
        // 首先检查$age是否为空
        if (empty($age) || !is_int($age)) {
            $age = 0; // 如果为空，则设置为0
        }
        // 构建插入语句
        $sql = "INSERT INTO Users VALUES (NULL,'$name',$age,'$address','$links','$key');";
        // 执行插入操作
        if ($conn->query($sql) === TRUE) {
            #echo "新记录插入成功";
            return true;
        } else {
            echo "Error: 失败" . $conn->error;
            return false;
        }
        $conn->close();
    }

    # 新增一条笔记记录
    function add_note_list($account, $passwd, $choseWord, $transWord, $Key, $date)
    {
        $conn = $this->connect();
        // 首先检查用户是否正确
        $ID = $this->verify_user_key($account, $passwd, $Key);
        // 构建插入语句
        if ($ID) { // 用户验证成功
            $sql = "INSERT INTO `translate` VALUES (null, '$choseWord', '$transWord', '$date', '$Key');";
            // 执行插入操作
            if ($conn->query($sql) === TRUE) {
                return true;
            } else {
                // echo "Error: 失败" . $conn->error;
                return false;
            }
        } else {
            return false;
        }
        $conn->close();
    }

    # 新增一条笔记记录，不验证用户信息
    function add_note_list_notverify($choseWord, $transWord, $Key)
    {
        $conn = $this->connect();
        $current_time = date('Y-m-d H:i:s');
        $sql = "INSERT INTO `translate` VALUES (null, '$choseWord', '$transWord', '$current_time', '$Key');";
        // 执行插入操作
        if ($conn->query($sql) === TRUE) {
            return true;
        } else {
            // echo "Error: 失败" . $conn->error;
            return false;
        }
        $conn->close();
    }

    # 新增一个笔记本
    function add_NoteBook($userID, $name, $abstract)
    {
        $conn = $this->connect();
        # 循环拿到一个没有使用过的Key
        do {
            $key = 'K_' . get_key();
        } while ($this->get_userId($key)); # Key已存在
        $current_time = date('Y-m-d H:i:s');
        $sql = "INSERT INTO listNote (user_id,note_name,create_date, abstract,note_key,state) VALUES('$userID','$name','$current_time', '$abstract', '$key','normal')";

        // 执行插入操作
        if ($conn->query($sql) === TRUE) {
            #echo "新记录插入成功";
            return true;
        } else {
            echo "Error: 插入失败" . $conn->error;
            return false;
        }
        $conn->close();
    }

    # 删除一个笔记本
    function delete_NoteBook($userID, $BOOKID)
    {
        $conn = $this->connect();
        $sql = "UPDATE listNote set state='delete' WHERE id='$BOOKID' and user_id='$userID'";
        // 执行更新操作
        if ($conn->query($sql) === TRUE) {
            return true;
        } else {
            echo "Error: 更新失败" . $conn->error;
            return false;
        }
        $conn->close();
    }

    # 恢复一个笔记本
    function recycle_NoteBook($userID, $BOOKID)
    {
        $conn = $this->connect();
        $sql = "UPDATE listNote set state='normal' WHERE id='$BOOKID' and user_id='$userID'";
        // 执行更新操作
        if ($conn->query($sql) === TRUE) {
            return true;
        } else {
            echo "Error: 更新失败" . $conn->error;
            return false;
        }
        $conn->close();
    }

    # 修改一个笔记本
    function modify_NoteBook($userID, $BOOKID, $name, $abstract)
    {
        $conn = $this->connect();
        $sql = "UPDATE listNote set note_name='$name' ,abstract='$abstract'  WHERE id='$BOOKID' and user_id='$userID'";
        // 执行更新操作
        if ($conn->query($sql) === TRUE) {
            return true;
        } else {
            echo "Error: 更新失败" . $conn->error;
            return false;
        }
        $conn->close();
    }

    # 永久删除一个笔记本
    function delete_NoteBook_permanent($userID, $BOOKKEY)
    {
        $conn = $this->connect();
        $sql = "DELETE l FROM listNote l WHERE l.user_id = '$userID' AND l.state = 'delete' AND l.note_key='$BOOKKEY'";
        // 执行更新操作
        if ($conn->query($sql) === TRUE) {
            $sql = "DELETE t FROM translate t WHERE t.NoteKey='$BOOKKEY' and t.NoteKey NOT IN ('K_444444', 'K_666666', 'K_sasasa')";
            $conn->query($sql);
            if ($conn->query($sql) === TRUE) {
                return true;
            }
        } else {
            echo "Error: 删除失败" . $conn->error;
            return false;
        }
        $conn->close();
    }

    # 删除一条记录
    function delete_transword_permanent($BOOKKEY, $id)
    {
        $conn = $this->connect();
        $sql = "DELETE l FROM translate l WHERE l.NoteKey = '$BOOKKEY' AND l.id = '$id'";
        // 执行更新操作
        if ($conn->query($sql) === TRUE) {
            return true;
        } else {

            return "删除失败";
        }
        $conn->close();
    }

    # 根据用户ID获取用户的笔记本
    public function get_notebook($ID)
    {
        $conn = $this->connect();
        $sql = "SELECT l.*,COUNT(t.id) 'list_number' FROM `listNote` l LEFT JOIN translate t on l.note_key=t.NoteKey WHERE l.user_id='$ID' and l.state!='delete' GROUP BY l.note_key,l.id;";
        $result = $conn->query($sql);
        $data = []; // 初始化一个空数组来存储所有记录
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                // 将每条记录添加到$data数组中
                $data[] = array(
                    'ID' => $row['id'],
                    'user_id' => $row['user_id'],
                    'note_name' => $row['note_name'],
                    'note_key' => $row['note_key'],
                    'create_date' => $row['create_date'],
                    'state' => $row['state'],
                    'list_number' => $row['list_number'],
                    'abstract' => $row['abstract']
                );
            }
        }
        return $data; // 返回包含所有记录的数组
    }

    # 根据用户ID获取用户删除的笔记本
    public function get_delete_NoteBook($ID)
    {
        $conn = $this->connect();
        $sql = "SELECT l.*,COUNT(t.id) 'list_number' FROM `listNote` l LEFT JOIN translate t on l.note_key=t.NoteKey WHERE l.user_id='$ID' and l.state='delete' GROUP BY l.note_key,l.id;";
        $result = $conn->query($sql);
        $data = []; // 初始化一个空数组来存储所有记录
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                // 将每条记录添加到$data数组中
                $data[] = array(
                    'ID' => $row['id'],
                    'user_id' => $row['user_id'],
                    'note_name' => $row['note_name'],
                    'note_key' => $row['note_key'],
                    'create_date' => $row['create_date'],
                    'state' => $row['state'],
                    'list_number' => $row['list_number'],
                    'abstract' => $row['abstract']
                );
            }
        }
        return $data; // 返回包含所有记录的数组
    }

    # 根据bookKEY来获取全部数据
    public function get_notebook_list($KEY)
    {
        $conn = $this->connect();
        $sql = "SELECT t.*,l.`note_name` from translate t left JOIN listNote l on l.note_key=t.NoteKey WHERE t.NoteKey='$KEY'";
        $result = $conn->query($sql);
        $data = []; // 初始化一个空数组来存储所有记录
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                // 将每条记录添加到$data数组中
                $data[] = array(
                    'ID' => $row['id'],
                    'choseWord' => $row['choseWord'],
                    'transWord' => $row['transWord'],
                    'create_date' => $row['create_date'],
                    'NoteKey' => $row['NoteKey'],
                    'note_name' => $row['note_name']
                );
            }
        }
        return $data; // 返回包含所有记录的数组
    }

    # 根据输入模糊查询
    public function search_notebook($ID, $name)
    {
        $conn = $this->connect();
        $sql = "SELECT t.*,l.`note_name`,l.abstract from translate t left JOIN listNote l on l.note_key=t.NoteKey WHERE l.user_id='$ID' and (t.choseWord like '%$name%' OR t.transWord like '%$name%')";
        $result = $conn->query($sql);
        $data = []; // 初始化一个空数组来存储所有记录
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                // 将每条记录添加到$data数组中
                $data[] = array(
                    'ID' => $row['id'],
                    'choseWord' => $row['choseWord'],
                    'transWord' => $row['transWord'],
                    'create_date' => $row['create_date'],
                    'NoteKey' => $row['NoteKey'],
                    'abstract' => $row['abstract'],
                    'note_name' => $row['note_name']
                );
            }
        }
        return $data; // 返回包含所有记录的数组
    }

    # 根据KEY获取笔记本全部数据
    public function get_notebook_data($KEY, ...$ID)
    {
        $conn = $this->connect();
        if (!empty($ID)) {
            $sql = "SELECT * FROM `translate` WHERE NoteKey='$KEY' and id>$ID[0]  order by id ";
        } else {
            $sql = "SELECT * FROM `translate` WHERE NoteKey='$KEY' order by id ";
        }
        // echo $sql;
        $result = $conn->query($sql);
        $data = []; // 初始化一个空数组来存储所有记录
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                // 将每条记录添加到$data数组中
                $data[] = array(
                    'ID' => $row['id'],
                    'choseWord' => $row['choseWord'],
                    'transWord' => $row['transWord'],
                    'create_date' => $row['create_date'],
                    'NoteKey' => $row['NoteKey']
                );
            }
        }
        return $data; // 返回包含所有记录的数组
    }
}


# 创建新用户函数
function new_user($name, $age, $address, $links, $password)
{
    $mysql = new MySQL();
    if (!($mysql->get_phone($links))) {  # 手机号未被注册
        $shakey = sha256($password);
        # 开始为用户创建数据链
        # 插入用户数据
        $mysql->add_user($name, $age, $address, $links, $shakey);
        return "去登录"; // 历史遗留问题，懒得改，随便写个返回值
    } else {
        return false;
    }
}

# 获取get请求
# 创建用户请求
if (isset($_GET['action']) && $_GET['action'] == 'createuser') {
    $mysql = new MySQL();
    $Name = $_GET['Name'];
    $Age = $_GET['Age'];
    $Address = $_GET['Address'];
    $Links = $_GET['Links'];
    $password = $_GET['password'];
    # 新增用户及建立相关链表
    $is_create = new_user($Name, $Age, $Address, $Links, $password);
    if ($is_create != false) {
        echo $is_create;
        //echo "用户创建成功，密码Key为：" . $is_create;
    } else {
        echo false;
    }
} elseif (isset($_GET['action']) && $_GET['action'] == 'loginuser') {
    $Account = $_POST['Account'];
    $Password = $_POST['Password'];
    $mysql = new MySQL();
    $ID = $mysql->login($Account, sha256($Password)); //判别用户登录函数
    if ($ID) {
        //echo $ID;
        $name = $mysql->get_username($ID);
        // 登录成功，生成JWT令牌
        $jwtService = new JWTService();
        $tokenPayload = ['user_id' => $ID, 'username' => $name];  // 根据需要定义JWT载荷
        $jwtToken = $jwtService->generateToken($tokenPayload);
        // 存储JWT令牌到cookie
        setcookie('jwt_token', $jwtToken, time() + (86400 * 7), '/', '', false, false);
        //echo "登录成功,$ID,$name";
        echo 'succeed';
    } else {
        echo "false";
    }
}
