<?php
require './mysql.php';

class database extends MySQL
{
    # 连接Mysql数据库
    function connect()
    {
        $conn = parent::connect();
        return $conn;
    }

    #覆写父类方法
    public function get_Note_ID($KEY)
    {
        $data = parent::get_Note_ID($KEY);
        return $data;
    }

    public function get_notebook_data($KEY, ...$ID)
    {
        $data = parent::get_notebook_data($KEY, ...$ID);
        return $data;
    }

    public function add_note_list($account, $passwd, $choseWord, $transWord, $Key, $date)
    {
        $data = parent::add_note_list($account, $passwd, $choseWord, $transWord, $Key, $date);
        return $data;
    }
}

$db = new database();

// 检查是否有传入的参数
if (isset($_GET['getNoteData'])) {
    // 获取getNoteData参数的值
    $KEY = $_GET['getNoteData'];
    if ($KEY != '') {
        $content = $db->get_Note_ID($KEY);
        $data_content = '太在意结果的人往往没有一个好结果';
        if ($content != '') {
            if (isset($_GET['MaxID']) && strlen($_GET['MaxID']) > 0) { // 判断是否存在ID起始位
                $data_content = $db->get_notebook_data($KEY, $_GET['MaxID']);
            } else { // 不存在ID起始位，查询全部
                $data_content = $db->get_notebook_data($KEY);
            }
        }
        $response = [
            'code' => 200,
            'msg' => 'success',
            'data' => [
                'summary' => $content,
                'data_list' => $data_content
            ]
        ];
    } else {
        $response = [
            'code' => 401,
            'msg' => 'not found',
            'summary' => [
                'content' => '办不到的承诺就成了枷锁',
            ]
        ];
    }
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $account = $_POST['account'] ?? '';
    $passwd = $_POST['passwd'] ?? '';
    $choseWord = $_POST['choseword'] ?? '';
    $transWord = $_POST['transword'] ?? '';
    $Key = $_POST['Key'] ?? '';
    if ($account != '' && $passwd != '' && $choseWord != '' && $transWord != '' && $Key != '') {
        $current_time = date('Y-m-d H:i:s');
        $response_data = $db->add_note_list($account, $passwd, $choseWord, $transWord, $Key, $current_time);
        if ($response_data) {
            $response = [
                'code' => 200,
                'msg' => 'success',
                'data' => [
                    'insertPOST' => $response_data
                ]
            ];
        } else {
            $response = [
                'code' => 402,
                'msg' => 'faild',
                'data' => [
                    'insertPOST' => '数据插入异常'
                ]
            ];
        }
    } else {
        $response = [
            'code' => 401,
            'msg' => 'faild',
            'data' => [
                'insertPOST' => '参数校验未通过'
            ]
        ];
    }
}

// 直接输出JSON数据
header('Content-Type: application/json');
echo json_encode($response);
