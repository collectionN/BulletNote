<?php
require './mysql.php';

class JWT extends JWTService
{
    private const JWT_SECRET_KEY = '2983717613';  // 替换为您的实际密钥

    public function validateToken(string $token): array
    {
        (array) $decoded = parent::validateToken($token, self::JWT_SECRET_KEY);
        return  $decoded;
    }
}

class db extends MySQL
{
    # 连接Mysql数据库
    function connect()
    {
        $conn = parent::connect();
        return $conn;
    }

    #覆写父类方法
    public function get_notebook($ID)
    {
        $data = parent::get_notebook($ID);
        return $data;
    }
    public function get_notebook_list($KEY)
    {
        $data = parent::get_notebook_list($KEY);
        return $data;
    }

    public function get_delete_NoteBook($ID)
    {
        $data = parent::get_delete_NoteBook($ID);
        return $data;
    }

    function add_NoteBook($userID, $name, $abstract)
    {
        $data = parent::add_NoteBook($userID, $name, $abstract);
        return $data;
    }

    function delete_NoteBook($userID, $BOOKID)
    {
        $data = parent::delete_NoteBook($userID, $BOOKID);
        return $data;
    }

    function recycle_NoteBook($userID, $BOOKID)
    {
        $data = parent::recycle_NoteBook($userID, $BOOKID);
        return $data;
    }

    function delete_NoteBook_permanent($userID, $BOOKID)
    {
        $data = parent::delete_NoteBook_permanent($userID, $BOOKID);
        return $data;
    }

    function search_notebook($userID, $name)
    {
        $data = parent::search_notebook($userID, $name);
        return $data;
    }

    function get_user_info($userID)
    {
        $data = parent::get_user_info($userID);
        return $data;
    }

    function modify_NoteBook($userID, $bookID, $name, $abstract)
    {
        $data = parent::modify_NoteBook($userID, $bookID, $name, $abstract);
        return $data;
    }

    function delete_transword_permanent($Book_KEY, $id)
    {
        $data = parent::delete_transword_permanent($Book_KEY, $id);
        return $data;
    }
    function add_note_list_notverify($choseWord, $transWord, $Key)
    {
        $data = parent::add_note_list_notverify($choseWord, $transWord, $Key);
        return $data;
    }
}

$jwtService = new JWT();
$db = new db();

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    // 获取 POST 请求中的 token
    $token = $_POST['token'] ?? '';
    if ($token != '' || $token != null) {
        // 移除 "Bearer " 前缀
        if (strpos($token, 'Bearer ') === 0) {
            $token = substr($token, 7);
        }

        // 验证 JWT 令牌
        $tokenPayload = $jwtService->validateToken($token);

        if (!empty($tokenPayload)) {
            // 令牌有效
            $type = $_POST['type'] ?? '';
            $Book_KEY = $_POST['Book_KEY'] ?? '';
            if ($type != '' || $type != null) {
                switch ($type) {
                    case 'get_note_booklist':
                        // 获取笔记本列表数据
                        $content = $db->get_notebook($tokenPayload['user_id']);
                        $response = [
                            'code' => 200,
                            'msg' => 'success',
                            'data' => [
                                'content' => $content,
                            ]
                        ];
                        break;
                    case 'get_note_wordlist':
                        // 获取笔记本列表数据
                        $content = $db->get_notebook_list($Book_KEY);
                        $response = [
                            'code' => 200,
                            'msg' => 'success',
                            'data' => [
                                'content' => $content,
                            ]
                        ];
                        break;
                    case 'add_NoteBook':
                        // 增加笔记本
                        $content = $db->add_NoteBook($tokenPayload['user_id'], $_POST['name'], $_POST['abstract']);
                        $response = [
                            'code' => 200,
                            'msg' => 'success',
                            'data' => [
                                'content' => $content,
                            ]
                        ];
                        break;
                    case 'add_note_list_notverify':
                        // 增加笔记
                        $content = $db->add_note_list_notverify($_POST['choseWord'], $_POST['transWord'], $_POST['Key']);
                        $response = [
                            'code' => 200,
                            'msg' => 'success',
                            'data' => [
                                'content' => 'Add_Note_OneNote',
                            ]
                        ];
                        break;
                    case 'delete_NoteBook':
                        // 删除笔记本
                        $content = $db->delete_NoteBook($tokenPayload['user_id'], $Book_KEY);
                        $response = [
                            'code' => 200,
                            'msg' => 'success',
                            'data' => [
                                'content' => 'deleted',
                            ]
                        ];
                        break;
                    case 'recycle_NoteBook':
                        // 恢复笔记本
                        $content = $db->recycle_NoteBook($tokenPayload['user_id'], $_POST['ID']);
                        $response = [
                            'code' => 200,
                            'msg' => 'success',
                            'data' => [
                                'content' => 'recycle',
                            ]
                        ];
                        break;
                    case 'modify_NoteBook':
                        // 修改笔记本
                        $content = $db->modify_NoteBook($tokenPayload['user_id'], $_POST['ID'], $_POST['name'], $_POST['abstract']);
                        $response = [
                            'code' => 200,
                            'msg' => 'success',
                            'data' => [
                                'content' => 'modifyed',
                            ]
                        ];
                        break;
                    case 'get_delete_NoteBook':
                        // 获取回收站笔记本
                        $content = $db->get_delete_NoteBook($tokenPayload['user_id'], $Book_KEY);
                        $response = [
                            'code' => 200,
                            'msg' => 'success',
                            'data' => [
                                'content' => $content,
                            ]
                        ];
                        break;
                    case 'delete_NoteBook_permanent':
                        // 永久删除笔记本
                        $content = $db->delete_NoteBook_permanent($tokenPayload['user_id'], $Book_KEY);
                        $response = [
                            'code' => 200,
                            'msg' => 'success',
                            'data' => [
                                'content' => 'droped',
                            ]
                        ];
                        break;
                    case 'delete_transword_permanent':
                        // 删除其中一条笔记
                        $content = $db->delete_transword_permanent($Book_KEY, $_POST['id']);
                        // $content = $Book_KEY + " " + $_POST['id'];
                        $response = [
                            'code' => 200,
                            'msg' => 'success',
                            'data' => [
                                'content' => $content,
                            ]
                        ];
                        break;
                    case 'search':
                        // 搜索笔记
                        $content = $db->search_notebook($tokenPayload['user_id'], $_POST['name']);
                        $response = [
                            'code' => 200,
                            'msg' => 'success',
                            'data' => [
                                'content' => $content,
                            ]
                        ];
                        break;
                    case 'get_user_info':
                        // 获取用户信息
                        $content = $db->get_user_info($tokenPayload['user_id']);
                        $response = [
                            'code' => 200,
                            'msg' => 'success',
                            'data' => [
                                'content' => $content,
                            ]
                        ];
                        break;
                    default:
                        $response = [
                            'code' => 401,
                            'msg' => '未知的处理事件',
                            'data' => [
                                'key' => $Book_KEY,
                            ]
                        ];
                        break;
                }
            } else {
                $response = [
                    'code' => 401,
                    'msg' => '无处理事件',
                    'data' => [
                        'key' => '',
                    ]
                ];
            }
        } else {

            // 令牌无效或已过期
            $response = [
                'code' => 401,
                'msg' => '验证失败，用户状态过期或无效',
                'data' => [
                    'key' => $token,
                ]
            ];
        }
    } else {
        // 令牌无效或已过期
        // 共享不需要登录
        $type = $_POST['type'] ?? '';
        if ($type === "get_share_NoteBook") {
            // 获取共享笔记本
            $content = $db->get_notebook('1001'); // 管理员账户
            $response = [
                'code' => 200,
                'msg' => 'success',
                'data' => [
                    'content' => $content,
                ]
            ];
        } else {
            $response = [
                'code' => 401,
                'msg' => '验证失败，未获取到用户状态',
                'data' => ''
            ];
        }
    }

    // 设置 JSON 响应内容类型
    header('Content-Type: application/json');
    // 输出响应
    echo json_encode($response);
} else {
    // 非法请求
    http_response_code(405); // 方法不允许
    echo json_encode(['code' => 405, 'msg' => '非法请求']);
}
