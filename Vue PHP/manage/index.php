<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <title>弹幕笔记管理</title>
  <link rel="stylesheet" href="./plugins/elementui/index.css" />
  <link rel="stylesheet" href="./css/hua.css" />
  <!-- 引入组件库 -->
  <link rel="stylesheet" href="//at.alicdn.com/t/c/font_4526221_0q5ly93fwoei.css">
</head>

<body>
  <div id="app">
    <el-row :gutter="10">
      <!-- 定义左侧列表栏目 -->
      <el-col :span="4">
        <div class="grid-content bg-purple height_full_content" style="position: relative;">
          <!-- logo栏目 -->
          <el-col :span="24">
            <div class="grid-content bg-purple margin_10 logo_position">
              <img src="img/logo.gif" alt="弹幕笔记" class="logo_img" />
            </div>
          </el-col>
          <!-- 列表栏目 -->
          <el-col :span="24" style="margin-top: 20px;">
            <span class="tip_text ">我的云文件</span>
            <el-menu default-active="2" class="el-menu-vertical-demo">
              <el-menu-item index="2" v-on:click="openUrl('page/listBook.html','我的笔记本')">
                <i class="iconfont icon-danciben"></i>
                <span slot="title" class="font_almmsh">我的笔记本</span>
              </el-menu-item>
              <el-menu-item index="4" v-on:click="openUrl('page/manageBook.html','管理笔记')">
                <i class="iconfont icon-tixing"></i>
                <span slot="title" class="font_almmsh">管理笔记</span>
              </el-menu-item>
              <el-menu-item index="5" v-on:click="openUrl('page/terminal.html','伴侣终端')">
                <i class="iconfont icon-shouye"></i>
                <span slot="title" class="font_almmsh">伴侣终端</span>
              </el-menu-item>
              <el-menu-item index="6" v-on:click="openUrl('page/recycle.html','回收站')">
                <i class="iconfont icon-shanchu"></i>
                <span slot="title" class="font_almmsh">回收站</span>
              </el-menu-item>
              <el-menu-item index="7" v-on:click="openUrl('page/public.html','分享广场')">
                <i class="iconfont icon-fenxiang"></i>
                <span slot="title" class="font_almmsh">分享广场</span>
              </el-menu-item>
            </el-menu>
          </el-col>
          <!-- 个人中心 -->
          <el-col :span="24">
            <el-popover placement="top-start" width="210" trigger="hover">
              <h3>{{User.name}} <el-tag color="#fc5531" size="mini" type='warning' effect="dark">
                  第{{ User.id }}个内测用户🎉
                </el-tag></h3>
              <span>{{User.sign}}</span>
              <el-divider></el-divider>
              <!-- <el-button size="mini" type="primary" icon="el-icon-setting" v-on:click="openUrl('page/message.html','个人中心')">用户中心</el-button> -->
              <!-- 使用Cookie判断用户登录状态 -->
              <el-button size="mini" type="danger" style="width: 98%;" icon="el-icon-circle-close" @click="logout">退出登陆</el-button>
              <!-- <el-button v-if="!this.token || this.token === ''" size="mini" type="success" icon="el-icon-circle-close" v-on:click="openUrl('page/login.html','个人中心')">注册登录</el-button>  -->
              <div class="grid-content bg-purple user_info bg_cyan" slot="reference" style="margin-left: -2px;">
                <el-row :gutter="2">
                  <el-col :span="5">
                    <div class="grid-content bg-purple" style="margin-left: -15px;">
                      <!-- 头像 -->
                      <el-avatar shape="square" :size="50" :src="User.img"></el-avatar>
                    </div>
                  </el-col>
                  <!-- 信息设置 -->
                  <el-col :span="19">
                    <div class="grid-content bg-purple user_info_content">
                      <h4>{{User.name}} <el-tag size="mini" v-if="User.key">{{User.key}}</el-tag><el-tag type="warning" size="mini" v-else="User.key">花粉</el-tag></h3>
                        <el-progress :percentage="percentage" status="success"></el-progress>
                        <span style="font-size: 12px;" class="font_almmsh">{{User.useStorage}}/{{User.storage}} NoteBook</span>
                    </div>
                  </el-col>
                </el-row>
              </div>
          </el-col>
          </el-popover>
        </div>
      </el-col>
      <!-- 定义右侧内容栏目 -->
      <el-col :span="20">
        <div class="grid-content bg-purple height_full_content" style="margin-top: 1%;height: 95.5vh;">
          <!-- 右侧顶部内容栏目 -->
          <el-row>
            <el-col :span="24">
              <div class="grid-content bg-purple-dark">
                <el-row :gutter="20">
                  <!-- 文件上传 -->
                  <el-col :span="8">
                    <div class="grid-content bg-purple">
                      <el-button type="primary" icon="iconfont icon-danciben" @click="dialogVisible = true">
                        新笔记本</el-button>
                      <el-button type="success" icon="el-icon-download" @click="backup">下载中心</el-button>
                        <!-- <el-button type="info" plain icon="el-icon-folder">新建文件夹</el-button> -->
                    </div>
                  </el-col>
                  <!-- 文件搜索 -->
                  <el-col :span="6" :offset="7">
                    <div class="grid-content bg-purple">
                      <el-input placeholder="搜索全部内容" v-model="filesQueryValues" @keyup.enter="filesQuery" clearable>
                        <i slot="prefix" class="el-input__icon el-icon-search"></i>
                      </el-input>
                    </div>
                  </el-col>
                  <!-- 客户端 -->
                  <el-col :span="3">
                    <div class="grid-content bg-purple">
                      <el-button type="primary" @click="filesQuery">搜索内容</el-button>
                    </div>
                  </el-col>
                </el-row>
              </div>
            </el-col>
          </el-row>
          <!-- 右侧内容栏目 -->
          <el-row>
            <!-- 内容banner -->
            <el-row>
              <el-col :span="24">
                <div class="grid-content bg-purple-dark">
                  <el-row :gutter="20">
                    <el-col :span="12">
                      <!-- 面包屑 -->
                      <div class="grid-content bg-purple">
                        <el-breadcrumb separator-class="el-icon-arrow-right">
                          <el-breadcrumb-item :to="{ path: '/' }" style="margin-top: 14px;font-size: 18px;" class="font_almmsh">首页</el-breadcrumb-item>
                          <el-breadcrumb-item style="margin-top: 14px;font-size: 18px;" class="font_almmsh">{{breadcrumb}}</el-breadcrumb-item>
                        </el-breadcrumb>
                      </div>
                    </el-col>
                    <template>
                      <el-dialog title="新建笔记本" :visible.sync="dialogVisible" width="30%" @close="resetForm">
                        <el-form>
                          <el-input v-model="notebookName" placeholder="笔记本名称 *"></el-input>
                          <el-input v-model="notebookDescription" placeholder="简单描述一下吧" style="margin-top: 20px;"></el-input>
                        </el-form>
                        <span slot="footer" class="dialog-footer">
                          <el-button @click="dialogVisible = false">取消</el-button>
                          <el-button type="primary" @click="confirmCreateNotebook">确认</el-button>
                        </span>
                      </el-dialog>
                    </template>
                    <!-- 多选操作 -->
                    <!-- <el-col :span="6" :offset="6">
                      <div class="grid-content bg-purple">
                        <el-button type="primary" icon="el-icon-share"></el-button>
                        <el-button type="danger" icon="el-icon-delete"></el-button>
                      </div>
                    </el-col> -->
                  </el-row>
                </div>
              </el-col>
            </el-row>
            <!-- 文件列表 -->
            <el-row>
              <el-col :span="24" style="height: 77vh;">
                <iframe ref="iframeRef" v-bind:src="iframeUrl" frameborder="0" width="100%" height="98%"></iframe>
              </el-col>
            </el-row>
          </el-row>
          <!--  -->
        </div>
      </el-col>
    </el-row>
  </div>
</body>
<script src="js/vue.js"></script>
<!-- 引入组件库 -->
<script src="plugins/elementui/index.js"></script>
<script type="text/javascript" src="js/jquery.min.js"></script>
<script src="https://cdn.bootcdn.net/ajax/libs/js-sha256/0.10.1/sha256.js"></script>
<script src="js/axios-0.18.0.js"></script>
<script src="js/vue-router.js"></script>
<script src="js/Cookie.js"></script>
<script type="text/javascript">
  var vue = new Vue({
    el: '#app',
    data() {
      return {
        isCollapse: false,
        uploadDialogVisible: false,
        filename: '',
        iframeUrl: "page/listBook.html", //加载的Iframe
        breadcrumb: '我的笔记本', //面包屑
        token: Cookies.get('jwt_token'), //取得本地Cookie
        dialogVisible: false, // 控制弹窗显示
        notebookName: '', // 笔记本名称输入框的数据
        notebookDescription: '', // 笔记本描述输入框的数据
        filesQueryValues: '', //搜索框
        User: { //实例用户
          name: '阿花',
          id: '1',
          account: '11111111111',
          password: 'bcb15f821479b4d5772bd0ca866c00ad5f926e3580720659cc80d39c9d09802a',
          sign: '所有的成长，到最后都是一次旅行',
          img: 'https://pic.imgdb.cn/item/663c9ab20ea9cb140363356f.jpg',
          key: '',
          // 权限相关
          useStorage: -1, // 已创建笔记本
          storage: 10, // 上限
          login: true,
          write: true,
          delete: true,
          ai: true,
          rename: true,
          upload: true,
          download: true,
          share: true
        },
        //表格数据
        form: {
          name: '',
          abstract: ''
        },
        formLabelWidth: '120px',
        percentage: 0
      }
    },
    //Vue接管事件，加载完成后使用
    mounted() {
      //进行令牌校验
      jwt_token = Cookies.get('jwt_token');
      // Vue的用户判别
      if (jwt_token != '' || jwt_token != null) {
        this.jwtcheck(jwt_token)
      } else {
        this.tip('warning', '用户未登录');
      };
      this.get_user_info(jwt_token);
    },
    methods: {
      //搜索框
      filesQuery() {
        var values = this.filesQueryValues
        if (values == '') {
          this.tip('warning', '请输入搜索内容');
        } else {
          this.openUrl("page/search.html?type=查询&values=" + values, '搜索结果')
        }
      },
      //切换不同的Iframe
      openUrl: function(url, msg) {
        this.iframeUrl = url //+ "&time=" + new Date().getTime();
        this.breadcrumb = msg
      },
      //编辑
      handleEdit(index, row) {
        console.log(index, row);
      },
      //删除
      handleDelete(index, row) {
        console.log(index, row);
      },
      // 提示窗口
      tip(type, msg) {
        //success,warning,error
        this.$message({
          showClose: true,
          message: msg,
          type: type
        });
      },
      //点击事件
      backup(path) {
        window.open('http://bullet.nrhs.eu.org/app', '_blank');
      },
      //退出登录
      logout() {
        this.tip('success', '已退出登录！')
        Cookies.remove('jwt_token')
        window.location.reload();
      },
      confirmCreateNotebook() {
        // 在这里处理确认新建笔记本的逻辑
        // console.log('确认创建笔记本：', this.notebookName, this.notebookDescription);
        if (this.notebookName != null && this.notebookName != '') {
          this.dialogVisible = false; // 关闭弹窗
          this.add_NoteBook(this.notebookName, this.notebookDescription)
        } else {
          this.tip('waring', '名称不能为空哦')
        }
        // 执行创建笔记本的操作...
      },
      resetForm() {
        // 在弹窗关闭时重置输入框
        this.notebookName = '';
        this.notebookDescription = '';
      },
      config() {
        return config = {
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          },
          transformRequest: [function(data) {
            let ret = '';
            for (let it in data) {
              ret += encodeURIComponent(it) + '=' + encodeURIComponent(data[it]) + '&';
            }
            return ret.substring(0, ret.length - 1);
          }]
        };
      },
      jwtcheck(token) {
        // 确保 Content-Type 为 application/x-www-form-urlencoded
        const config = this.config();

        // POST 发送请求
        axios.post("../verify.php", {
            token: 'Bearer ' + token
          }, config).then((response) => {
            if (response.data.code == 200) {
              this.tip('success', '哈喽，' + response.data.data.name + '同学');
              // 存储cookie状态，判断用户信息
              this.User.id = response.data.data.id - 1000
              this.User.name = response.data.data.name
              // that.tableData = res.data.data;
            } else {
              this.tip('error', response.data.code + " " + response.data.msg);
              window.location.replace("../register.php");
            }
          })
          .catch((error) => {
            // 处理请求错误
            this.tip('error', '请求失败：' + error.message);
          });
      },
      get_user_info(token) {
        // 获取我的词书数据
        // 确保 Content-Type 为 application/x-www-form-urlencoded
        const config = this.config();

        // POST 发送请求
        axios.post("../../device/get_data.php", {
            token: 'Bearer ' + token,
            type: 'get_user_info'
          }, config).then((response) => {
            if (response.data.code == 200) {
              this.User.useStorage = response.data.data.content[0].note_count;
              this.percentage = response.data.data.content[0].note_count * 10;
              // $.getScript('copy.js');
              return true;
            } else {
              this.tip('error', response.code + " " + response.data.msg);
              return false;
            }
          })
          .catch((error) => {
            // 处理请求错误
            this.tip('error', '请求失败：' + error.message);
          });
      },
      add_NoteBook(name, abstract) {
        // 获取我的词书数据
        const config = this.config();

        // POST 发送请求
        axios.post("../../device/get_data.php", {
            token: 'Bearer ' + this.token,
            type: 'add_NoteBook',
            name: name,
            abstract: abstract
          }, config).then((response) => {
            if (response.data.code == 200) {
              this.tip('success', '已创建' + name);
              this.openUrl("./page/listBook.html", '我的笔记本')
            } else {
              this.tip('error', response.code + " " + response.data.msg);
              return false;
            }
          })
          .catch((error) => {
            // 处理请求错误
            this.tip('error', '请求失败：' + error.message);
          });
      },

    },
  })

  // 样式
  //列表js样式
  var items = document.querySelectorAll('.el-menu-vertical-demo .el-menu-item');

  items.forEach(function(item) {
    item.addEventListener('click', function() {
      // 移除所有菜单项的 'active' 类
      items.forEach(function(i) {
        i.classList.remove('active');
      });
      // 给被点击的菜单项添加 'active' 类
      this.classList.add('active');
    });
  });


  var items = document.querySelectorAll('.el-menu-vertical-demo .el-menu-item');

  items.forEach(function(item) {
    item.addEventListener('click', function() {
      // 移除所有菜单项的 'active' 类
      items.forEach(function(i) {
        i.classList.remove('active');
      });
      // 给被点击的菜单项添加 'active' 类
      this.classList.add('active');
    });
  });
</script>

</html>