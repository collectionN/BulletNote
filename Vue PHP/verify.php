<?php
require './vendor/autoload.php';

use Firebase\JWT\JWT;
use Firebase\JWT\Key;

class JWTService
{
    private const JWT_SECRET_KEY = '2983717613';  // 替换为您的实际密钥

    public function validateToken(string $token): array
    {
        try {
            $decoded = JWT::decode($token, new Key(self::JWT_SECRET_KEY, 'HS256'));
            return (array) $decoded;
        } catch (Exception $e) {
            // 如果令牌过期或者校验失败，返回 false
            return [];
        }
    }
}

$jwtService = new JWTService();
// 检查是否收到了 POST 请求
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    // 获取 POST 请求中的 token
    $token = $_POST['token'] ?? '';
    if ($token != '' || $token != null) {
        // 移除 "Bearer " 前缀
        if (strpos($token, 'Bearer ') === 0) {
            $token = substr($token, 7);
        }

        // 验证 JWT 令牌
        $tokenPayload = $jwtService->validateToken($token);

        if (!empty($tokenPayload)) {
            // 令牌有效，您可以在这里返回用户信息或其他数据
            $response = [
                'code' => 200,
                'msg' => '验证成功',
                'data' => [
                    'id' => $tokenPayload['user_id'] ?? '',
                    'name' => $tokenPayload['username'] ?? '',
                ]
            ];
        } else {
            // 令牌无效或已过期
            $response = [
                'code' => 401,
                'msg' => '验证失败，未获取到用户',
                'data' => [
                    'key' => $token,
                ]
            ];
        }
    } else {
        // 令牌无效或已过期
        $response = [
            'code' => 401,
            'msg' => '验证失败，未获取到用户状态',
            'data' => ''
        ];
    }

    // 设置 JSON 响应内容类型
    header('Content-Type: application/json');
    // 输出响应
    echo json_encode($response);
} else {
    // 非法请求
    http_response_code(405); // 方法不允许
    echo json_encode(['code' => 405, 'msg' => '非法请求']);
}
