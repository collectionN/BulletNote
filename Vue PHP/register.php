<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>登录到弹幕笔记</title>
    <link rel="stylesheet" href="./css/register.css">
    <link href="./css/layui.css" rel="stylesheet">
    <script src="./js/layui.js"></script>
    <script src="./manage/js/jquery.min.js"></script>

    <!-- 导入字体图标 -->
    <script>
        function load_pageData() {
            layui.use('layer', function() {
                let layer = layui.layer;
                //Loading
                let index = layer.load(0, {
                    shade: false
                }); //0代表加载的风格，支持0-2
            });
        }

        function createuser() {
            // 获取input的值
            //var inputValue = document.getElementById('inputValue').value;
            var password = document.getElementById("password").value;
            var Name = document.getElementById("Name").value;
            var againstPassword = document.getElementById("againstPassword").value;
            var Links = document.getElementById("Links").value;
            if (Name !== "" && Links !== "" && password !== '' && password === againstPassword && password.length >= 6) {
                load_pageData(); //动画
                // 发送AJAX请求到本地PHP页面
                $.ajax({
                    url: './device/mysql.php?action=createuser',
                    type: 'GET',
                    data: {
                        //value: inputValue,
                        password: password,
                        Age: 0,
                        Address: '',
                        Name: Name,
                        Links: Links
                    },
                    success: function(response) {
                        if (response != false) {
                            // 修改p标签的内容
                            $(".btn-grad").text(response)
                            $(".user_info").hide()
                            $(".user_success_register").show()
                            $(".login_button").on("click", function() {
                                $(".user_info").show()
                            });

                            $(".register_button").on("click", function() {
                                $(".user_info").hide()
                                $(".user_success_register").show()
                                location.reload();
                            });
                            layer.closeAll("loading"); //关闭动画
                        } else {
                            $(".btn-grad").text("返回")
                            $(".user_info").hide()
                            $(".user_fail_register").show()
                            $(".btn-grad").on("click", function() {
                                $(".user_info").show()
                                location.reload();
                            });
                            layer.closeAll("loading"); //关闭动画
                        }
                    },
                    error: function() {
                        layer.closeAll("loading"); //关闭动画
                        layer.msg('啊这？好像请求失败了？刷新试试.');
                    }
                });
            } else {
                layer.msg('信息不完整，密码不能小于6位哦');
            }
        }
        //登录逻辑
        function login_user() {
            // 获取input的值
            //var inputValue = document.getElementById('inputValue').value;
            var Account = document.getElementById("Account").value;
            var Password = document.getElementById("Password").value;
            if (Password !== "" && Account !== "") {
                load_pageData(); //动画
                // 发送AJAX请求到本地PHP页面
                $.ajax({
                    url: './device/mysql.php?action=loginuser', // 替换为你的PHP页面路径
                    type: 'POST',
                    data: {
                        //value: inputValue,
                        Account: Account,
                        Password: Password
                    },
                    success: function(response) {
                        //
                        if (response == "false") {
                            $(".user_info").hide()
                            $(".user_fail_register").show()
                            $(".btn-grad").on("click", function() {
                                location.reload();
                            });
                            layer.closeAll("loading"); //关闭动画
                        } else {
                            layer.closeAll("loading"); //关闭动画
                            console.log("zsss");
                            window.location.replace("./manage/");
                        }
                    },
                    error: function() {
                        layer.msg('啊这？好像登录失败了？刷新试试.');
                        layer.closeAll("loading"); //关闭动画
                    }
                });
            } else {
                layer.msg('信息不完整，密码不能小于6位哦');
            }
        }
    </script>
</head>

<body>
    <div class="shell">
        <div class="container a-container" id="a-container">
            <form action="" method="" class="form" id="a-form">
                <h2 class="form_title title">创建账号</h2>
                <!--<div class="form_icons">
                    <i class="iconfont icon-QQ"></i>
                    <i class="iconfont icon-weixin"></i>
                    <i class="iconfont icon-bilibili-line"></i>
                </div>-->
                <div class="user_info">
                    <span class="form_span">联系方式将作为登录账户</span>
                    <input type="text" class="form_input" id="Name" placeholder="账户/昵称">
                    <input type="text" class="form_input" id="Links" placeholder="手机号码/电子邮箱">
                    <input type="text" class="form_input" id="password" placeholder="密码">
                    <input type="text" class="form_input" id="againstPassword" placeholder="确认密码"><br />
                    <button class="form_button button submit" onclick="createuser();"> 注册</button>
                </div>
                <div class="user_success_register">
                    <div class="ico"></div>
                    <h1><b>注册成功</b></h1>
                    <h2><b>去登录试试吧</b></h2>
                    <div class="btn-grad login_button switch-btn switch_button"></div>
                </div>
                <div class="user_fail_register">
                    <div class="ico"></div>
                    <h1><b>注册失败</b></h1>
                    <h2><b>可能你的手机号码已经被使用？！</b></h2>
                    <div class="btn-grad">返回</div>
                </div>
            </form>
        </div>

        <div class="container b-container" id="b-container">
            <form action="" method="" class="form" id="b-form">
                <h2 class="form_title title">登录账号</h2>
                <!--<div class="form_icons">
                    <i class="iconfont icon-QQ"></i>
                    <i class="iconfont icon-weixin"></i>
                    <i class="iconfont icon-bilibili-line"></i>
                </div>-->
                <div class="user_info">
                    <span class="form_span">请输入账户名与密码进行登录</span>
                    <input type="text" class="form_input" id="Account" placeholder="手机号码/邮箱地址 *">
                    <input type="password" class="form_input" id="Password" placeholder="密码">
                    <button class="form_button button submit " onclick="login_user();">登录</button>

                    <!--<a class="form_link">忘记密码？</a>-->
                </div>
                <div class="user_fail_register">
                    <div class="ico"></div>
                    <h1><b>登录失败</b></h1>
                    <h2><b>可能你的账户密码错误呐</b></h2>
                    <div class="btn-grad">返回</div>

                </div>
            </form>
        </div>

        <div class="switch" id="switch-cnt">
            <div class="switch_circle"></div>
            <div class="switch_circle switch_circle-t"></div>
            <div class="switch_container" id="switch-c1">
                <h2 class="switch_title title" style="letter-spacing: 0;">登录弹幕笔记</h2>
                <p class="switch_description description">快速建立同步笔记本，信息多端同步共享<br />支持PC笔记快速流转到弹幕笔记APP</p>
                <button class="switch_button button switch-btn login_button" id="login_button_t">去登录</button>
            </div>
            <div class="switch_container is-hidden" id="switch-c2">
                <h2 class="switch_title title" style="letter-spacing: 0;">注册弹幕笔记</h2>
                <p class="switch_description description">注册成为弹幕笔记用户<br />体验完整弹幕笔记功能</p>
                <button class="switch_button button switch-btn register_button">去注册</button>
            </div>
        </div>
    </div>
</body>

</html>
<!--
<script>
    $(document).ready(function() {
        $('.user_info').on('submit', function(event) {
            event.preventDefault(); // 阻止表单默认提交行为
            //var Name = document.getElementById("Name").value;
            //var Age = document.getElementById("Age").value;
            //var Address = document.getElementById("Address").value;
            //var Links = document.getElementById("Links").value;
            var formData = $(this).serialize(); // 获取表单数据
            $.ajax({
                url: './device/mysql.php',
                type: 'GET',
                dataType: 'text',
                data: formData,
                success: function(response) {
                    $('.btn-grad').html(response); // 将PHP返回值修改到页面的p标签中
                },
                error: function(xhr, status, error) {
                    console.error('Error: ' + error);
                }
            });
        });
    });
</script>-->
<script>
    let switchCtn = document.querySelector("#switch-cnt");
    let switchC1 = document.querySelector("#switch-c1");
    let switchC2 = document.querySelector("#switch-c2");
    let switchCircle = document.querySelectorAll(".switch_circle");
    let switchBtn = document.querySelectorAll(".switch-btn");
    let aContainer = document.querySelector("#a-container");
    let bContainer = document.querySelector("#b-container");
    let allButtons = document.querySelectorAll(".submit");

    let getButtons = (e) => e.preventDefault()
    let changeForm = (e) => {
        // 修改类名
        switchCtn.classList.add("is-gx");
        setTimeout(function() {
            switchCtn.classList.remove("is-gx");
        }, 1500)
        switchCtn.classList.toggle("is-txr");
        switchCircle[0].classList.toggle("is-txr");
        switchCircle[1].classList.toggle("is-txr");

        switchC1.classList.toggle("is-hidden");
        switchC2.classList.toggle("is-hidden");
        aContainer.classList.toggle("is-txl");
        bContainer.classList.toggle("is-txl");
        bContainer.classList.toggle("is-z");
    }
    // 点击切换
    let shell = (e) => {
        for (var i = 0; i < allButtons.length; i++)
            allButtons[i].addEventListener("click", getButtons);
        for (var i = 0; i < switchBtn.length; i++)
            switchBtn[i].addEventListener("click", changeForm)
    }

    window.addEventListener("load", shell);
</script>
</body>
<script>
    window.onload = function() {
        document.getElementById('login_button_t').click();
    };
</script>

</html>