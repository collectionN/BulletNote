--分辨率转换函数
function 转分辨率(sdp)
  --导入所需类
  import "android.util.TypedValue"

  local dm=this.getResources().getDisplayMetrics()

  local types={px=0,dp=1,sp=2,pt=3,["in"]=4,mm=5}

  local n,ty=sdp:match("^(%-?[%.%d]+)(%a%a)$")

  return TypedValue.applyDimension(types[ty],tonumber(n),dm)

end


function drawTag()

  import "android.graphics.RectF"

  import "android.graphics.Point"

  import "android.graphics.Paint"

  import "android.graphics.Path"

  import "android.graphics.Color"

  return LuaDrawable(function(c,p,d)

    c.drawColor(0x00000000)

    p.setAntiAlias(true);

    p.setStrokeWidth(20);

    p.setStyle(Paint.Style.FILL);

    local quYu=d.bounds

    local width=quYu.right

    local height=quYu.bottom

    local radius = 转分辨率("12dp")

    p.setColor(0x17d1a200)

    p.setShadowLayer(25,0,0,0x17d1a200)

    local path=Path();

    path.moveTo(0,radius/2);

    path.arcTo(RectF(0,0,radius,radius),180,90);

    path.lineTo(width-radius,0);

    path.arcTo(RectF(width-radius,0,width,radius),270,90);

    path.lineTo(width,height-radius/2);

    path.arcTo(RectF(width-radius,height-radius,width,height),0,90);

    path.lineTo(width*.06,height);

    path.lineTo(0,height-width*.06)

    c.drawPath(path,p);

    p.setShadowLayer(15,1,-1,0x17d1a200)

    local path2 = Path()

    path2.moveTo(0,height-width*.06)

    path2.lineTo(width*.06-radius/2,height-width*.06);

    path2.arcTo(RectF(width*.06-radius,height-width*.06,width*.06,height-width*.06+radius),270,90);

    path2.lineTo(width*.06,height)

    c.drawPath(path2,p)

  end)

end
