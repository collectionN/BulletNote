--[[简单的浏览器]]

require "import"
require "import"
import "android.app.*"
import "android.os.*"
import "android.widget.*"
import "android.view.*"
import "AndLua"
layout=
{
  LinearLayout,--线性布局
  orientation='vertical',--方向
  layout_width='fill',--宽度
  layout_height='wrap',--高度
  background='#00FFFFFF',--背景颜色或图片路径
  {
    CardView;--卡片控件
    layout_margin='0dp';--边距
    layout_gravity='center';--重力
    layout_width='fill';--宽度
    layout_height='fill';--高度
    CardBackgroundColor='0';--颜色
    radius='0dp';--圆角
    {
      LuaWebView;--浏览器控件
      id="more_bowser";
      layout_width='fill';--宽度
      layout_height='fill';--高度
    };
  };
};

activity.setTheme(R.Theme_Blue)
activity.setContentView(loadlayout(layout))

local m={
  {MenuItem,
    title="退出",},
  {MenuItem,
    title="浏览器打开",},
}
function onCreateOptionsMenu(menu)
  loadmenu(menu,m,nil,2)
end

function onOptionsItemSelected(item)
  local item = tostring(item)
  if item == "退出" then
    activity.finish()
   elseif item == "浏览器打开" then
    import "android.content.Intent"
    import "android.net.Uri"
    viewIntent = Intent("android.intent.action.VIEW",Uri.parse(more_bowser.getUrl()))
    activity.startActivity(viewIntent)
  end
end
function main(name)
  activity.ActionBar.setTitle(name)
  if name=="关于我们" then
    more_bowser.loadUrl("https://support.qq.com/embed/phone/427645/team/")
   elseif name=="交流帮助" then
    more_bowser.loadUrl("https://support.qq.com/embed/phone/427645/")
   elseif name=="更新信息" then
    more_bowser.loadUrl("http://pan.nrhs.eu.org/K%E7%9A%84%E8%89%BA%E6%9C%AF/Resource/DeskBullet")
   elseif name:find("http") then
    more_bowser.loadUrl(name)
  end
end
