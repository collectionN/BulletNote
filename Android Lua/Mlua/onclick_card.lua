import "android.graphics.Typeface"
function ttf_lay()
  return Typeface.createFromFile(File(activity.getLuaDir().."/res/Alimama_ShuHeiTi_Bold.ttf"))
end
function add_card()
  添加卡片弹窗=
  {--布局
    LinearLayout;
    orientation="vertical";
    Focusable=true,
    FocusableInTouchMode=true,
    {
      TextView;--文本控件
      gravity='center';--重力
      --左:left 右:right 中:center 顶:top 底:bottom
      layout_width='fill';--宽度
      layout_height='40dp';--高度
      textColor='#ff51acf6';--文字颜色
      text='添加武器';--显示文字
      textSize='20';--文字大小
      layout_weight="1.0";
      layout_marginTop="fill";--边顶
      Typeface=ttf_lay(),
    };
    {
      LinearLayout;
      orientation='horizontal';--重力属性
      layout_width='fill';--布局宽度
      layout_height='50dp';--布局高度
      gravity='center';--默认居中
      backgroundColor="#FFFFFFFF";--背景色
      {
        CardView;--卡片控件
        layout_margin='8dp';--边距
        layout_gravity='center';--重力
        --左:left 右:right 中:center 顶:top 底:bottom
        elevation='0dp';--阴影
        layout_width='fill';--宽度
        layout_height='12%w';--高度
        CardBackgroundColor='0';--颜色
        radius='8dp';--圆角
        {
          CardView;--卡片控件
          layout_margin='0dp';--边距
          layout_gravity='center|left';--重力
          --左:left 右:right 中:center 顶:top 底:bottom
          elevation='0dp';--阴影
          layout_width='fill';--宽度
          layout_height='12%w';--高度
          CardBackgroundColor='#2a000000';--颜色
          radius='8dp';--圆角
          {
            EditText;--编辑框控件
            id="edit";
            singleLine=true,--设置单行输入
            layout_width='fill';--编辑框宽度
            layout_height='fill';--编辑框高度
            textSize='12sp';--文字大小
            hintTextColor='#FF7D7D7D';--提示文字颜色
            textColor='#FF000000';--输入文字的颜色
            Hint='输入名称或粘贴同步Key';--设置编辑框为空时的提示文字
            background='#ffefefef';
          };
        };

      };
    };
    {
      LinearLayout;
      orientation='horizontal';--重力属性
      layout_width='fill';--布局宽度
      layout_height='fill';--布局高度
      {

        TextView;--文本控件
        gravity='center';--重力
        --左:left 右:right 中:center 顶:top 底:bottom
        layout_width='43.5%w';--宽度
        textColor='#ff51acf6';--文字颜色
        text='取消';--显示文字
        textSize='15dp';--文字大小
        style="?android:attr/borderlessButtonStyle";
        onClick=function()
          添加卡片弹窗.dismiss()
        end
      };
      {
        TextView;--文本控件
        layout_gravity='center';--重力
        --左:left 右:right 中:center 顶:top 底:bottom
        layout_width='43.5%w';--宽度
        textColor='#ff51acf6';--文字颜色
        text='添加';--显示文字
        textSize='15dp';--文字大小
        style="?android:attr/borderlessButtonStyle";
        id="wc";
      };
    };
  };
  添加卡片弹窗=AlertDialog.Builder(this).setView(loadlayout(添加卡片弹窗)).show()

  wc.onClick=function()--点击事件
    edit.Text=edit.Text:gsub(' ',"")
    if edit.Text ~= "" then
      if edit.Text:sub(1, 2)=="K_" and #edit.Text==10 then
        print("尝试同步笔记本...")
        添加卡片弹窗.dismiss()
        create_sync_data(edit.Text)
       else
        addtable(edit.Text)
        添加卡片弹窗.dismiss()
        update("adp")--更新数据
        Has_up_write_config(edit.Text,"","")
      end
     else
      print("名称不能为空！")
    end
  end;

end


function message_card(table_name)
  管理卡片弹窗=
  {--布局
    LinearLayout;
    orientation="vertical";
    Focusable=true,
    FocusableInTouchMode=true,
    backgroundColor="#ffffff";
    {
      TextView;--文本控件
      gravity='center';--重力
      layout_marginTop='2%w';--顶距
      --左:left 右:right 中:center 顶:top 底:bottom
      layout_width='fill';--宽度
      layout_height='fill';--高度
      textColor='#ff1e8ae8';--文字颜色
      text=''..table_name;--显示文字
      textSize='20dp';--文字大小
    };
    {
      CardView;--卡片控件
      layout_margin='8dp';--边距
      layout_gravity='center';--重力
      --左:left 右:right 中:center 顶:top 底:bottom
      elevation='0dp';--阴影
      layout_width='fill';--宽度
      layout_height='10%w';--高度
      CardBackgroundColor='#598EDA';--颜色
      radius='8dp';--圆角
      id="play_card",
      {
        TextView;--文本控件
        gravity='center';--重力
        --左:left 右:right 中:center 顶:top 底:bottom
        layout_width='fill';--宽度
        layout_height='fill';--高度
        textColor='#ffffff';--文字颜色
        text='弹幕卡片';--显示文字
        textSize='14dp';--文字大小
      };
    };
    {
      CardView;--卡片控件
      layout_margin='8dp';--边距
      layout_gravity='center';--重力
      --左:left 右:right 中:center 顶:top 底:bottom
      elevation='0dp';--阴影
      layout_width='fill';--宽度
      layout_height='10%w';--高度
      CardBackgroundColor='#598EDA';--颜色
      radius='8dp';--圆角
      id="unplay_card",
      {
        TextView;--文本控件
        gravity='center';--重力
        --左:left 右:right 中:center 顶:top 底:bottom
        layout_width='fill';--宽度
        layout_height='fill';--高度
        textColor='#ffffff';--文字颜色
        text='取消弹幕';--显示文字
        textSize='14dp';--文字大小
      };
    };
    {
      CardView;--卡片控件
      layout_margin='8dp';--边距
      layout_gravity='center';--重力
      --左:left 右:right 中:center 顶:top 底:bottom
      elevation='0dp';--阴影
      layout_width='fill';--宽度
      layout_height='10%w';--高度
      CardBackgroundColor='#ff5f2e';--颜色
      radius='8dp';--圆角
      id="rename_card",
      {
        TextView;--文本控件
        gravity='center';--重力
        --左:left 右:right 中:center 顶:top 底:bottom
        layout_width='fill';--宽度
        layout_height='fill';--高度
        textColor='#ffffff';--文字颜色
        text='重命名卡片';--显示文字
        textSize='14dp';--文字大小
      };
    };
    {
      CardView;--卡片控件
      layout_margin='8dp';--边距
      layout_gravity='center';--重力
      --左:left 右:right 中:center 顶:top 底:bottom
      elevation='0dp';--阴影
      layout_width='fill';--宽度
      layout_height='10%w';--高度
      CardBackgroundColor='#F16B6F';--颜色
      radius='8dp';--圆角
      id="delete_card",
      {
        TextView;--文本控件
        gravity='center';--重力
        --左:left 右:right 中:center 顶:top 底:bottom
        layout_width='fill';--宽度
        layout_height='fill';--高度
        textColor='#ffffff';--文字颜色
        text='删除卡片';--显示文字
        textSize='14dp';--文字大小
      };
    };
  };
  管理卡片弹窗=AlertDialog.Builder(this).setView(loadlayout(管理卡片弹窗)).show()

  function update_play()
    if read_config(table_name) =="playing" then
      play_card.setVisibility(View.GONE)
      unplay_card.setVisibility(View.VISIBLE)
     else
      play_card.setVisibility(View.VISIBLE)
      unplay_card.setVisibility(View.GONE)
    end
    update()
    main_fun_a()
  end

  play_card.onClick=function()--点击事件
    Has_up_write_config(table_name,"playing","")
    管理卡片弹窗.dismiss()
    update_play()
  end;

  unplay_card.onClick=function()--点击事件
    Has_up_write_config(table_name,"unplay","")
    管理卡片弹窗.dismiss()
    update_play()
  end;
  update_play()
  --重命名
  rename_card.onClick=function()--点击事件
    InputLayout={
      LinearLayout;
      orientation="vertical";
      Focusable=true,
      FocusableInTouchMode=true,
      {
        EditText;
        hint="输入新的名称";
        layout_marginTop="5dp";
        layout_width="80%w";
        layout_gravity="center",
        id="rename_edit";
      };
    };

    AlertDialog.Builder(this)
    .setTitle("重命名")
    .setView(loadlayout(InputLayout))
    .setPositiveButton("确定",{onClick=function(v)
        --print(rename_edit.Text)
        if readHave_sync(table_name) == true then
          updata_sync(table_name,rename_edit.Text,-1)
        end
        rename_table(table_name,rename_edit.Text)
        update()
      end})
    .setNegativeButton("取消",nil)
    .show()

    管理卡片弹窗.dismiss()
  end;

  delete_card.onClick=function()--点击事件
    管理卡片弹窗.dismiss()
    AlertDialog.Builder(this)
    .setTitle("确定删除武器吗？")
    .setMessage("该操作会清空其下所有记录！")
    .setPositiveButton("取消",nil)
    .setNegativeButton("确定",{onClick=function(v)
        drop_table(table_name)
        if readHave_sync(table_name) == true then
          delete_record_sync(table_name)
        end
        delete_record_config(table_name)
        update()
      end})
    .show()
  end;

end
