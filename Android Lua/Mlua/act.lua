--[[用户点击具体某个表时，
显示表内容，及基础操作，导入导出，添加，删除，详情，清空等
]]

require "import"
import "android.app.*"
import "android.os.*"
import "android.widget.*"
import "android.view.*"
import "AndLua"

import "android.view.animation.ScaleAnimation"
import "android.animation.ObjectAnimator"
import "android.graphics.drawable.GradientDrawable"
import "android.graphics.drawable.GradientDrawable"
import "android.content.DialogInterface"
import "android.graphics.drawable.BitmapDrawable"
import "java.io.File"
import "android.database.sqlite.*"
--import 'files' --文件选择开源模块

layout=
{
  LinearLayout,--线性布局
  orientation="vertical",--布局方向
  layout_width="fill",--布局宽度
  layout_height="fill",--布局高度
  {
    CardView;--卡片控件
    layout_margin='0dp';--边距
    layout_gravity='center';--重力
    elevation='0dp';--阴影
    layout_width='fill';--宽度
    layout_height='fill';--高度
    CardBackgroundColor='0';--颜色
    radius='0dp';--圆角
    {
      ListView,--列表视图控件
      layout_width="fill",--布局宽度
      layout_height="wrap",--布局高度
      dividerHeight="1",--分割线高度
      verticalScrollBarEnabled=false,--隐藏滑条
      id="list",--控件ID
      layout_alignParentLeft=true,--重力居左
      layout_centerVertical=true,--将控件置于垂直方向的中心位置
    },
    {
      CardView;--卡片控件
      layout_margin='8dp';--边距
      id="rise_list";
      layout_gravity='right|bottom';--重力
      elevation='0dp';--阴影
      layout_width='wrap';--宽度
      layout_height='wrap';--高度
      CardBackgroundColor='0';--颜色
      radius='0dp';--圆角

      {
        LinearLayout,--线性布局
        orientation='vertical',--方向
        layout_width='fill',--宽度
        layout_height='fill',--高度
        background='0',--背景颜色或图片路径
        {
          CardView;--卡片控件
          layout_margin='8dp';--边距
          layout_gravity='center';--重力
          elevation='1dp';--阴影
          layout_width='13%w';--宽度
          layout_height='13%w';--高度
          CardBackgroundColor='#58C9B9';--颜色
          radius='8dp';--圆角
          id="cardview1",
          {
            ImageView;--图片控件
            layout_margin='3.5%w';--边距
            src='res/share.png';--图片路径
            layout_width='fill';--宽度
            layout_height='fill';--高度
            scaleType='fitXY';--图片显示类型
          };
        };
        {
          CardView;--卡片控件
          layout_margin='8dp';--边距
          layout_gravity='center';--重力
          elevation='1dp';--阴影
          layout_width='13%w';--宽度
          layout_height='13%w';--高度
          CardBackgroundColor='#a3a1a1';--颜色
          radius='8dp';--圆角
          id="cardview2",
          {
            ImageView;--图片控件
            layout_margin='3.5%w';--边距
            src='res/import.png';--图片路径
            layout_width='fill';--宽度
            layout_height='fill';--高度
            scaleType='fitXY';--图片显示类型
          };
        };
        {
          CardView;--卡片控件
          layout_margin='8dp';--边距
          layout_gravity='center';--重力
          elevation='1dp';--阴影
          layout_width='13%w';--宽度
          layout_height='13%w';--高度
          CardBackgroundColor='#f9320c';--颜色
          radius='8dp';--圆角
          id="cardview3",
          {
            ImageView;--图片控件
            layout_margin='3.5%w';--边距
            src='res/delete.png';--图片路径
            layout_width='fill';--宽度
            layout_height='fill';--高度
            scaleType='fitXY';--图片显示类型
          };
        };
        {
          CardView;--卡片控件
          layout_margin='8dp';--边距
          layout_gravity='center';--重力
          elevation='1dp';--阴影
          layout_width='13%w';--宽度
          layout_height='13%w';--高度
          CardBackgroundColor='#ff1e8ae8';--颜色
          radius='8dp';--圆角
          id="cardview4",
          {
            ImageView;--图片控件
            layout_margin='3.5%w';--边距
            src='res/rise.png';--图片路径
            layout_width='fill';--宽度
            layout_height='fill';--高度
            scaleType='fitXY';--图片显示类型
          };
        };
        {
          CardView;--卡片控件
          layout_margin='8dp';--边距
          layout_gravity='center';--重力
          elevation='1dp';--阴影
          layout_width='13%w';--宽度
          layout_height='13%w';--高度
          CardBackgroundColor='#A593E0';--颜色
          radius='8dp';--圆角
          id="fab",
          {
            ImageView;--图片控件
            layout_margin='3.5%w';--边距
            src='res/fab.png';--图片路径
            layout_width='fill';--宽度
            layout_height='fill';--高度
            scaleType='fitXY';--图片显示类型
          };
        };
      };
    };
  };
}--线性布局结束

activity.setTheme(R.Theme_Blue)
--activity.setTitle("桌面弹幕")
activity.setContentView(loadlayout(layout))

适配布局={
  LinearLayout,--线性布局
  orientation="vertical",--布局方向
  layout_width="fill",--布局宽度
  layout_height="wrap",--布局高度
  gravity="left|center",--重力居左｜置中
  padding="10dp",--布局填充
  {
    TextView,--文本框控件
    text="标题",--文本内容
    textSize="15sp",--文本大小
    textColor="#222222",--文本颜色
    id="wenben1",--控件ID
  },
  {
    TextView,--文本框控件
    text="简介",--文本内容
    textSize="12sp",--文本大小
    id="wenben2",--控件ID
  },
}

--数据库
db = SQLiteDatabase.openOrCreateDatabase(this.getLuaDir() .. "/bullet.db",MODE_PRIVATE, nil);

--execSQL()方法可以执行insert、delete、update和CREATE TABLE之类有更改行为的SQL语句
function exec(sql)
  db.execSQL(sql);
end

--rawQuery()方法用于执行select语句。
function raw(sql,text)
  cursor=db.rawQuery(sql,text)
end

--为表创建新纪录
function addrecord(tablename,newrecord,newremark)
  local newrecord=newrecord:gsub('&&',"•")
  local newremark=newremark:gsub('&&',"•")
  local sql="insert into "..tablename.."(cyan,remark) values('"..newrecord.."','"..newremark.."')"
  --提示(sql)
  if pcall(exec,sql) then
    --print("添加列成功")
   else
    print("添加列失败")
  end
end

cyan_list={}

--查询数据库表的记录
function sqlite_sel_tab(tablename)
  local sql="select * from "..tablename
  if pcall(raw,sql,nil) then
    adp_list.clear()--更新适配器
    while (cursor.moveToNext()) do
      local uid = cursor.getInt(0); --获取第一列的值,第一列的索引从0开始
      local cyan = cursor.getString(1);--获取第二列的值
      local remark = cursor.getString(2);--获取第三列的值
      adp_list.add{--构建视图控件
        wenben1=cyan,--标题
        wenben2=remark,--简介
      }
    end
    --提示(table.concat(cyan_list,","))
    cursor.close()
   else
    print("数据库查询失败！")
  end
  return uid
end

function delete_record(record_name)
  local sql="delete from "..to_table_name.." where cyan='"..record_name.."'"
  if pcall(exec,sql) then
    --print("删除成功")
   else
    print("删除失败")
  end
end

function clear_record(name)
  local sql="delete from "..name.." "
  if pcall(exec,sql) then
    sqlite_sel_tab(to_table_name)
    提示("已清空所有记录")
   else
    提示("清空失败")
  end
end

--传入cyan，返回remark
function cyan_remark(tablename,cyan)
  local sql="select * from "..tablename.." where cyan='"..cyan.."' "
  if pcall(raw,sql,nil) then
    while (cursor.moveToNext()) do
      cyan= cursor.getString(1); --获取第二列
      remark = cursor.getString(2);--获取第三列的值
      if remark==nil then --坑了我好久的坑，记录一下。
        remark="" --Lua读sqlite列，当列为nil空值时，传回来直接return报错。应该是不能return空值。加个判断就好了。
      end
    end
    cursor.close()
   else
    print("数据库返回信息失败！")
  end
  --提示(cyan.."re:"..remark)
  return remark
end

function main(name)
  activity.ActionBar.setTitle("全部记录："..name)
  sqlite_sel_tab(name)
  to_table_name=name
  import "Mlua/downimg"
  import "Mlua/input"
end


adp_list=LuaAdapter(activity,适配布局)
list.setAdapter(adp_list)

--远端或本地数据

--单击适配项目
list.onItemLongClick=function(a,b)
  delete_record(b.Tag.wenben1.Text)
  提示("已删除记录")--标题
  sqlite_sel_tab(to_table_name)
  return true--返回
end

list.onItemClick=function(a,b)
  activity.newActivity("Mlua/edit",{b.Tag.wenben1.Text.."\n"..cyan_remark(to_table_name,b.Tag.wenben1.Text)})--跳转页面
end

cardview1.onClick=function() --分享,导出
  --文件写入
  保存弹窗.show()
end

cardview2.onClick=function() --导入
  导入弹窗.show()
end

cardview3.onClick=function() --清空
  AlertDialog.Builder(this)
  .setTitle("确定清空弹夹吗？")
  .setMessage("该操作会清空所有记录！")
  .setPositiveButton("取消",nil)
  .setNegativeButton("确定",{onClick=function(v) clear_record(to_table_name) end})
  .show()
end

cardview4.onClick=function() --新增
  import "Mlua/rise_win"
  添加记录弹窗.show()
end

--作者 Error

cardview1.setVisibility(View.INVISIBLE)
cardview2.setVisibility(View.INVISIBLE)
cardview3.setVisibility(View.INVISIBLE)
cardview4.setVisibility(View.INVISIBLE)


fab_bck=GradientDrawable() fab_bck.setShape(GradientDrawable.OVAL) fab_bck.setColor(0xff60c4ba) fab_bck.setCornerRadii({90,90,90,90,90,90,90,90})
W=activity.width H=activity.height
fab.onClick=function(v)

  ObjectAnimator().ofFloat(v,"scaleX",{1.2,.8,1.1,.9,1}).start() ObjectAnimator().ofFloat(v,"scaleY",{1.2,.8,1.1,.9,1}).start()
  if cardview1.getVisibility()==0 then
    import "android.view.animation.Animation$AnimationListener"
    import "android.view.animation.ScaleAnimation"
    cardview1.startAnimation(ScaleAnimation(1.0, 0.0, 1.0, 0.0,1, 0.5, 1, 0.5).setDuration(500))
    cardview1.setVisibility(View.INVISIBLE)
    cardview2.startAnimation(ScaleAnimation(1.0, 0.0, 1.0, 0.0,1, 0.5, 1, 0.5).setDuration(400))
    cardview2.setVisibility(View.INVISIBLE)
    cardview3.startAnimation(ScaleAnimation(1.0, 0.0, 1.0, 0.0,1, 0.5, 1, 0.5).setDuration(300))
    cardview3.setVisibility(View.INVISIBLE)
    cardview4.startAnimation(ScaleAnimation(1.0, 0.0, 1.0, 0.0,1, 0.5, 1, 0.5).setDuration(200))
    cardview4.setVisibility(View.INVISIBLE)
   else
    import "android.view.animation.ScaleAnimation"
    cardview1.setVisibility(View.VISIBLE)
    cardview2.setVisibility(View.VISIBLE)
    cardview3.setVisibility(View.VISIBLE)
    cardview4.setVisibility(View.VISIBLE)

    cardview1.startAnimation(ScaleAnimation(0.0, 1.0, 0.0, 1.0,1, 0.5, 1, 0.5).setDuration(200))
    cardview2.startAnimation(ScaleAnimation(0.0, 1.0, 0.0, 1.0,1, 0.5, 1, 0.5).setDuration(300))
    cardview3.startAnimation(ScaleAnimation(0.0, 1.0, 0.0, 1.0,1, 0.5, 1, 0.5).setDuration(400))
    cardview4.startAnimation(ScaleAnimation(0.0, 1.0, 0.0, 1.0,1, 0.5, 1, 0.5).setDuration(500))
  end
end
