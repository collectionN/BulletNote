--启动事件
--write_config("bullet_state","","false")
--print(type(read_config("bullet_state")))
--点击事件

--底部按钮点击事件
m1.onClick=function() --功能点击选择
  zy.showPage(0)
  color("m1")
end

m2.onClick=function()
  zy.showPage(1)
  color("m2")
end


m3.onClick=function()
  zy.showPage(2)
  color("m3")
end

search.onClick=function()
  activity.newActivity("Mlua/search")--跳转页面
end

--监听滑动窗口状态
appp=activity.getWidth()
local kuan=appp/4
zy.setOnPageChangeListener(PageView.OnPageChangeListener{
  onPageScrolled=function(a,b,c)
    if c==0 then
      if a==0 then
        color("m1")
        标题.setText("首页")
        --enlarge("m1")
       elseif a==1 then
        color("m2")
        -- enlarge()
        标题.setText("弹幕")
       elseif a==2 then
        color("m3")
        标题.setText("我的")
      end
    end
  end})

窗口 = activity.getSystemService(Context.WINDOW_SERVICE)

--布局内响应
function 悬浮窗开关.onClick()
  if(read_config("faw")==true)then
    窗口.removeView(悬浮窗)
    载入悬浮球()
    Has_up_write_config("faw","","false")
  end
end

悬浮球图标.onLongClick=function()
  if(read_config("fab")==true)then
    窗口.removeView(悬浮球)
    Has_up_write_config("fab","","false")
  end
  if(read_config("faw")==true)then
    窗口.removeView(悬浮窗)
    Has_up_write_config("faw","","false")
  end
end

function 悬浮球图标.onClick
  if(read_config("faw")==false)then
    载入悬浮窗()
    --xfc_edit_cyan.setFocusable(true)
    --xfc_edit_remark.setFocusable(true)
    xfc_list_table.setVisibility(View.GONE) --隐藏表名列表卡片
    xfc_list_table_fun() --数据库读取表面并适配列表
  end
end

function 开关本体.onClick()
  get_permission()
  if read_config("fab")==false then
    载入悬浮球()
    MD提示("已开启悬浮",0xFFFFFFFF,0x9C000000,4,10)
    translate_today()
    Has_up_write_config("fab","","true")
   else
    关闭悬浮球()
    MD提示("已关闭悬浮",0xFFFFFFFF,0x9C000000,4,10)
    Has_up_write_config("fab","","false")
  end
end

--开启弹幕传参
--[[最开始使用了属性动画，多个函数间相互传参。最后在动画监听部分一直
提示冲突，也没找到办法，于是改成了补间动画。

再干脆把所有事件写一起算了，反正机器读而已。
]]

function ballistic() --弹道一事件
  home_view()
  local random=math.random(1,#home_list_name) --随机数取当前弹幕序列中的卡片
  bullet_text.setText(random_sqlite(home_list_name[random])) --设置随机记录到Text上去
  bullet_cars.measure(View.MeasureSpec.makeMeasureSpec(0,View.MeasureSpec.UNSPECIFIED),View.MeasureSpec.makeMeasureSpec(0,View.MeasureSpec.UNSPECIFIED));
  local width =0-(bullet_cars.getMeasuredWidth()); --获取设置文字后的控件宽度。这里需注意，必须在setText下面，否则读不到正常宽度。
  bullet_cars.startAnimation(TranslateAnimation(获取屏幕宽(),width,0,0).setDuration(20000).setFillAfter(true).setAnimationListener(AnimationListener{
    onAnimationEnd=function() --设置补间动画结束监听
      if read_config("bullet_state") == true then
        ballistic()
      end
    end}))
  if read_config("log")==true then
    local log= "弹道1,表:"..home_list_name[random].."内容："..random_sqlite(home_list_name[random]).."弹幕状态："..tostring(read_config("bullet_state"))
    addrecord("log","弹道一",log)
  end
end

-- 可能循环用定时器写比较优雅，但是我不大会玩，都是现学。将就将就，自己用而已
-- 以下函数同上

function ballistic2()
  home_view()
  local random=math.random(1,#home_list_name)
  bullet_cars2.setVisibility(View.GONE) --开始时隐藏它
  task(2500,function()--1000毫秒=1秒。延迟一会，增加层次感
    bullet_cars2.setVisibility(View.VISIBLE) --延迟后再显示出来
    bullet_text2.setText(random_sqlite(home_list_name[random]))
    bullet_cars2.measure(View.MeasureSpec.makeMeasureSpec(0,View.MeasureSpec.UNSPECIFIED),View.MeasureSpec.makeMeasureSpec(0,View.MeasureSpec.UNSPECIFIED));
    local width =0-(bullet_cars2.getMeasuredWidth());
    bullet_cars2.startAnimation(TranslateAnimation(获取屏幕宽(),width,0,0).setDuration(20000).setFillAfter(true).setAnimationListener(AnimationListener{
      onAnimationEnd=function()
        if read_config("bullet_state") == true then
          ballistic2()
        end
      end}))
  end)
  if read_config("log")==true then
    local log= "弹道2,表:"..home_list_name[random].."内容："..random_sqlite(home_list_name[random]).."弹幕状态："..tostring(read_config("bullet_state"))
    addrecord("log","弹道2",log)
  end
end

--横屏监听奇奇怪怪的，这个是个坑，还未来得及填
--用户横屏后弹幕获取的屏幕宽仍然是竖屏时的高度

--[[

function isHorizontal()
  if activity.width>activity.height
    --横屏
    push_notise(3,"弹幕已关闭","横屏多为游戏电影场景，已关闭弹幕，点击再次开启","nolong")
    clear_push(1)
    sys_close()
    窗口.removeView(bullet2)
    窗口.removeView(bullet)
    Has_up_write_config("bullet_state","","false")
    print("h")
   else
    --竖屏
    print("w")
  end
end
isHorizontal()
--横屏监听
currentOrientation=activity.resources.configuration.orientation
oldOrientation=currentOrientation
luajava.override(OrientationListener,{
  onOrientationChanged=function(_,orientation)
    currentOrientation=activity.resources.configuration.orientation
    if currentOrientation~=oldOrientation
      isHorizontal()
    end
    oldOrientation=currentOrientation
  end}).enable()
]]

--开启弹幕
function start_bullet.onClick(bullet_name)
  get_permission() --检查权限
  if #home_list_name~=0 then
    if read_config("bullet_state") == false then
      ballistic()
      ballistic2()
      窗口.addView(bullet,弹幕窗口容器)
      窗口.addView(bullet2,弹幕窗口容器1)
      push_notise(1,"弹幕笔记","弹幕运行中...","long")
      MD提示("弹幕开启~",0xFFFFFFFF,0x9C000000,4,10)
      start_bullet.setBackgroundColor(Color.parseColor("#61649f"))
      start_bullet_tip.setText("弹幕开启啦~")
      Has_up_write_config("bullet_state","","true") --更新数据库配置状态
     else
      Has_up_write_config("bullet_state","","false")
      MD提示("已关闭弹幕",0xFFFFFFFF,0x9C000000,4,10)
      mNManager.cancelAll()
      窗口.removeView(bullet)
      窗口.removeView(bullet2)
      start_bullet_tip.setText("弹幕关闭了诶┐(´-｀)┌")
      start_bullet.setBackgroundColor(Color.parseColor("#a3a1a1"))
    end
   else
    提示("没有卡片在弹幕序列中，请添加.")
  end
end

--反转动画
users.onClick=function()--点击事件
  if reversal==true then
    hide_users.setVisibility(View.GONE) --隐藏我的页面信息卡片
    取消反转(hide_users)
    取消反转(users)
    users.setVisibility(View.VISIBLE)
   else
    hide_users.setVisibility(View.VISIBLE) --隐藏我的页面信息卡片
    users.setVisibility(View.GONE) --隐藏我的页面信息卡片
    --开始反转(users)
    --[[hide_users.startAnimation(AlphaAnimation(0,1).setDuration(500).setFillAfter(true).setAnimationListener(AnimationListener{
      onAnimationEnd=function()
      end}))]]
    reversal=true
  end
end

hide_users.onClick=function()--重命名卡片隐藏点击事件
  if reversal==true then
    hide_users.setVisibility(View.GONE) --隐藏我的页面信息卡片
    取消反转(hide_users)
    取消反转(users)
    users.setVisibility(View.VISIBLE)
   else
    开始反转(users)
    hide_users.startAnimation(AlphaAnimation(0,1).setDuration(500).setFillAfter(true).setAnimationListener(AnimationListener{
      onAnimationEnd=function()
        hide_users.setVisibility(View.VISIBLE) --隐藏我的页面信息卡片
        users.setVisibility(View.GONE) --隐藏我的页面信息卡片
      end}))
  end
end;

--添加武器页面

add_table.onClick=function()--点击事件
  add_card()
  添加卡片弹窗.show()
end;

refresh.onClick=function()--点击事件
  zy.showPage(1)
end;

--悬浮窗表名列表点击事件
Control_xfc_list_table.onClick=function()
  if xfc_list_table.getVisibility()==8 then
    xfc_list_table.setVisibility(View.VISIBLE)
   else
    xfc_list_table.setVisibility(View.GONE)
  end
end;

--悬浮窗添加记录
xfc_edit_rise.onClick=function()--点击事件
  if xfc_list_name~=nil then
    if xfc_edit_cyan.Text ~= "" then
      if count_zh_en(xfc_edit_cyan.Text)<=20 and count_zh_en(xfc_edit_remark.Text)<=20 then
        if Grt1.isChecked() == true then
          local c=xfc_edit_cyan.Text
          local c=string.gsub(c,string.format("%.1s",c),string.upper(string.format("%.1s",c)),1)
          addrecord(xfc_list_name,c,xfc_edit_remark.Text)
          get_and_to(xfc_list_title,"成功添加记录,首字母已大写~")
          xfc_edit_cyan.setText('')
          xfc_edit_remark.setText('')
         else
          addrecord(xfc_list_name,xfc_edit_cyan.Text,xfc_edit_remark.Text)
          get_and_to(xfc_list_title,"成功添加记录~")
          xfc_edit_cyan.setText('')
          xfc_edit_remark.setText('')
        end
       else
        get_and_to(xfc_list_title,"记录过长了哦，删掉一点吧~")
      end
     else
      get_and_to(xfc_list_title,"主字段不能为空.")
    end
   else
    get_and_to(xfc_list_title,"未选择添加到哪个表呢~")
  end
end;

--重置内容
xfc_edit_clear.onClick=function()--点击事件
  xfc_edit_cyan.setText('')
  xfc_edit_remark.setText('')
  get_and_to(xfc_list_title,"已清空内容")
end;

--重命名我的信息区域
rename.onClick=function()--点击事件
  if rename_edit.Text~="" then
    if count_zh_en(rename_edit.Text)<=9 then
      Has_up_write_config("users_name",rename_edit.Text,"true")
      MD提示("重命名成功~",0xFF2196F3,0xFFFFFFFF,4,10)
      user_name()
     else
      提示("昵称不要超过6个字呢~")
    end
   else
    提示("名称不能为空哦~")
  end
  --转回去
  if reversal==true then
    hide_users.setVisibility(View.GONE) --隐藏我的页面信息卡片
    取消反转(hide_users)
    取消反转(users)
    users.setVisibility(View.VISIBLE)
   else
    开始反转(users)
    hide_users.startAnimation(AlphaAnimation(0,1).setDuration(500).setFillAfter(true).setAnimationListener(AnimationListener{
      onAnimationEnd=function()
        hide_users.setVisibility(View.VISIBLE) --隐藏我的页面信息卡片
        users.setVisibility(View.GONE) --隐藏我的页面信息卡片
      end}))
  end
end;

--悬浮窗翻译中英文切换事件
xfc_trans_zh.onClick=function()--点击事件
  xfc_trans_zh.setVisibility(View.GONE)
  xfc_trans_en.setVisibility(View.VISIBLE)
end;

xfc_trans_en.onClick=function()--点击事件
  xfc_trans_en.setVisibility(View.GONE)
  xfc_trans_zh.setVisibility(View.VISIBLE)
end;

--翻译事件
xfc_trans_start.onClick=function()--点击事件
  if xfc_edit_trans.Text~="" then
    summary.setText("...")
    Http.get(translator(xfc_edit_trans.Text),nil,"utf-8",nil,function(code,content)
      if code==200 then
        tool_summary.setVisibility(View.VISIBLE)
        local data=json.decode(content)
        local translate=data["trans_result"][1]["dst"]
        local translate_src=data["trans_result"][1]["src"]
        summary.setText(translate_src.."\n\n"..translate)
        copy_summary.onClick=function()--点击事件
          --activity.getSystemService(Context.CLIPBOARD_SERVICE).setText(translate)
          写入剪切板(translate)
          get_and_to(summary,"已复制~")
        end;
        tts_summary.onClick=function()--点击事件
          tts(translate)
        end;
       else
        summary.setText("API请求错误"..code)
      end
    end)
   else
    get_and_to(summary,"客官没有输入呢~")
  end
  缩放动画(xfc_trans_start)
end;


trans_edit_clear.onClick=function()--点击事件
  xfc_edit_trans.setText("")
end;

help_tip.onClick=function()--点击事件
  activity.newActivity("Mlua/browser",{"关于我们"})--跳转页面
end;

--结束程序
over_procedure.onClick=function()--点击事件
  mNManager.cancelAll()
  os.exit()
end;

open_source.onClick=function()--点击事件
  activity.newActivity("Mlua/browser",{"https://blog.nrhs.eu.org/2022/11/12/BulletNote_open-source/"})--跳转页
end;

function progress_setting()
  AlertDialog.Builder(this)
  .setTitle("隐藏任务卡片")
  .setMessage("从最近任务程序中隐藏本应用卡片")
  .setPositiveButton("隐藏卡片",{onClick=function(v)
      hidden_progress(true)
      提示("后台已隐藏")
    end})
  .setNeutralButton("取消隐藏",{onClick=function(v)
      hidden_progress(false)
      提示("后台取消隐藏")
    end})
  .setNegativeButton("取消",nil)
  .show()
end

power_manage.onClick=function()--开启
  progress_setting()
end;