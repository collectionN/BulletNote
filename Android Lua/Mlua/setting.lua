require "import"
import "android.app.*"
import "android.os.*"
import "android.widget.*"
import "android.view.*"
import "layout"
import "AndLua"
import "android.database.sqlite.*"

function 浏览器打开(链接)
  import "android.content.Intent"
  import "android.net.Uri"
  viewIntent = Intent("android.intent.action.VIEW",Uri.parse(链接))
  activity.startActivity(viewIntent)
end

layout=
{
  LinearLayout,--线性布局
  orientation='vertical',--方向
  layout_width='fill',--宽度
  layout_height='wrap',--高度
  background='#00FFFFFF',--背景颜色或图片路径
  --标题介绍适配器
  {
    GridView;--网格控件
    id="tit_ind_set",
    layout_width='fill';--网格宽度
    layout_height='fill';--网格高度
    --Gravity='center';--对齐方式
    NumColumns='1';--列数
    ColumnWidth='wrap';--每一列的宽度
    --StretchMode='1';--缩放
    --HorizontalSpacing='3%w';--两列直接的边距
    --VerticalSpacing='1';--两行之间的间距
    VerticalScrollBarEnabled=false;--隐藏竖向滑条
  };
  --
};

activity.setTheme(R.Theme_Orange)
activity.setContentView(loadlayout(layout))


item1={
  LinearLayout,--线性布局
  orientation='vertical',--方向
  layout_width='fill',--宽度
  layout_height='wrap',--高度
  background='#00FFFFFF',--背景颜色或图片路径
  {
    CardView;--卡片控件
    layout_margin='8dp';--边距
    layout_gravity='center';--重力
    --左:left 右:right 中:center 顶:top 底:bottom
    elevation='0dp';--阴影
    layout_width='fill';--宽度
    layout_height='wrap';--高度
    CardBackgroundColor='#f1f0ed';--颜色
    radius='8dp';--圆角
    {
      LinearLayout,--线性布局
      orientation='vertical',--方向
      layout_width='fill',--宽度
      layout_height='fill',--高度
      background='#00FFFFFF',--背景颜色或图片路径
      {
        CardView;--卡片控件
        layout_margin='0dp';--边距
        layout_gravity='center';--重力
        --左:left 右:right 中:center 顶:top 底:bottom
        elevation='0dp';--阴影
        layout_width='fill';--宽度
        layout_height='10%w';--高度
        CardBackgroundColor='#A593E0';--颜色
        radius='0dp';--圆角
        {
          TextView;--文本控件
          layout_marginLeft='2%w';--左距
          gravity='center|left';--重力
          --左:left 右:right 中:center 顶:top 底:bottom
          layout_width='fill';--宽度
          layout_height='fill';--高度
          textColor='#ffffff';--文字颜色
          text='';--显示文字
          textSize='14dp';--文字大小
          id="title_set";
        };
      };
      {
        TextView;--文本控件
        gravity='center|left';--重力
        layout_margin='8dp';--左距
        layout_width='fill';--宽度
        layout_height='fill';--高度
        textColor='#474b4c';--文字颜色
        text='';--显示文字
        textSize='14dp';--文字大小
        id="ind_set";
      };
    };
  };
};


function GetAppInfo(包名)
  local 版本号 = activity.getPackageManager().getPackageInfo(包名, 0).versionName
  local 最后更新时间 = activity.getPackageManager().getPackageInfo(包名, 0).lastUpdateTime
  local cal = Calendar.getInstance();
  cal.setTimeInMillis(最后更新时间);
  local 最后更新时间 = cal.getTime().toLocaleString()
  --return 包名,版本号,最后更新时间
  return "当前版本: "..版本号.."\n最后更新于: "..最后更新时间
end

function clear()
  --导入File类
  import "java.io.File"
  --显示多选框
  items={"清除缓存","所有文件！丢失所有数据！"}
  多选对话框=AlertDialog.Builder(this)
  .setTitle("清除记录")
  --勾选后执行
  .setPositiveButton("确定",function()
    if clearhistory==1 and clearall==1 then
      File(lstads).delete()
      File(lstwebads).delete()
      lst={}
      lstweb={}
      os.execute("pm clear "..activity.getPackageName())
     elseif clearhistory==0 and clearall==1 then
      os.execute("pm clear "..activity.getPackageName())
     elseif clearhistory==1 and clearall==0 then
      File(lstads).delete()
      File(lstwebads).delete()
      lst={}
      lstweb={}
     else return nil
    end
  end)
  --选择事件
  .setMultiChoiceItems(items, nil,{ onClick=function(v,p)
      --清除历史
      if p==0 then clearhistory=1
      end
      --清除缓存
      if p==1 then clearall=1
      end
    end})
  多选对话框.show();
  clearhistory=0
  clearall=0
end


--数据库
db = SQLiteDatabase.openOrCreateDatabase(this.getLuaDir() .. "/bullet.db",MODE_PRIVATE, nil);

--execSQL()方法可以执行insert、delete、update和CREATE TABLE之类有更改行为的SQL语句
function exec(sql)
  db.execSQL(sql);
end

--rawQuery()方法用于执行select语句。
function raw(sql,text)
  cursor=db.rawQuery(sql,text)
end

--创建表
function addtable(tablename)
  local CreatrTableSql="create table "..tablename.."(id integer primary key,cyan varchar,remark varchar)"
  if pcall(exec,CreatrTableSql) then
    --创建user表，integer类型为自动增长
    print("创建成功")
   else
    print("创建失败")
  end
end

--删除表
function drop_table(table_name)
  local sql="drop table "..table_name
  if pcall(exec,sql) then
    MD提示("删除表成功",0xFF2196F3,0xFFFFFFFF,4,10)
   else
    print("删除表失败")
  end
end

--为配置表创建新配置
function write_config(name,value,bool_f) --应该尽量避免value与bool_f同时传入，不太方便判断
  --name为主键，必须传入，即配置项的名称；value是值，varchar类型；bool_f为布尔值，一般传入布尔值即可
  local sql="insert into config(name,value,bool) values('"..name.."','"..value.."','"..bool_f.."')"
  --print(sql)
  if pcall(exec,sql) then
    --MD提示("配置读写成功",0xFF2196F3,0xFFFFFFFF,4,10)
   else
    print("配置写入失败!")
  end
end

--为配置表更新配置
function updata_config(name,value,bool_f) --应该尽量避免value与bool_f同时传入，不太方便判断
  --name为主键，必须传入，即配置项的名称；value是值，varchar类型；bool_f为布尔值，一般传入布尔值即可
  local sql=("UPDATE config SET value = '"..value.."' , bool = '"..bool_f.."' WHERE name='"..name.."';")
  --print(sql)
  if pcall(exec,sql) then
    --MD提示("配置更新成功",0xFF2196F3,0xFFFFFFFF,4,10)
   else
    print("更新配置读写失败!")
  end
end

--传入名称先判定配置项是否存在
function readHave_config(recig_name)
  local sql="select * from config where name='"..recig_name.."' "
  if pcall(raw,sql,nil) then
    if cursor.moveToFirst() ~= true then
      return false --当数据库中不存在name时返回false
    end
   else
    print("数据库判定记录读写失败")
  end
end


--数据库判定记录是否存在，分别进行更新与写入记录操作
function Has_up_write_config(name,value,bool_f)
  if readHave_config(name) ~= false then
    updata_config(name,value,bool_f)
   else
    write_config(name,value,bool_f)
  end
end

--读取配置
--传入配置项名称，返回值与类型。只设置其一的话较好判断
function read_config(name)
  if readHave_config(name) == false then
    write_config(name,"","false")
  end
  local sql=("SELECT * FROM config where name='"..name.."'")
  if pcall(raw,sql,nil) then
    while (cursor.moveToNext()) do
      value= cursor.getString(1); --获取第二列
      bool_f = cursor.getString(2);--获取第三列的值
    end
    cursor.close()
   else
    print("数据库配置信息读取失败！")
  end
  if value =="" and bool_f=="false" then
    return false
   elseif value =="" and bool_f=="true" then
    return true
   elseif value ~="" and bool_f=="true" then
    return value
   elseif value ~= "" and bool_f=="" then
    return value:gsub(" ","")
   else
    return value.."%"..bool_f --%为间隔符
  end
end

--[[GetAppInfo("com.nrhs.deskbullet")
--import "android.content.FileProvider"
contentUri=FileProvider.getUriForFile(activity,activity.getPackageName()..".FileProvider",File(file))
intent.setDataAndType(contentUri,"application/vnd.android.package-archive")
activity.startActivity(intent)]]

function recover()
  AlertDialog.Builder(this)
  .setTitle("确认重载配置表吗？")
  .setMessage("不会清除数据")
  .setPositiveButton("确认",{onClick=function(v)
      local sql="drop table config"
      if pcall(exec,sql) then
        MD提示("重置配置表成功,请重启应用.",0xFF2196F3,0xFFFFFFFF,4,10)
       else
        提示("数据库读写失败！")
      end
      activity.finish()
    end})
  .setNeutralButton("查看配置表",{onClick=function(v)
      local sql=("SELECT * FROM config")
      if pcall(raw,sql,nil) then
        local content={}
        while (cursor.moveToNext()) do
          local name= cursor.getString(0); --获取第二列
          local value= cursor.getString(1); --获取第二列
          local bool_f = cursor.getString(2);--获取第三列的值
          table.insert(content,name.."&"..value.."&"..bool_f)
        end
        cursor.close()
        activity.newActivity("Mlua/edit",{table.concat(content,"\n")})--跳转页面
       else
        print("数据库配置信息读取失败！")
      end
    end})
  .setNegativeButton("取消",nil)
  .show()
end


adp_list_1=LuaAdapter(activity,item1)
tit_ind_set.setAdapter(adp_list_1)

总列表=[[
<重置配置表>
《当程序异常的时候，使用此重置配置。但不会删除数据。》

<清除缓存>
《程序异常时清除缓存，焕然如新~》

<下载路径>
《sdcard/弹幕笔记/导出文件/》

<开启日志>
《记录弹幕详情，抓BUG》

<软件版本>
《1.997》
]]

local name_list=总列表:gmatch("<(.-)>\n《(.-)》")--截取格式
for a,b in name_list do--循环取值
  if a=="软件版本" then
    b=GetAppInfo("com.nrhs.deskbullet")
  end
  adp_list_1.add{--构建视图控件
    title_set=a,--图标
    ind_set=b,--标题
  }
end

tit_ind_set.onItemClick=function(a,b)
  local name=b.Tag.title_set.Text
  if name=="重置配置表" then
    recover()
   elseif name=="清除缓存" then
    clear()
   elseif name=="软件版本" then
    浏览器打开("http://pan.nrhs.eu.org/K%E7%9A%84%E8%89%BA%E6%9C%AF/Resource/DeskBullet")
   elseif name=="开启日志" then
    if read_config("log")==true then
      drop_table("log")
      提示("log日志已关闭！")
      Has_up_write_config("log","","false")
     else
      确定弹窗("弹幕日志？","非必要不开启，开启后log表会迅速增长，记得关闭~","已知晓",function()
        addtable("log")
        提示("log日志已开启！")
        Has_up_write_config("log","","true")
      end)
    end
  end
  return true--返回
end