require "import"
import "android.app.*"
import "android.os.*"
import "android.widget.*"
import "android.view.*"
import "layout"
import "AndLua"
import "android.database.sqlite.*"

layout=--全屏框架
{
  LinearLayout;--线性控件
  orientation='vertical';--布局方向
  layout_width='fill';--布局宽度
  layout_height='fill';--布局高度
  gravity='vertical';--控件内容的重力方向
  {
    CardView;--卡片控件
    layout_margin='8dp';--边距
    layout_gravity='center';--重力
    --左:left 右:right 中:center 顶:top 底:bottom
    elevation='5dp';--阴影
    layout_width='fill';--宽度
    layout_height='15%w';--高度
    CardBackgroundColor='#ffffffff';--颜色
    radius='8dp';--圆角
    {
      SearchView;--搜索控件
      layout_width='fill';--控件宽度
      layout_height='fill';--控件高度
      id='Search';
      queryHint='输入笔记关键字';--提示
      --conifiedByDefault=false;--搜索图标内外
      maxWidth='fill';--最大宽度
      --inputType='number';--设置仅数字输入
    };
  };
  {
    CardView;--卡片控件
    layout_margin='0dp';--边距
    layout_gravity='center';--重力
    elevation='0dp';--阴影
    layout_width='fill';--宽度
    layout_height='fill';--高度
    CardBackgroundColor='0';--颜色
    radius='0dp';--圆角
    {
      ListView,--列表视图控件
      layout_width="fill",--布局宽度
      layout_height="wrap",--布局高度
      dividerHeight="1",--分割线高度
      verticalScrollBarEnabled=false,--隐藏滑条
      id="list",--控件ID
      layout_alignParentLeft=true,--重力居左
      layout_centerVertical=true,--将控件置于垂直方向的中心位置
    },
  },
}

activity.setTheme(R.Theme_Blue)
activity.setContentView(loadlayout(layout))

适配布局={
  LinearLayout,--线性布局
  orientation="vertical",--布局方向
  layout_width="fill",--布局宽度
  layout_height="wrap",--布局高度
  gravity="left|center",--重力居左｜置中
  padding="10dp",--布局填充
  {
    TextView,--文本框控件
    text="标题",--文本内容
    textSize="15sp",--文本大小
    textColor="#222222",--文本颜色
    id="wenben1",--控件ID
  },
  {
    TextView,--文本框控件
    text="简介",--文本内容
    textSize="15sp",--文本大小
    textColor="#222222",--文本颜色
    id="wenben2",--控件ID
  },
  {
    TextView,--文本框控件
    text="信息",--文本内容
    textSize="12sp",--文本大小
    id="wenben3",--控件ID
  },
}

adp_list=LuaAdapter(activity,适配布局)
list.setAdapter(adp_list)
--数据库
db = SQLiteDatabase.openOrCreateDatabase(this.getLuaDir() .. "/bullet.db",MODE_PRIVATE, nil);
search_text=""
--execSQL()方法可以执行insert、delete、update和CREATE TABLE之类有更改行为的SQL语句
function exec(sql)
  db.execSQL(sql);
end

--rawQuery()方法用于执行select语句。
function raw(sql,text)
  cursor=db.rawQuery(sql,text)
end

table_list={}

function table_select()
  local sql="SELECT name from config WHERE name NOT IN('bullet_state','users_name','fab','faw')"
  if pcall(raw,sql,nil) then
    while (cursor.moveToNext()) do
      name= cursor.getString(0); --获取第二列
      if name==nil then --坑了我好久的坑，记录一下。
        name="" --Lua读sqlite列，当列为nil空值时，传回来直接return报错。应该是不能return空值。加个判断就好了。
      end
      table.insert(table_list,name)
    end
    cursor.close()
   else
    print("数据库返回信息失败！")
  end
end

--模糊查询数据库表的记录
function sqlite_sel_tab(tablename,content)
  --提示("搜索"..tablename..content)
  local sql="SELECT * from `"..tablename.."` WHERE cyan like '%"..content.."%' or remark like '%"..content.."%'"
  if pcall(raw,sql,nil) then
    while (cursor.moveToNext()) do
      local uid = cursor.getInt(0); --获取第一列的值,第一列的索引从0开始
      local cyan = cursor.getString(1);--获取第二列的值
      local remark = cursor.getString(2);--获取第三列的值
      adp_list.add{--构建视图控件
        wenben1=cyan,--标题
        wenben2=remark,--简介
        wenben3=tablename
      }
    end
    cursor.close()
   else
    print("数据库查询失败！")
  end
end

-- 传入文字进行模糊搜索
function search_list(str)
  adp_list.clear()--更新适配器
  for i in pairs(table_list)
    --提示(table_list[i])
    sqlite_sel_tab(table_list[i],str)
  end
  list.setAdapter(adp_list)
end

function delete_record(book,record_name)
  local sql="delete from "..book.." where cyan='"..record_name.."'"
  if pcall(exec,sql) then
    --print("删除成功")
   else
    print("删除失败")
  end
end

--单击适配项目
list.onItemLongClick=function(a,b)
  delete_record(b.Tag.wenben3.Text,b.Tag.wenben1.Text)
  提示("已删除记录")--标题
  search_list(search_text)
  return true--返回
end

list.onItemClick=function(a,b)
  activity.newActivity("Mlua/edit",{b.Tag.wenben1.Text.."\n"..b.Tag.wenben2.Text.."\n"})--跳转页面
end

--常规设置
--Search.setMaxWidth(500);--设置最大宽度
--Search.setSubmitButtonEnabled(true);--设置是否显示搜索框展开时的提交按钮
--Search.setQueryHint('hint');--设置输入框为空时提示的文本
--Search.requestFocus();--获取焦点
--Search.clearFocus();--失去焦点

--SearchView有三种默认展开搜索框的设置方式,区别如下：

--设置搜索框直接展开显示,左侧有放大镜(在搜索框中),右侧有叉叉,可以关闭搜索框
--Search.setIconified(false);

--设置搜索框直接展开显示,左侧有放大镜(在搜索框外),右侧无叉叉,有输入内容后有叉叉,不能关闭搜索框
--Search.setIconifiedByDefault(false);

--设置搜索框直接展开显示,左侧有无放大镜(在搜索框中),右侧无叉叉,有输入内容后有叉叉,不能关闭搜索框
Search.onActionViewExpanded();


--设置搜索框有字时显示叉叉，无字时隐藏叉叉
--Search.onActionViewExpanded();
--Search.setIconified(true);


--事件监听
--搜索框展开时后面叉叉按钮的点击事件
Search.setOnCloseListener{
  onClose=function(v)
    print('关闭')
  end};

--搜索图标按钮(打开搜索框的按钮)的点击事件
Search.setOnSearchClickListener{
  onClick=function(v)
    print('打开')
  end};

--搜索框文字变化监听
Search.setOnQueryTextListener(SearchView.OnQueryTextListener{
  onQueryTextSubmit=function(str)--右下角搜索的监听提交事件
    print('提交'..str)
    search_text=str
    search_list(str)
  end,
  onQueryTextChange=function(str)--输入文字监听
    print('改变'..str)
    search_text=str
    search_list(str)
  end
})

--隐藏下划线,背景变透明
function SearchColor(id)
  import "android.graphics.Color"
  local argClass = id.getClass();
  --指定某个私有属性,mSearchPlate是搜索框父布局的名字
  local ownField = argClass.getDeclaredField("mSearchPlate");
  --暴力反射,只有暴力反射才能拿到私有属性
  ownField.setAccessible(true);
  local mView = ownField.get(id);
  --设置背景,透明
  mView.setBackgroundColor(Color.TRANSPARENT);
end


--调用
SearchColor(Search)

function main(name)
  activity.ActionBar.setTitle("搜索")
  --sqlite_sel_tab(name)
  table_select() --将所有表名存储起来方便查询，因为sqlite没有系统表
end

