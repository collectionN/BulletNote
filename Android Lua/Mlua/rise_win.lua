import "android.graphics.Typeface"
function ttf_lay()
  return Typeface.createFromFile(File(activity.getLuaDir().."/res/Alimama_ShuHeiTi_Bold.ttf"))
end
添加记录=
{--布局
  LinearLayout;
  orientation="vertical";
  Focusable=true,
  FocusableInTouchMode=true,
  {
    TextView;--文本控件
    gravity='center';--重力
    --左:left 右:right 中:center 顶:top 底:bottom
    layout_width='fill';--宽度
    layout_height='40dp';--高度
    textColor='#ff51acf6';--文字颜色
    text='添加记录';--显示文字
    textSize='20';--文字大小
    layout_weight="1.0";
    layout_marginTop="fill";--边顶
    Typeface=ttf_lay(),
  };
  {
    TextView,--水平分割线
    layout_width="fill",--布局宽度
    layout_height="1px",--布局高度
    layout_gravity="center",--重力居中
    backgroundColor="#16000000",--背景色
  };
  {
    LinearLayout;
    orientation='horizontal';--重力属性
    layout_width='fill';--布局宽度
    layout_height='50dp';--布局高度
    gravity='center';--默认居中
    backgroundColor="#FFFFFFFF";--背景色
    {
      CardView;--卡片控件
      layout_margin='2dp';--边距
      layout_gravity='center';--重力
      --左:left 右:right 中:center 顶:top 底:bottom
      elevation='0dp';--阴影
      layout_width='fill';--宽度
      layout_height='12%w';--高度
      CardBackgroundColor='0';--颜色
      radius='8dp';--圆角
      {
        CardView;--卡片控件
        layout_margin='0dp';--边距
        layout_gravity='center|left';--重力
        --左:left 右:right 中:center 顶:top 底:bottom
        elevation='0dp';--阴影
        layout_width='fill';--宽度
        layout_height='12%w';--高度
        CardBackgroundColor='#ff9900';--颜色
        radius='8dp';--圆角
        {
          EditText;--编辑框控件
          id="edit_cyan";
          singleLine=true,--设置单行输入
          layout_width='fill';--编辑框宽度
          layout_height='fill';--编辑框高度
          textSize='12sp';--文字大小
          hintTextColor='#FF7D7D7D';--提示文字颜色
          textColor='#FF000000';--输入文字的颜色
          Hint='主字段*';--设置编辑框为空时的提示文字
          background='#ffefefef';
          layout_marginLeft='0dp';--布局左距
          layout_marginRight='0dp';--布局右距
        };
      };
    };
  };
  {
    LinearLayout;
    orientation='horizontal';--重力属性
    layout_width='fill';--布局宽度
    layout_height='50dp';--布局高度
    gravity='center';--默认居中
    backgroundColor="#FFFFFFFF";--背景色
    {
      CardView;--卡片控件
      layout_margin='2dp';--边距
      layout_gravity='center';--重力
      --左:left 右:right 中:center 顶:top 底:bottom
      elevation='0dp';--阴影
      layout_width='fill';--宽度
      layout_height='12%w';--高度
      CardBackgroundColor='0';--颜色
      radius='8dp';--圆角
      {
        CardView;--卡片控件
        layout_margin='0dp';--边距
        layout_gravity='center|left';--重力
        --左:left 右:right 中:center 顶:top 底:bottom
        elevation='0dp';--阴影
        layout_width='fill';--宽度
        layout_height='12%w';--高度
        CardBackgroundColor='#ff9900';--颜色
        radius='8dp';--圆角
        {
          EditText;--编辑框控件
          id="edit_remark";
          singleLine=true,--设置单行输入
          layout_width='fill';--编辑框宽度
          layout_height='fill';--编辑框高度
          textSize='12sp';--文字大小
          hintTextColor='#FF7D7D7D';--提示文字颜色
          textColor='#FF000000';--输入文字的颜色
          Hint='次字段';--设置编辑框为空时的提示文字
          background='#ffefefef';
          layout_marginLeft='0dp';--布局左距
          layout_marginRight='0dp';--布局右距
        };
      };
    };
  };
  {
    CardView;--卡片控件
    layout_margin='8dp';--边距
    layout_gravity='center';--重力
    --左:left 右:right 中:center 顶:top 底:bottom
    elevation='0dp';--阴影
    layout_width='fill';--宽度
    layout_height='8%w';--高度
    CardBackgroundColor='0';--颜色
    radius='8dp';--圆角
    {
      TextView;--文本控件
      gravity='center|left';--重力
      --左:left 右:right 中:center 顶:top 底:bottom
      layout_width='wrap';--宽度
      layout_height='fill';--高度
      textColor='#a3a1a1';--文字颜色
      text='首字母大写';--显示文字
      textSize='14dp';--文字大小
    };
    {
      Switch;
      layout_gravity='center|right';--重力
      id="Grt";
    };
  };
  {
    LinearLayout;
    orientation='horizontal';--重力属性
    layout_width='fill';--布局宽度
    layout_height='fill';--布局高度
    {
      TextView;--文本控件
      gravity='center';--重力
      --左:left 右:right 中:center 顶:top 底:bottom
      layout_width='43.5%w';--宽度
      textColor='#ff51acf6';--文字颜色
      text='批量导入';--显示文字
      textSize='15dp';--文字大小
      style="?android:attr/borderlessButtonStyle";
      onClick=function()
        导入弹窗.show()
      end
    };
    {
      TextView;--文本控件
      layout_gravity='center';--重力
      --左:left 右:right 中:center 顶:top 底:bottom
      layout_width='43.5%w';--宽度
      textColor='#ff51acf6';--文字颜色
      text='添加一条';--显示文字
      textSize='15dp';--文字大小
      style="?android:attr/borderlessButtonStyle";
      id="rise_record";
    };
  };
};
添加记录弹窗=AlertDialog.Builder(this).setView(loadlayout(添加记录)).show()

--中文过滤函数
function filter_spec_chars(s)
  local ss = {}
  for k = 1, #s do
    local c = string.byte(s,k)
    if not c then break end
    if (c>=48 and c<=57) or (c>= 65 and c<=90) or (c>=97 and c<=122) then
      if not string.char(c):find("%w") then
        table.insert(ss, string.char(c))
      end
     elseif c>=228 and c<=233 then
      local c1 = string.byte(s,k+1)
      local c2 = string.byte(s,k+2)
      if c1 and c2 then
        local a1,a2,a3,a4 = 128,191,128,191
        if c == 228 then a1 = 184
         elseif c == 233 then a2,a4 = 190,c1 ~= 190 and 191 or 165
        end
        if c1>=a1 and c1<=a2 and c2>=a3 and c2<=a4 then
          k = k + 2
          table.insert(ss, string.char(c,c1,c2))
        end
      end
    end
  end
  return table.concat(ss)
end

--内容长度判断函数
--传入内容返回字符数量，中文与英文同时处理，为确保长度不过分长，中英文长度同时/2，原中文3，英文1                                              
function count_zh_en(content)
  local count=#filter_spec_chars(content)/2+#content:gsub(filter_spec_chars(content),"")/2
  return count;
end

rise_record.onClick=function()--点击事件
  if edit_cyan.Text ~= "" then
    if count_zh_en(edit_cyan.Text)<=20 and count_zh_en(edit_remark.Text)<=20 then
      if Grt.isChecked() == true then
        local c=edit_cyan.Text
        local c=string.gsub(c,string.format("%.1s",c),string.upper(string.format("%.1s",c)),1)
        addrecord(to_table_name,c,edit_remark.Text)
        MD提示("成功添加记录,首字母已大写~",0xFF2196F3,0xFFFFFFFF,4,10)
        sqlite_sel_tab(to_table_name)
        添加记录弹窗.dismiss()
        edit_cyan.setText('')
        edit_remark.setText('')
       else
        addrecord(to_table_name,edit_cyan.Text,edit_remark.Text)
        MD提示("添加记录成功~",0xFF2196F3,0xFFFFFFFF,4,10)
        sqlite_sel_tab(to_table_name)
        添加记录弹窗.dismiss()
        edit_cyan.setText('')
        edit_remark.setText('')
      end
     else
      提示("记录过长了哦，删掉一点吧~")
    end
   else
    提示("主字段不能为空.")
  end
end;