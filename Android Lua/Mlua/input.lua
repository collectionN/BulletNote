require "import"
import "android.app.*"
import "android.os.*"
import "android.widget.*"
import "android.view.*"
import "AndLua"
import "java.io.File"
import "files"
import "android.graphics.Typeface"
function ttf_lay()
  return Typeface.createFromFile(File(activity.getLuaDir().."/res/Alimama_ShuHeiTi_Bold.ttf"))
end
导入弹窗=
{--布局
  LinearLayout;
  orientation="vertical";
  Focusable=true,
  FocusableInTouchMode=true,
  {
    TextView;--文本控件
    gravity='center';--重力
    --左:left 右:right 中:center 顶:top 底:bottom
    layout_width='fill';--宽度
    layout_height='40dp';--高度
    textColor='#ff51acf6';--文字颜色
    text='导入数据';--显示文字
    textSize='20';--文字大小
    layout_weight="1.0";
    layout_marginTop="fill";--边顶
    Typeface=ttf_lay(),
  };
  {
    TextView,--水平分割线
    layout_width="fill",--布局宽度
    layout_height="1px",--布局高度
    layout_gravity="center",--重力居中
    backgroundColor="#16000000",--背景色
  };
  {
    HorizontalScrollView,--横向滑动
    layout_width='wrap';--宽度
    layout_height='wrap';--高度
    horizontalScrollBarEnabled=false;--隐藏横向滑条
    --布局写这
    {
      LinearLayout,--线性布局
      orientation='horizontal',--方向
      layout_width='fill',--宽度
      layout_height='10%w',--高度
      background='#00FFFFFF',--背景颜色或图片路径
      {
        CardView;--卡片控件
        layout_margin='5dp';--边距
        layout_gravity='center|right';--重力
        --左:left 右:right 中:center 顶:top 底:bottom
        elevation='0dp';--阴影
        layout_width='wrap';--宽度
        layout_height='fill';--高度
        CardBackgroundColor='#ff1e8ae8';--颜色
        radius='4dp';--圆角
        {
          TextView;--文本控件
          gravity='center';--重力
          --左:left 右:right 中:center 顶:top 底:bottom
          layout_width='fill';--宽度
          layout_marginRight='5%w';
          layout_marginLeft='5%w';
          layout_height='fill';--高度
          textColor='#ffffff';--文字颜色
          text='从TXT文件导入';--显示文字
          textSize='14dp';--文字大小
          -- textIsSelectable=true--长按复制
        };
      };
      {
        CardView;--卡片控件
        layout_margin='5dp';--边距
        layout_gravity='center|right';--重力
        --左:left 右:right 中:center 顶:top 底:bottom
        elevation='0dp';--阴影
        layout_width='wrap';--宽度
        layout_height='fill';--高度
        CardBackgroundColor='#ff1e8ae8';--颜色
        radius='4dp';--圆角
        onClick=function()
          activity.newActivity("Mlua/browser",{"交流帮助"})--跳转页面
        end;
        {
          TextView;--文本控件
          gravity='center';--重力
          --左:left 右:right 中:center 顶:top 底:bottom
          layout_width='fill';--宽度
          layout_marginRight='5%w';
          layout_marginLeft='5%w';
          layout_height='fill';--高度
          textColor='#ffffff';--文字颜色
          text='如何导入？';--显示文字
          textSize='14dp';--文字大小
          -- textIsSelectable=true--长按复制
        };
      };
      {
        CardView;--卡片控件
        layout_margin='5dp';--边距
        layout_gravity='center|right';--重力
        --左:left 右:right 中:center 顶:top 底:bottom
        elevation='0dp';--阴影
        layout_width='wrap';--宽度
        layout_height='fill';--高度
        CardBackgroundColor='#1e000000';--颜色
        radius='4dp';--圆角
        onClick=function()
          提示("暂不支持.")
        end;
        {
          TextView;--文本控件
          gravity='center';--重力
          --左:left 右:right 中:center 顶:top 底:bottom
          layout_width='fill';--宽度
          layout_marginRight='5%w';
          layout_marginLeft='5%w';
          layout_height='fill';--高度
          textColor='#ffffff';--文字颜色
          text='其他类型文件';--显示文字
          textSize='14dp';--文字大小
          -- textIsSelectable=true--长按复制
        };
      };
    };
  };
  {
    LinearLayout;
    orientation='horizontal';--重力属性
    layout_width='fill';--布局宽度
    layout_height='50dp';--布局高度
    gravity='center';--默认居中
    backgroundColor="#FFFFFFFF";--背景色
    {
      CardView;--卡片控件
      layout_margin='2dp';--边距
      layout_gravity='center';--重力
      --左:left 右:right 中:center 顶:top 底:bottom
      elevation='0dp';--阴影
      layout_width='fill';--宽度
      layout_height='12%w';--高度
      CardBackgroundColor='0';--颜色
      radius='8dp';--圆角
      {
        CardView;--卡片控件
        layout_margin='0dp';--边距
        layout_gravity='center|left';--重力
        --左:left 右:right 中:center 顶:top 底:bottom
        elevation='0dp';--阴影
        layout_width='fill';--宽度
        layout_height='12%w';--高度
        CardBackgroundColor='#ff9900';--颜色
        radius='8dp';--圆角
        {
          EditText;--编辑框控件
          id="edit_choose_cyan";
          singleLine=true,--设置单行输入
          layout_width='fill';--编辑框宽度
          layout_height='fill';--编辑框高度
          textSize='12sp';--文字大小
          hintTextColor='#FF7D7D7D';--提示文字颜色
          textColor='#FF000000';--输入文字的颜色
          Hint='选择或输入txt文件位置';--设置编辑框为空时的提示文字
          background='#ffefefef';
          layout_marginLeft='0dp';--布局左距
          layout_marginRight='0dp';--布局右距
        };
      };
      {
        CardView;--卡片控件
        layout_margin='0dp';--边距
        layout_gravity='center|right';--重力
        --左:left 右:right 中:center 顶:top 底:bottom
        elevation='0dp';--阴影
        layout_width='wrap';--宽度
        layout_height='wrap';--高度
        CardBackgroundColor='#ff8b7be8';--颜色
        radius='8dp';--圆角
        id="choose_file";
        {
          TextView;--文本控件
          gravity='center';--重力
          --左:left 右:right 中:center 顶:top 底:bottom
          layout_width='fill';--宽度
          layout_marginRight='5%w';
          layout_marginLeft='5%w';
          layout_margin='2%w';--边距
          layout_height='fill';--高度
          textColor='#ffffff';--文字颜色
          text='浏览';--显示文字
          textSize='14dp';--文字大小
          -- textIsSelectable=true--长按复制
        };
      };
    };
  };
  --[[{
    CardView;--卡片控件
    layout_marginRight='6dp';--右距
    layout_margin='8dp';--边距
    elevation='0dp';--阴影
    layout_width='fill';--宽度
    layout_height='3dp';--高度
    CardBackgroundColor='#15000000';--颜色
    radius='5dp';--圆角
    id="button";
    {
      TextView;--文本控件
      --gravity='center';--重力
      layout_height='fill';--高度
      background='#FF5B81FE',--背景颜色或图片路径
      text='';--显示文字
      id="jv";
    };
  };]]

  {
    LinearLayout;
    orientation='horizontal';--重力属性
    layout_width='fill';--布局宽度
    layout_height='fill';--布局高度
    {

      TextView;--文本控件
      gravity='center';--重力
      --左:left 右:right 中:center 顶:top 底:bottom
      layout_width='43.5%w';--宽度
      textColor='#ff51acf6';--文字颜色
      text='取消';--显示文字
      textSize='15dp';--文字大小
      style="?android:attr/borderlessButtonStyle";
      onClick=function()
        导入弹窗.dismiss()
      end
    };
    {
      TextView;--文本控件
      layout_gravity='center';--重力
      --左:left 右:right 中:center 顶:top 底:bottom
      layout_width='43.5%w';--宽度
      textColor='#ff51acf6';--文字颜色
      text='导入';--显示文字
      textSize='15dp';--文字大小
      style="?android:attr/borderlessButtonStyle";
      id="input_choose";
      Typeface=ttf_lay(),
    };
    {
      TextView;--文本控件
      layout_gravity='center';--重力
      --左:left 右:right 中:center 顶:top 底:bottom
      layout_width='43.5%w';--宽度
      textColor='#ff51acf6';--文字颜色
      text='完成';--显示文字
      textSize='15dp';--文字大小
      style="?android:attr/borderlessButtonStyle";
      id="wc_choose";
      Typeface=ttf_lay(),
    };
  };
};
导入弹窗=AlertDialog.Builder(this).setView(loadlayout(导入弹窗)).show()
导入弹窗.dismiss()

wc_choose.setVisibility(View.GONE)

wc_choose.onClick=function()--点击事件
  导入弹窗.dismiss()
  input_choose.setVisibility(View.VISIBLE)
  wc_choose.setVisibility(View.GONE)
end;

function is_exist()
  if File("/sdcard/弹幕笔记/").exists() then
    return "/sdcard/弹幕笔记/"
   else
    return "/sdcard"
  end
end

function for_in_file(path)
  local v=0
  for c in io.lines(path) do
    if c:find("┃") then
      local cyan=c:match("(.+)┃")
      local remark=c:match("┃(.+)")
      addrecord(to_table_name,cyan,remark)
      --提示(cyan..remark.."ok")
     else
      addrecord(to_table_name,c,"")
      --提示(type(c))
    end
    v=v+1
    input_choose.setText("已导入"..v)
  end
  提示("导入完成！共导入"..v.."条数据")
  sqlite_sel_tab(to_table_name) --更新适配器
  input_choose.setVisibility(View.GONE)
  wc_choose.setVisibility(View.VISIBLE)
end

input_choose.onClick=function()
  if edit_choose_cyan.Text~="" then
    if edit_choose_cyan.Text:find(".txt") then
      for_in_file(edit_choose_cyan.Text)
     else
      提示("文件类型或路径有误！请选择txt文件！")
    end
   else
    提示("没有选择文件哦~")
  end
end;

fs=files.open(is_exist(),'df')
:title('选择文件')
:filter('txt')
:on(function(t)
  edit_choose_cyan.setText(t)
end)

function choose_file.onClick()
  fs:show()
end

function onDestroy()
  fs:close()
end
--打开文件选择
--_M.open(path, mode)
--第一参数为初始路径，第二参数为选择模式
--d 文件夹 f 文件 m 多选

--设置标题
--_M:title(str)
--传入字符串，设置对话框标题，为空即取消标题

--过滤类型
--_M:filter(ext...)
--只允许指定类型

--回调事件
--_M:on(func)
--传入函数，返回字符串或数组

--跳转路径
--_M:go(path)

--显示
--_M:show()

--隐藏
--_M:hide()

--关闭文件选择
--_M:close()