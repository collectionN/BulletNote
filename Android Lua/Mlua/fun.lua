--[[
该文件为最重要的函数文件，其中包含了几乎所有的自定义函数和不便于分类简单布局
]]

--字体设置
function ttf_lay()
  return Typeface.createFromFile(File(activity.getLuaDir().."/res/Alimama_ShuHeiTi_Bold.ttf"))
end

--弹幕适配器
item=
{
  LinearLayout,--线性布局
  orientation='vertical',--方向
  layout_width='fill',--宽度
  layout_height='fill',--高度
  background='#00FFFFFF',--背景颜色或图片路径
  {
    CardView;--卡片控件
    layout_margin='8dp';--边距
    layout_gravity='center';--重力
    elevation='0dp';--阴影
    layout_width='fill';--宽度
    layout_height='wrap';--高度
    CardBackgroundColor='#A593E0';--颜色
    radius='8dp';--圆角
    id="list_table";
    {
      CardView;--卡片控件
      layout_margin='1%w';--边距
      layout_gravity='center|left';--重力
      elevation='0dp';--阴影
      layout_width='50%w';--宽度
      layout_height='fill';--高度
      CardBackgroundColor='0';--颜色
      radius='0dp';--圆角
      {
        LinearLayout,--线性布局
        orientation='vertical',--方向
        layout_width='fill',--宽度
        layout_height='fill',--高度
        {
          TextView;--文本控件
          layout_margin='10dp';--边距
          gravity='center|top|left';--重力
          layout_width='fill';--宽度
          layout_height='wrap';--高度
          textColor='#ffffff';--文字颜色
          textSize='20dp';--文字大小
          Typeface=ttf_lay(),
          id="table_title";
        };
        {
          TextView;--文本控件
          layout_margin='5dp';--边距
          layout_marginLeft="2%w";
          gravity='center|left|bottom';--重力
          layout_width='fill';--宽度
          layout_height='wrap';--高度
          textColor='#ffffff';--文字颜色
          textSize='14dp';--文字大小
          id="table_count";
        };
      };
    };
    {
      CardView;--卡片控件
      layout_margin='5%w';--边距
      layout_gravity='center|right';--重力
      elevation='0dp';--阴影
      layout_width='wrap';--宽度
      layout_height='wrap';--高度
      CardBackgroundColor='0';--颜色
      radius='0dp';--圆角
      {
        CardView;--卡片控件
        layout_marginTop="1%w";
        layout_gravity='center';--重力
        elevation='0dp';--阴影
        layout_width='fill';--宽度
        layout_height='wrap';--高度
        CardBackgroundColor='0';--颜色
        radius='8dp';--圆角
        {
          TextView;--文本控件
          layout_margin='5dp';--边距
          gravity='center';--重力
          layout_width='fill';--宽度
          layout_height='fill';--高度
          textColor='#ffffff';--文字颜色
          Typeface=ttf_lay(),
          textSize='14dp';--文字大小
          id="table_tips";
        };
      };
    };
  };
};
--#######################################
--我的页面适配器
item_more={
  LinearLayout,--线性布局
  orientation='vertical',--方向
  layout_width='fill',--宽度
  layout_height='fill',--高度
  background='#00FFFFFF',--背景颜色或图片路径
  {
    CardView;--卡片控件
    layout_margin='0dp';--边距
    layout_gravity='center';--重力
    --左:left 右:right 中:center 顶:top 底:bottom
    elevation='0dp';--阴影
    layout_width='fill';--宽度
    layout_height='13%w';--高度
    CardBackgroundColor='#ffffff';--颜色
    radius='0dp';--圆角
    {
      CardView;--卡片控件
      layout_margin='2dp';--边距
      layout_marginLeft='3%w';--左距
      layout_gravity='center|left';--重力
      --左:left 右:right 中:center 顶:top 底:bottom
      elevation='0dp';--阴影
      layout_width='wrap';--宽度
      layout_height='wrap';--高度
      CardBackgroundColor='0';--颜色
      radius='0dp';--圆角
      {
        LinearLayout,--线性布局
        orientation='horizontal',--方向
        layout_width='wrap',--宽度
        layout_height='wrap',--高度
        background='0',--背景颜色或图片路径
        {
          ImageView;--图片控件
          layout_width='7%w';--宽度
          layout_height='7%w';--高度
          scaleType='fitXY';--图片显示类型
          id="ico_left",
        };
        {
          TextView;--文本控件
          gravity='center';--重力
          layout_marginLeft='2%w';--左距
          --左:left 右:right 中:center 顶:top 底:bottom
          layout_width='wrap';--宽度
          layout_height='fill';--高度
          textColor='#eb000000';--文字颜色
          textSize='15dp';--文字大小
          Typeface=ttf_lay(),
          id="txt_center",
        };
      };
      --
    };
    {
      CardView;--卡片控件
      layout_margin='8dp';--边距
      layout_gravity='center|right';--重力
      --左:left 右:right 中:center 顶:top 底:bottom
      elevation='0dp';--阴影
      layout_width='5%w';--宽度
      layout_height='5%w';--高度
      CardBackgroundColor='0';--颜色
      radius='8dp';--圆角
      {
        ImageView;--图片控件
        src='res/forward.png';--图片路径
        layout_width='fill';--宽度
        layout_height='fill';--高度
        scaleType='fitXY';--图片显示类型
      };
    };
  };
};

adp_list=LuaAdapter(activity,item_more)
more_list.setAdapter(adp_list)

local 总列表=[[
<res/statistics.png>
《统计信息》

<res/setting.png>
《软件设置》

<res/question.png>
《交流帮助》

<res/updata.png>
《更新信息》

<res/contact.png>
《关于我们》
]]

local name_list=总列表:gmatch("<(.-)>\n《(.-)》")--截取格式
for v,i in name_list do--循环取值
  adp_list.add{--构建视图控件
    txt_center=i,--图标
    ico_left=v,--标题
  }
end

--我的页面点击事件

more_list.onItemClick=function(v,i)
  local name=i.Tag.txt_center.Text
  if name=="统计信息" then
    MD提示("建设中~",0xFF2196F3,0xFFFFFFFF,4,10)
   elseif name=="软件设置" then
    activity.newActivity("Mlua/setting")--跳转页面
   else
    activity.newActivity("Mlua/browser",{name})--跳转页面
  end
  return true--返回
end


--################################################
--悬浮窗page页面

local function addTab(t)
  return {
    CardView;
    cardBackgroundColor=0x00000000,
    elevation="0";
    radius="38dp";
    id="___",
    layout_marginLeft="12dp",
    onClick=function(v)
      for i=0,v.parent.getChildCount()-1 do
        if v.parent.getChildAt(i).id==v.id then
          pg.setCurrentItem(i)
          return
        end
      end
    end,
    {
      LinearLayout;
      layout_width="-2";
      layout_height="-2";
      padding="4dp",
      paddingLeft="14dp",
      paddingRight="14dp",
      orientation="vertical";
      {
        TextView;
        textSize="14sp",
        textColor=0xFFFFD6E2,
        gravity="center";
        text=t;
      };
    };
  };
end

local function setWidth(a,b)
  local q=a.layoutParams
  q.width=b
  a.layoutParams=q
end

function dp2px(dpValue)
  local scale = activity.getResources().getDisplayMetrics().scaledDensity
  return dpValue * scale + 0.5
end

bar.addView(loadlayout(addTab("添加记录"),nil,bar.class))
bar.addView(loadlayout(addTab("翻译系统"),nil,bar.class))

bar.getChildAt(0).getChildAt(0).getChildAt(0).textColor=0xffffffff

choose.addView(loadlayout({
  CardView;
  cardBackgroundColor=0xFFF2BECA,
  elevation="0";
  radius="38dp";
  layout_height="24dp",
  layout_marginLeft="12dp",
},nil,choose.class))

bar.getChildAt(0).post {
  run=function()
    setWidth(choose.getChildAt(0),bar.getChildAt(0).width)
  end
}

--[[for i=1,2 do
  pg.adapter.add(loadlayout{
    LinearLayout,
    layout_width="fill",
    layout_height="fill",
  })
end]]

local data={
  scrollData={},

}

pg.setOnPageChangeListener{
  onPageSelected=function(t)

    for i=0,bar.getChildCount()-1 do
      bar.getChildAt(i).getChildAt(0).getChildAt(0).textColor=0xFFFFD6E2
    end
    bar.getChildAt(t).getChildAt(0).getChildAt(0).textColor=0xffffffff
    setWidth(choose.getChildAt(0),bar.getChildAt(t).width)
    choose.getChildAt(0).x=bar.getChildAt(t).x
  end,
  onPageScrollStateChanged=function(i)
    data.scrollData.scroll=i>0
  end,
  onPageScrolled=function(a,b,c)
    local nowView=bar.getChildAt(a)

    local nextView=bar.getChildAt(a==bar.getChildCount() and bar.getChildCount() or a+1)

    if data.scrollData.scroll and b~=0 then
      if data.scrollData.last and data.scrollData.last<b then
        if nextView.width<nowView.width then
          setWidth(choose.getChildAt(0),nowView.width-((nowView.width-nextView.width)*b))
         else
          setWidth(choose.getChildAt(0),nowView.width+((nextView.width-nowView.width)*b))
        end
        choose.getChildAt(0).x=nextView.x-((nextView.x-nowView.x)*(1-b))
       else

        local lastView=bar.getChildAt(a)
        local nowView=bar.getChildAt(a+1)

        if lastView.width>nowView.width then
          setWidth(choose.getChildAt(0),nowView.width+((lastView.width-nowView.width)*(1-b)))
         else
          setWidth(choose.getChildAt(0),nowView.width-((nowView.width-lastView.width)*(1-b)))
        end

        choose.getChildAt(0).x=lastView.x+((nowView.x-lastView.x)*b)
      end

    end

    if b==0 or c==0 then
      for i=0,bar.getChildCount()-1 do
        bar.getChildAt(i).getChildAt(0).getChildAt(0).textColor=0xFFFFD6E2
      end
      bar.getChildAt(pg.getCurrentItem()).getChildAt(0).getChildAt(0).textColor=0xffffffff
    end

    data.scrollData.page=a
    data.scrollData.last=b
  end,
}

--悬浮窗中列表适配器
xfc_item={
  LinearLayout,--线性布局
  orientation="vertical",--布局方向
  layout_width="fill",--布局宽度
  layout_height="wrap",--布局高度
  gravity="left|center",--重力居左｜置中
  padding="10dp",--布局填充
  {
    TextView,--文本框控件
    text="",--文本内容
    textSize="15sp",--文本大小
    textColor="#222222",--文本颜色
    id="xfc_list_table_name",--控件ID
  },
}
--#################################################3


--基础函数封装
--动画效果区

--[[function 开始反转(id)
  reversal=true
  反转动画 = ObjectAnimator.ofFloat(id, "rotationX",{0,180})
  反转动画.setRepeatCount(0)--设置动画重复次数，这里-1代表无限
  反转动画.setRepeatMode(Animation.REVERSE)--循环模式
  反转动画.setInterpolator(DecelerateInterpolator())--设置插值器
  反转动画.setDuration(1000)--设置动画时间
  反转动画.start()
end]]

function 取消反转(id)
  reversal=false
  反转动画 = ObjectAnimator.ofFloat(id, "rotationX",{180,0})
  反转动画.setRepeatCount(0)--设置动画重复次数，这里-1代表无限
  反转动画.setRepeatMode(Animation.REVERSE)--循环模式
  反转动画.setInterpolator(DecelerateInterpolator())--设置插值器
  反转动画.setDuration(1000)--设置动画时间
  反转动画.start()
end

--放大动画
function enlarge()
  放大动画=ScaleAnimation(1,1.05,1,1.05,Animation.RELATIVE_TO_SELF,0.5,Animation.RELATIVE_TO_SELF,0.5)
  放大动画.setDuration(500)--设置动画时间
  放大动画.setFillAfter(true)--设置动画后停留位置
  放大动画.setRepeatCount(0)--设置无限循环
end

--缩小动画
function unlarge()
  缩小动画=ScaleAnimation(1,1.0,1,1.0,Animation.RELATIVE_TO_SELF,0.5,Animation.RELATIVE_TO_SELF,0.5)
  缩小动画.setDuration(500)--设置动画时间
  缩小动画.setFillAfter(true)--设置动画后停留位置
  缩小动画.setRepeatCount(0)--设置无限循环
end

function 揭露动画(view,a,b,c,d,e)
  translationUp = ViewAnimationUtils.createCircularReveal(view,a,b,c,d)
  translationUp.setInterpolator(DecelerateInterpolator())
  translationUp.setDuration(e)
  translationUp.start()
end

单击缩放={
  onTouch=function (v,e)
    if e.action==0 then
      设置缩放(v,1,0.95,250)
     else
      设置缩放(v,0.90,1,250)
    end
  end}

function 设置缩放(view,startscale,endscale,time)
  local animatorSetsuofasng = AnimatorSet()
  local scaleX=ObjectAnimator.ofFloat(view,"scaleX",{startscale,endscale})
  local scaleY=ObjectAnimator.ofFloat(view,"scaleY",{startscale,endscale})
  animatorSetsuofasng.setDuration(time)
  animatorSetsuofasng.setInterpolator(DecelerateInterpolator())
  animatorSetsuofasng.play(scaleX).with(scaleY);
  animatorSetsuofasng.start()
end

function 缩放动画(控件)
  import "android.view.animation.*"
  控件.startAnimation(ScaleAnimation(0.0,1.0,0.0,1.0,1,0.5,1,0.5).setDuration(200))
end


--[[弹幕动画
function bullet_animation(id)
  id.measure(View.MeasureSpec.makeMeasureSpec(0,View.MeasureSpec.UNSPECIFIED),View.MeasureSpec.makeMeasureSpec(0,View.MeasureSpec.UNSPECIFIED));
  local width =0-(id.getMeasuredWidth());
  print(width)
  --平移动画 = ObjectAnimator.ofFloat(id, "X",{获取屏幕宽(),width})
  平移动画 = ObjectAnimator.ofFloat(id, "X",{获取屏幕宽(),width})
  平移动画.setRepeatCount(0)--设置动画重复次数，这里-1代表无限
  平移动画.setRepeatMode(Animation.REVERSE)--循环模式
  平移动画.setInterpolator(DecelerateInterpolator())--设置插值器
  平移动画.setDuration(15000)--设置动画时间
  平移动画.start()
end]]

--绘制开关
dip={toPx=function(context, dpValue)
    scale = context.getResources().getDisplayMetrics().density;
    return dpValue * scale + 0.5
  end}

function CircleBack_SeekDra(InsideColor,Ad_Size,ble,Colorse3)
  local colors = InsideColor
  local Sizes=dip.toPx(this,Ad_Size)--设置开关大小
  local Stroke=dip.toPx(this,4)--设置拖块边距
  local Track_Stroke=dip.toPx(this,2)--设置背景边距
  local drawable = GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM,{});
  drawable.setCornerRadius(Sizes/2);
  drawable.setColor(InsideColor)
  if ble
    drawable.setStroke(Stroke, 0x00ffffff)
    drawable.setSize(Sizes,Sizes)
   else
    drawable.setAlpha(60)
    drawable.setStroke(Track_Stroke, 0x00ffffff)
  end
  drawable.setGradientType(GradientDrawable.RECTANGLE);
  return drawable
end

Switch_x=function(view,Colors,Colors2,Colors3,Ad_Size)
  pcall(function()
    if view.isChecked()
      Colorse=Colors
      Colorse2=Colors
     else
      Colorse=Colors2
      Colorse2=Colors3
    end
    local padd_W=dip.toPx(this,Ad_Size/2.5)
    view.setThumbDrawable(CircleBack_SeekDra(Colorse,Ad_Size,true))
    .setTrackDrawable(CircleBack_SeekDra(Colorse2,Ad_Size,false))
    .setPadding(padd_W,padd_W,padd_W,padd_W)
  end)
end

Grt1.setOnCheckedChangeListener({
  onCheckedChanged=function(buttonView, isChecked)
    Switch_x(Grt1,0xffff6e17,0xFFECECEC,0xff000000,20,6)--预加载
  end})

Ad_Color=0xffff0000--边框颜色与拖块选中颜色
Ad_Color3=0xff000000--设置底部背景未选中颜色
Ad_Color2=0xFFECECEC
Ad_Size=25--按钮的大小dip
Ad_Padding=6--设置拖块边距范围dip
Switch_x(Grt1,0xffff6e17,0xFFECECEC,0xff000000,20,6)--预加载

--MD5
function MD5(str)
  local HexTable = {"0","1","2","3","4","5","6","7","8","9","A","B","C","D","E","F"}
  local A = 0x67452301
  local B = 0xefcdab89
  local C = 0x98badcfe
  local D = 0x10325476

  local S11 = 7
  local S12 = 12
  local S13 = 17
  local S14 = 22
  local S21 = 5
  local S22 = 9
  local S23 = 14
  local S24 = 20
  local S31 = 4
  local S32 = 11
  local S33 = 16
  local S34 = 23
  local S41 = 6
  local S42 = 10
  local S43 = 15
  local S44 = 21

  local function F(x,y,z)
    return (x & y) | ((~x) & z)
  end
  local function G(x,y,z)
    return (x & z) | (y & (~z))
  end
  local function H(x,y,z)
    return x ~ y ~ z
  end
  local function I(x,y,z)
    return y ~ (x | (~z))
  end
  local function FF(a,b,c,d,x,s,ac)
    a = a + F(b,c,d) + x + ac
    a = (((a & 0xffffffff) << s) | ((a & 0xffffffff) >> 32 - s)) + b
    return a & 0xffffffff
  end
  local function GG(a,b,c,d,x,s,ac)
    a = a + G(b,c,d) + x + ac
    a = (((a & 0xffffffff) << s) | ((a & 0xffffffff) >> 32 - s)) + b
    return a & 0xffffffff
  end
  local function HH(a,b,c,d,x,s,ac)
    a = a + H(b,c,d) + x + ac
    a = (((a & 0xffffffff) << s) | ((a & 0xffffffff) >> 32 - s)) + b
    return a & 0xffffffff
  end
  local function II(a,b,c,d,x,s,ac)
    a = a + I(b,c,d) + x + ac
    a = (((a & 0xffffffff) << s) | ((a & 0xffffffff) >> 32 - s)) + b
    return a & 0xffffffff
  end



  local function MD5StringFill(s)
    local len = s:len()
    local mod512 = len * 8 % 512
    --需要填充的字节数
    local fillSize = (448 - mod512) // 8
    if mod512 > 448 then
      fillSize = (960 - mod512) // 8
    end

    local rTab = {}

    --记录当前byte在4个字节的偏移
    local byteIndex = 1
    for i = 1,len do
      local index = (i - 1) // 4 + 1
      rTab[index] = rTab[index] or 0
      rTab[index] = rTab[index] | (s:byte(i) << (byteIndex - 1) * 8)
      byteIndex = byteIndex + 1
      if byteIndex == 5 then
        byteIndex = 1
      end
    end
    --先将最后一个字节组成4字节一组
    --表示0x80是否已插入
    local b0x80 = false
    local tLen = #rTab
    if byteIndex ~= 1 then
      rTab[tLen] = rTab[tLen] | 0x80 << (byteIndex - 1) * 8
      b0x80 = true
    end

    --将余下的字节补齐
    for i = 1,fillSize // 4 do
      if not b0x80 and i == 1 then
        rTab[tLen + i] = 0x80
       else
        rTab[tLen + i] = 0x0
      end
    end

    --后面加原始数据bit长度
    local bitLen = math.floor(len * 8)
    tLen = #rTab
    rTab[tLen + 1] = bitLen & 0xffffffff
    rTab[tLen + 2] = bitLen >> 32

    return rTab
  end

  --	Func:	计算MD5
  --	Param:	string
  --	Return:	string
  ---------------------------------------------

  function string.md5(s)
    --填充
    local fillTab = MD5StringFill(s)
    local result = {A,B,C,D}

    for i = 1,#fillTab // 16 do
      local a = result[1]
      local b = result[2]
      local c = result[3]
      local d = result[4]
      local offset = (i - 1) * 16 + 1
      --第一轮
      a = FF(a, b, c, d, fillTab[offset + 0], S11, 0xd76aa478)
      d = FF(d, a, b, c, fillTab[offset + 1], S12, 0xe8c7b756)
      c = FF(c, d, a, b, fillTab[offset + 2], S13, 0x242070db)
      b = FF(b, c, d, a, fillTab[offset + 3], S14, 0xc1bdceee)
      a = FF(a, b, c, d, fillTab[offset + 4], S11, 0xf57c0faf)
      d = FF(d, a, b, c, fillTab[offset + 5], S12, 0x4787c62a)
      c = FF(c, d, a, b, fillTab[offset + 6], S13, 0xa8304613)
      b = FF(b, c, d, a, fillTab[offset + 7], S14, 0xfd469501)
      a = FF(a, b, c, d, fillTab[offset + 8], S11, 0x698098d8)
      d = FF(d, a, b, c, fillTab[offset + 9], S12, 0x8b44f7af)
      c = FF(c, d, a, b, fillTab[offset + 10], S13, 0xffff5bb1)
      b = FF(b, c, d, a, fillTab[offset + 11], S14, 0x895cd7be)
      a = FF(a, b, c, d, fillTab[offset + 12], S11, 0x6b901122)
      d = FF(d, a, b, c, fillTab[offset + 13], S12, 0xfd987193)
      c = FF(c, d, a, b, fillTab[offset + 14], S13, 0xa679438e)
      b = FF(b, c, d, a, fillTab[offset + 15], S14, 0x49b40821)

      --第二轮
      a = GG(a, b, c, d, fillTab[offset + 1], S21, 0xf61e2562)
      d = GG(d, a, b, c, fillTab[offset + 6], S22, 0xc040b340)
      c = GG(c, d, a, b, fillTab[offset + 11], S23, 0x265e5a51)
      b = GG(b, c, d, a, fillTab[offset + 0], S24, 0xe9b6c7aa)
      a = GG(a, b, c, d, fillTab[offset + 5], S21, 0xd62f105d)
      d = GG(d, a, b, c, fillTab[offset + 10], S22, 0x2441453)
      c = GG(c, d, a, b, fillTab[offset + 15], S23, 0xd8a1e681)
      b = GG(b, c, d, a, fillTab[offset + 4], S24, 0xe7d3fbc8)
      a = GG(a, b, c, d, fillTab[offset + 9], S21, 0x21e1cde6)
      d = GG(d, a, b, c, fillTab[offset + 14], S22, 0xc33707d6)
      c = GG(c, d, a, b, fillTab[offset + 3], S23, 0xf4d50d87)
      b = GG(b, c, d, a, fillTab[offset + 8], S24, 0x455a14ed)
      a = GG(a, b, c, d, fillTab[offset + 13], S21, 0xa9e3e905)
      d = GG(d, a, b, c, fillTab[offset + 2], S22, 0xfcefa3f8)
      c = GG(c, d, a, b, fillTab[offset + 7], S23, 0x676f02d9)
      b = GG(b, c, d, a, fillTab[offset + 12], S24, 0x8d2a4c8a)

      --第三轮
      a = HH(a, b, c, d, fillTab[offset + 5], S31, 0xfffa3942)
      d = HH(d, a, b, c, fillTab[offset + 8], S32, 0x8771f681)
      c = HH(c, d, a, b, fillTab[offset + 11], S33, 0x6d9d6122)
      b = HH(b, c, d, a, fillTab[offset + 14], S34, 0xfde5380c)
      a = HH(a, b, c, d, fillTab[offset + 1], S31, 0xa4beea44)
      d = HH(d, a, b, c, fillTab[offset + 4], S32, 0x4bdecfa9)
      c = HH(c, d, a, b, fillTab[offset + 7], S33, 0xf6bb4b60)
      b = HH(b, c, d, a, fillTab[offset + 10], S34, 0xbebfbc70)
      a = HH(a, b, c, d, fillTab[offset + 13], S31, 0x289b7ec6)
      d = HH(d, a, b, c, fillTab[offset + 0], S32, 0xeaa127fa)
      c = HH(c, d, a, b, fillTab[offset + 3], S33, 0xd4ef3085)
      b = HH(b, c, d, a, fillTab[offset + 6], S34, 0x4881d05)
      a = HH(a, b, c, d, fillTab[offset + 9], S31, 0xd9d4d039)
      d = HH(d, a, b, c, fillTab[offset + 12], S32, 0xe6db99e5)
      c = HH(c, d, a, b, fillTab[offset + 15], S33, 0x1fa27cf8)
      b = HH(b, c, d, a, fillTab[offset + 2], S34, 0xc4ac5665)

      --第四轮
      a = II(a, b, c, d, fillTab[offset + 0], S41, 0xf4292244)
      d = II(d, a, b, c, fillTab[offset + 7], S42, 0x432aff97)
      c = II(c, d, a, b, fillTab[offset + 14], S43, 0xab9423a7)
      b = II(b, c, d, a, fillTab[offset + 5], S44, 0xfc93a039)
      a = II(a, b, c, d, fillTab[offset + 12], S41, 0x655b59c3)
      d = II(d, a, b, c, fillTab[offset + 3], S42, 0x8f0ccc92)
      c = II(c, d, a, b, fillTab[offset + 10], S43, 0xffeff47d)
      b = II(b, c, d, a, fillTab[offset + 1], S44, 0x85845dd1)
      a = II(a, b, c, d, fillTab[offset + 8], S41, 0x6fa87e4f)
      d = II(d, a, b, c, fillTab[offset + 15], S42, 0xfe2ce6e0)
      c = II(c, d, a, b, fillTab[offset + 6], S43, 0xa3014314)
      b = II(b, c, d, a, fillTab[offset + 13], S44, 0x4e0811a1)
      a = II(a, b, c, d, fillTab[offset + 4], S41, 0xf7537e82)
      d = II(d, a, b, c, fillTab[offset + 11], S42, 0xbd3af235)
      c = II(c, d, a, b, fillTab[offset + 2], S43, 0x2ad7d2bb)
      b = II(b, c, d, a, fillTab[offset + 9], S44, 0xeb86d391)

      --加入到之前计算的结果当中
      result[1] = result[1] + a
      result[2] = result[2] + b
      result[3] = result[3] + c
      result[4] = result[4] + d
      result[1] = result[1] & 0xffffffff
      result[2] = result[2] & 0xffffffff
      result[3] = result[3] & 0xffffffff
      result[4] = result[4] & 0xffffffff
    end

    --将Hash值转换成十六进制的字符串
    local retStr = ""
    for i = 1,4 do
      for _ = 1,4 do
        local temp = result[i] & 0x0F
        local str = HexTable[temp + 1]
        result[i] = result[i] >> 4
        temp = result[i] & 0x0F
        retStr = retStr .. HexTable[temp + 1] .. str
        result[i] = result[i] >> 4
      end
    end

    return retStr
  end

  return string.md5(str)
end

--设置主页图标的大小，颜色
--写得不够优雅，但也就3个图标，索性就直接复制粘贴了。
--没有研究优雅的操作方法，比如说下标控制之内的

function color(id)
  enlarge()
  unlarge()
  if id=="m1" then
    m1t.setTextColor(0xFF5C7CFF)
    m1p.setColorFilter(0xFF5C7CFF)
    m2p.setColorFilter(0xFFC6C6C6)
    m3p.setColorFilter(0xFFC6C6C6)
    m2t.setTextColor(0xFFC6C6C6)
    m3t.setTextColor(0xFFC6C6C6)

    m1.startAnimation(放大动画)
    m2.startAnimation(缩小动画)
    m3.startAnimation(缩小动画)
   elseif id=="m2" then
    m1t.setTextColor(0xFFC6C6C6)
    m2t.setTextColor(0xFF5C7CFF)
    m2p.setColorFilter(0xFF5C7CFF)
    m1p.setColorFilter(0xFFC6C6C6)
    m3p.setColorFilter(0xFFC6C6C6)
    m3t.setTextColor(0xFFC6C6C6)

    m2.startAnimation(放大动画)
    m1.startAnimation(缩小动画)
    m3.startAnimation(缩小动画)
   elseif id=="m3" then
    m1t.setTextColor(0xFFC6C6C6)
    m2t.setTextColor(0xFFC6C6C6)
    m3t.setTextColor(0xFF5C7CFF)
    m3p.setColorFilter(0xFF5C7CFF)
    m2p.setColorFilter(0xFFC6C6C6)
    m1p.setColorFilter(0xFFC6C6C6)

    m3.startAnimation(放大动画)
    m2.startAnimation(缩小动画)
    m1.startAnimation(缩小动画)
  end
end

--###########################################

--常用函数封装
function print(txt)
  tcxx={
    LinearLayout;--线性布局
    orientation='vertical';--布局方向
    layout_width='fill';--布局宽度
    layout_height='fill';--布局高度
    {
      CardView;--卡片控件
      layout_width='wrap';--卡片宽度
      layout_height='wrap';--卡片高度
      CardBackgroundColor='#ffffff';--卡片颜色
      elevation=0;--阴影属性
      radius='12dp';--卡片圆角
      {
        TextView;
        layout_width="wrap";--布局宽度
        layout_height="wrap";--布局高度
        background="#cc000000";--背景颜色
        padding="8dp";--布局填充
        textSize="15sp";--文字大小
        TextColor="#ffeeeeee";--文字颜色
        gravity="center";--布局居中
        id="wenzi";--控件ID
      };
    };
  };
  local toast=Toast.makeText(activity,"文本",Toast.LENGTH_SHORT).setView(loadlayout(tcxx))
  toast.setGravity(Gravity.BOTTOM,0,120)
  wenzi.Text=""..txt..""
  toast.show()
end

--调用封装

--############################################################

--数据库
--execSQL()方法可以执行insert、delete、update和CREATE TABLE之类有更改行为的SQL语句
function exec(sql)
  db.execSQL(sql);
end

--rawQuery()方法用于执行select语句。
function raw(sql,text)
  cursor=db.rawQuery(sql,text)
end

-- 判断表是否存在
function Is_Have_table(name)
  local sql="select count(*) from sqlite_master where type='table' and name='"..name.."'"
  if pcall(raw,sql,nil) then
    cursor.moveToFirst(); --移动指针
    local result = cursor.getLong(0);
    if result==0 then
      -- 不存在表
      return false
     else
      return true
    end
   else
    print("数据库查询失败！")
  end
end


--创建表
function addtable(tablename)
  if Is_Have_table(tablename)==false then
    local CreatrTableSql="create table "..tablename.."(id integer primary key,cyan varchar,remark varchar)"
    if pcall(exec,CreatrTableSql) then
      --创建user表，integer类型为自动增长
      print("创建成功")
     else
      print("创建失败")
    end
   else
    print(tablename.."已存在.")
  end
end

--删除配置表记录
function delete_record_config(record_name)
  local sql="delete from config where name='"..record_name.."'"
  if pcall(exec,sql) then
    --print("删除成功")
   else
    提示("删除失败")
  end
end

-- 删除同步表
function delete_record_sync(record_name)
  local sql="delete from sync where name='"..record_name.."'"
  if pcall(exec,sql) then
    --print("删除成功")
   else
    提示("删除失败")
  end
end

--重命名表
function rename_table(old_name,new_name)
  local sql="ALTER TABLE "..old_name.." RENAME to "..new_name
  if pcall(exec,sql) then
    MD提示("重命名成功",0xFF2196F3,0xFFFFFFFF,4,10)
    updata_name_config(old_name,new_name)
   else
    print("重命名失败")
  end
end

function updata_name_config(name,new_name)
  local sql=("UPDATE config SET name = '"..new_name.."' WHERE name='"..name.."';")
  --print(sql)
  if pcall(exec,sql) then
    --MD提示("配置更新成功",0xFF2196F3,0xFFFFFFFF,4,10)
   else
    print("更新配置读写失败!")
  end
end

--为表创建新纪录
function addrecord(tablename,newrecord,newremark)
  local newrecord=newrecord:gsub('&&',"•")
  local newremark=newremark:gsub('&&',"•")
  local sql="insert into "..tablename.."(cyan,remark) values('"..newrecord.."','"..newremark.."')"
  --print(sql)
  if pcall(exec,sql) then
    --MD提示("添加新记录成功",0xFF2196F3,0xFFFFFFFF,4,10)
   else
    print("添加新纪录失败")
  end
end

--为配置表创建新配置
function write_config(name,value,bool_f) --应该尽量避免value与bool_f同时传入，不太方便判断
  --name为主键，必须传入，即配置项的名称；value是值，varchar类型；bool_f为布尔值，一般传入布尔值即可
  local sql="insert into config(name,value,bool) values('"..name.."','"..value.."','"..bool_f.."')"
  --print(sql)
  if pcall(exec,sql) then
    --MD提示("配置读写成功",0xFF2196F3,0xFFFFFFFF,4,10)
   else
    print("配置写入失败!")
  end
end

-- 当前时间get函数
function get_current_time()
  local now = os.time()
  local time_table = os.date("*t", now)
  return string.format("%04d-%02d-%02d %02d:%02d:%02d", time_table.year, time_table.month, time_table.day, time_table.hour, time_table.min, time_table.sec)
end

-- 计算时间差值
function calculate_time_passed(input_time)
  -- 获取当前时间
  local current_time = os.time()
  local current_time_table = os.date("*t", current_time)

  -- 将输入的时间字符串转换为时间表
  local pattern = "(%d+)-(%d+)-(%d+) (%d+):(%d+):(%d+)"
  local year, month, day, hour, min, sec = input_time:match(pattern)
  local input_time_table = { year = tonumber(year), month = tonumber(month), day = tonumber(day), hour = tonumber(hour), min = tonumber(min), sec = tonumber(sec) }

  -- 将时间表转换回时间戳
  local input_time_stamp = os.time(input_time_table)

  -- 计算时间差
  local difference = current_time - input_time_stamp

  -- 判断返回的单位
  if difference >= 7200 then
    -- 返回过去的小时数
    return "--"
   elseif difference >= 3600 then
    -- 返回过去的分钟数
    return math.floor(difference / 60).."h前"
   elseif difference >= 60 then
    -- 返回过去的分钟数
    return math.floor(difference / 60).."m前"
   else
    -- 返回过去的秒数
    return difference.."s前"
  end
end


--为配置表更新配置
function updata_config(name,value,bool_f) --应该尽量避免value与bool_f同时传入，不太方便判断
  --name为主键，必须传入，即配置项的名称；value是值，varchar类型；bool_f为布尔值，一般传入布尔值即可
  local sql=("UPDATE config SET value = '"..value.."' , bool = '"..bool_f.."' WHERE name='"..name.."';")
  --print(sql)
  if pcall(exec,sql) then
    --MD提示("配置更新成功",0xFF2196F3,0xFFFFFFFF,4,10)
   else
    print("更新配置读写失败!")
  end
end

--为同步表创建新同步
function write_sync(name,key,user_name,count_record,bool_f)
  local sql="insert into sync(name,key,user_name,sync_date,count_record,bool) values('"..name.."','"..key.."','"..user_name.."','"..get_current_time().."','"..count_record.."','"..bool_f.."')"
  --print(sql)
  if pcall(exec,sql) then
    --MD提示("配置读写成功",0xFF2196F3,0xFFFFFFFF,4,10)
   else
    print("配置写入失败!")
  end
end

--传入名称先判定同步笔记本是否存在
function readHave_sync(name)
  local sql="select * from sync where name='"..name.."' "
  if pcall(raw,sql,nil) then
    if cursor.moveToFirst() ~= true then
      return false --当数据库中不存在name时返回false
     else
      return true
    end
   else
    print("数据库判定记录读写失败")
  end
end

--为同步表更新记录
function updata_sync(name,new_name,count_record) -- 暂时只涉及这三个字段
  --name为主键，因为列表只能拿到这个值，所以暂且用这个，问题是不能存在重复名称的单词本
  if readHave_sync(name) then
    local sql=("UPDATE sync SET name = '"..new_name.."' , sync_date = '"..get_current_time().."' WHERE name='"..name.."';")
    if count_record~=-1 then
      sql=("UPDATE sync SET name = '"..new_name.."' , sync_date = '"..get_current_time().."',count_record='"..count_record.."' WHERE name='"..name.."';")
    end
    --print(sql)
    if pcall(exec,sql) then
      --MD提示("配置更新成功",0xFF2196F3,0xFFFFFFFF,4,10)
     else
      print("更新配置读写失败!")
    end
   else
    print("不存在该同步记录!")
  end
end

--传入名称先判定配置项是否存在
function readHave_config(recig_name)
  local sql="select * from config where name='"..recig_name.."' "
  if pcall(raw,sql,nil) then
    if cursor.moveToFirst() ~= true then
      return false --当数据库中不存在name时返回false
    end
   else
    print("数据库判定记录读写失败")
  end
end

--删除表
function drop_table(table_name)
  if readHave_config(table_name) == true then
    delete_record_config(table_name)
  end
  local sql="drop table "..table_name
  if pcall(exec,sql) then
    MD提示("删除表成功",0xFF2196F3,0xFFFFFFFF,4,10)
   else
    print("删除表失败")
  end
end

--数据库判定记录是否存在，分别进行更新与写入记录操作
function Has_up_write_config(name,value,bool_f)
  if readHave_config(name) ~= false then
    updata_config(name,value,bool_f)
   else
    write_config(name,value,bool_f)
  end
end

--读取配置
--传入配置项名称，返回值与类型。只设置其一的话较好判断
function read_config(name)
  if readHave_config(name) == false then
    write_config(name,"","false")
  end
  local sql=("SELECT * FROM config where name='"..name.."'")
  if pcall(raw,sql,nil) then
    while (cursor.moveToNext()) do
      value= cursor.getString(1); --获取第二列
      bool_f = cursor.getString(2);--获取第三列的值
    end
    cursor.close()
   else
    print("数据库配置信息读取失败！")
  end
  if value =="" and bool_f=="false" then
    return false
   elseif value =="" and bool_f=="true" then
    return true
   elseif value ~="" and bool_f=="true" then
    return value
   elseif value ~= "" and bool_f=="" then
    return value:gsub(" ","")
   else
    return value.."%"..bool_f --%为间隔符
  end
end

--随机数据库记录

function random_sqlite(table_name)
  local sql="SELECT * FROM "..table_name.." ORDER BY RANDOM() limit 1;" --随机取一条记录
  if pcall(raw,sql,nil) then
    while (cursor.moveToNext()) do
      cyan= cursor.getString(1);
      name = cursor.getString(2);--获取第三列的值
      if name==nil then --坑了我好久的坑，记录一下。
        name="" --Lua读sqlite列，当列为nil空值时，传回来直接return报错。应该是不能return空值。加个判断就好了。
      end
    end
    --cursor.close() --关闭数据库
   else
    print("数据库查询失败！")
  end
  return cyan.." "..name
end


--查询数据库中的表
function sqlite_name()
  local sql="select * from sqlite_master where type='table'"
  if pcall(raw,sql,nil) then
    list_name={} --创建table，结果将写入此表
    while (cursor.moveToNext()) do
      local name = cursor.getString(1);--获取第二列的值
      if name~= "config" and name~="android_metadata" and name~= "cyan" and name~="sync" and name~="sqlite_sequence" then --排除安卓默认表与配置,一言表.
        table.insert(list_name,name)
      end
    end
    --print(table.concat(list_name,","))
    -- cursor.close()
   else
    print("数据库查询失败！")
  end
end

--计数数据库记录
function sqlite_count(tablename)
  local sql="select count(*) from "..tablename
  if pcall(raw,sql,nil) then
    cursor.moveToFirst(); --移动指针
    local result = cursor.getLong(0);
    return result;
   else
    print("数据库查询失败！")
  end
end

-- 读取同步表信息
function sqlite_sync(sync_name)
  local sql="select * from sync where name='"..sync_name.."'"
  if pcall(raw,sql,nil) then
    local sqlite_sync_data={}
    while (cursor.moveToNext()) do
      local name = cursor.getString(3);--获取第二列的值
      local sync_date = cursor.getString(4);--获取第二列的值
      table.insert(sqlite_sync_data,name)
      table.insert(sqlite_sync_data,sync_date)
    end
    return sqlite_sync_data;
   else
    print("数据库查询失败！")
  end
end

--首页一览表判定
function home_view()
  local sql='select name from config where value="playing" '
  home_list_name={} --创建table，结果将写入此表
  count=0
  if pcall(raw,sql,nil) then
    while (cursor.moveToNext()) do
      local name = cursor.getString(0);--获取第一列的值
      table.insert(home_list_name,name)
    end
    --不知道为什么sqlite_count函数在while中输出会奇奇怪怪，只好单独拿出来
    for i=1,#home_list_name do
      count=count+sqlite_count(home_list_name[i])
    end
    cursor.close()
   else
    print("[local:首页]数据库查询失败！")
  end
  return count;
end

--从数据库中批量添加卡片
function add_list_bullet()
  datas={}
  sqlite_name()--查询数据库中有多少表
  for i=1,#list_name do --开始循环，迭代数据库名称表
    local count=sqlite_count(list_name[i])
    if read_config(list_name[i]) =="playing" then
      if readHave_sync(list_name[i]) == true then
        local content=sqlite_sync(list_name[i])
        local time_passed = calculate_time_passed(content[2])
        table.insert(datas,{table_title=list_name[i],table_count=content[1].."分享: "..count.."发  "..time_passed,table_tips="弹幕中..."})
       else
        table.insert(datas,{table_title=list_name[i],table_count="弹药量："..count.."发",table_tips="弹幕中..."})
      end
     else
      if readHave_sync(list_name[i]) == true then
        local content=sqlite_sync(list_name[i])
        local time_passed = calculate_time_passed(content[2])
        table.insert(datas,{table_title=list_name[i],table_count=content[1].."分享: "..count.."发  "..time_passed})
       else
        table.insert(datas,{table_title=list_name[i],table_count="弹药量："..count.."发"})
      end
    end
  end --添加到适配器
  adp=LuaAdapter(activity,datas,item)
  gd.Adapter=adp
  --单击事件
  gd.onItemClick=function(l,v,p,i)
    --hide_list_card.setVisibility(View.GONE)
    local name=list_name[i]
    activity.newActivity("Mlua/act",{name})--跳转页面
    return true
  end
  --项目被长按
  gd.onItemLongClick=function(l,v,p,i)
    message_card(list_name[i])
    管理卡片弹窗.show()
    return true
  end
end

function update()
  add_list_bullet()--再次查询数据库，更新列表
  adp.notifyDataSetChanged()--更新适配器
end

--activity返回事件，更新列表
function onResume()
  update()
end

--悬浮窗事件
function xfc_list_table_fun()
  sqlite_name()
  xfc_adp=LuaAdapter(activity,xfc_item)
  xfc_list_table.setAdapter(xfc_adp)
  for i=1, #list_name do--循环取值
    xfc_adp.add{--构建视图控件
      xfc_list_table_name=list_name[i],--表名
    }
  end
end

窗口 = activity.getSystemService(Context.WINDOW_SERVICE)

function 载入悬浮球()
  if(read_config("fab")==false) then
    窗口.addView(悬浮球,窗口容器)
    悬浮球.setLayoutAnimation(动画容器)
    Has_up_write_config("fab","","true")
  end
end

function 关闭悬浮球()
  --开关本体.setText("关闭悬浮")
  if(read_config("fab")==true)then
    窗口.removeView(悬浮球)
    Has_up_write_config("fab","","false")
  end
end

function 载入悬浮窗()
  local 坐标={x=2,y=2}
  local 动画对象 = AnimationSet(true)
  local 动画容器 = LayoutAnimationController(动画对象,0.2)
  local 渐变动画 = AlphaAnimation(0,1)
  local 滑入动画 = TranslateAnimation(-100,0,0,0)
  动画容器.setOrder(LayoutAnimationController.ORDER_NORMAL)
  渐变动画.setDuration(500)
  滑入动画.setDuration(500)
  动画对象.addAnimation(渐变动画)
  动画对象.addAnimation(滑入动画)
  if(read_config("faw")==false)then
    悬浮窗.setLayoutAnimation(动画容器)
    窗口.addView(悬浮窗,悬浮窗口容器)
    关闭悬浮球()
    Has_up_write_config("faw","","true")
  end
end


xfc_list_table.onItemClick=function(a,b)
  xfc_list_name=b.Tag.xfc_list_table_name.Text
  xfc_list_title.setText("快来塞满"..xfc_list_name.."吧")
  Control_xfc_list_table.setBackgroundColor(Color.parseColor("#ff9900"))
  xfc_list_table.setVisibility(View.GONE)
  --print(a.Tag.xfc_list_table_name.Text)
  return true
end

function xfc_top_tips(text)
  top_tip_linear.startAnimation(TranslateAnimation(0,0,-top_tip_linear.height,0).setDuration(200))
  top_tip_linear.setVisibility(View.VISIBLE)
  top_tip_text.setText(text)
  --作者：Paixs
  task(1000,function()
    top_tip_linear.setVisibility(View.GONE)
    top_tip_linear.startAnimation(TranslateAnimation(0,0,0,-top_tip_linear.height).setDuration(200))
  end)
end

--重命名事件
function user_name()
  if read_config("users_name")~=false then
    user_title.setText(read_config("users_name"))
   else
    user_title.setText("你好9527")
  end
end

--悬浮窗翻译函数
--参见百度翻译api接口文档
function translator(txt,to_str)
  local random=随机数(100,10000)
  local appid="xxxxxxxxxxxxxx"
  md5=appid..txt..random.."xxxxxxxxxxxxxxxxxxx" --开源文件请使用自己的参数,
  if xfc_trans_zh.getVisibility()==0 then--显示0--隐藏8
    --判断是中译英还是英译中，请使用自己的appid
    url="https://api.fanyi.baidu.com/api/trans/vip/translate?q="..URLEncoder.encode(txt).."&from=auto&to=zh&appid="..appid.."&salt="..random.."&sign="..string.lower(MD5(md5))..""
   else
    url="https://api.fanyi.baidu.com/api/trans/vip/translate?q="..URLEncoder.encode(txt).."&from=auto&to=en&appid="..appid.."&salt="..random.."&sign="..string.lower(MD5(md5))..""
  end
  return url
end

-- tts已实现，但并未应用

import "android.speech.tts.*"

function tts(content)
  mTextSpeech = TextToSpeech(activity, TextToSpeech.OnInitListener{
    onInit=function(status)
      --如果装载TTS成功
      if (status == TextToSpeech.SUCCESS)
        result = mTextSpeech.setLanguage(Locale.CHINESE);
        --[[LANG_MISSING_DATA-->语言的数据丢失
          LANG_NOT_SUPPORTED-->语言不支持]]
        if (result == TextToSpeech.LANG_MISSING_DATA or result == TextToSpeech.LANG_NOT_SUPPORTED)
          --不支持中文
          xfc_top_tips("您的手机不支持中文语音合成");
          result = mTextSpeech.setLanguage(Locale.ENGLISH);
          if (result == TextToSpeech.LANG_MISSING_DATA or result == TextToSpeech.LANG_NOT_SUPPORTED)
            --不支持中文和英文
            xfc_top_tips("您的手机不支持语音合成");
           else
            --不支持中文但支持英文
            --语调,1.0默认
            mTextSpeech.setPitch(2);
            --语速,1.0默认
            mTextSpeech.setSpeechRate(1);
            mTextSpeech.speak(content, TextToSpeech.QUEUE_FLUSH, nil);
          end
         else
          --支持中文
          --语调,1.0默认
          mTextSpeech.setPitch(3);
          --语速,1.0默认
          mTextSpeech.setSpeechRate(2);
          mTextSpeech.speak(content, TextToSpeech.QUEUE_FLUSH, nil);
        end
      end
    end
  });
end

--每日翻译
function translate_today()
  local url = 'https://web.shanbay.com/opp/quotes/today'
  local 是否使用随机日期 = false
  function dateChange(time,dayChange)--来自大佬的日期切换
    if string.len(time)==10 and string.match(time,"%d%d%d%d%-%d%d%-%d%d") then
      local year=string.sub(time,0,4);--年
      local month=string.sub(time,6,7);--月
      local day=string.sub(time,9,10);--日
      local time=os.time({year=year, month=month, day=day})+dayChange*86400 --一天86400秒
      return os.date('%Y',time).."-"..os.date('%m',time).."-"..os.date('%d',time)
     else
      return "请输入YYYY-MM-DD格式的日期"
    end
  end
  local date = dateChange("2019-11-06",math.random(1,666))

  if 是否使用随机日期 then
    local url = 'https://web.shanbay.com/opp/quotes/'..date
    print(date)
  end
  --使用Http访问该网站，获取网页源码
  Http.get(url,nil,'utf8',nil,function(状态码,网页源码)
    --判断返回状态码，确定网站的正常运行
    if 状态码 ==200 then
      --print(网页源码)
      原句=网页源码:match([[p class="content">(.-)</p><p class="translation]])
      --  print("原句:"..原句)
      翻译=网页源码:match([[<meta name="description" content="(.-)"><meta name="share_img" content]])
      --  print("翻译:"..翻译)
      图片=网页源码:match([[<meta name="share_img" content="(.-)"><link rel="icon"]])
      -- print("图片链接:"..图片)--感谢大佬帮忙修改
      summary.setText("你需要使用自己的参数\n"..原句.."\n\n"..翻译)
      --[[webView.getSettings().setSupportZoom(true); --支持网页缩放
    webView.getSettings().setBuiltInZoomControls(true); --支持缩放
    webView.getSettings().setLoadWithOverviewMode(true);--缩放至屏幕的大小
    webView.getSettings().setDisplayZoomControls(false); --隐藏自带的右下角缩放控件
    webView.getSettings().setLoadsImagesAutomatically(true);--图片自动加载
    webView.getSettings().setUseWideViewPort(true) --图片自适应
    webView.loadUrl(图片)--加载网页]]
     else
      print('获取内容失败')
    end
  end)
end

function newIntent(name)
  import "android.net.Uri"
  import "android.content.Intent"
  intent = Intent(activity,LuaActivity);
  intent.putExtra("name",name);
  path=activity.getLuaDir().."/"..name..".lua"
  intent.setData(Uri.parse("file://"..path));
  activity.startActivity(intent)
end

--通知栏推送
function push_notise(notice_id,title,content,value)
  --设置标题
  builder.setContentTitle(title);
  --设置内容
  builder.setContentText(content);
  --设置状态栏显示的图标，建议图标颜色透明
  builder.setSmallIcon(R.drawable.icon)

  builder.setWhen(System.currentTimeMillis())--通知时间
  builder.setShowWhen(true)--是否显示通知时间
  builder.setLargeIcon(loadbitmap("null"))--右侧图标

  --添加完成事件
  --intentDone = Intent(mDoneAction)
  NotificationManager = activity.getSystemService(Context.NOTIFICATION_SERVICE)

  --创建 Intent
  NotificationIntent = Intent("android.intent.action.VIEW",Uri.parse(""));
  NotificationIntent.setClassName(--[[this.getPackageName()]]"com.nrhs.deskbullet","com.androlua.Main")
  --创建 PendingIntent
  pendingIntent = PendingIntent.getActivity(activity.getApplicationContext(),0,NotificationIntent,1)

  hangIntent = Intent();
  hangIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
  --hangIntent.setClass(Context,activity.getApplicationContext())
  --如果描述的PendingIntent已经存在,则在产生新的Intent之前会先取消掉当前的
  hangPendingIntent = PendingIntent.getActivity(activity.getApplicationContext(),0,hangIntent,PendingIntent.FLAG_CANCEL_CURRENT)
  builder.setFullScreenIntent(hangPendingIntent,true)
  builder.setContentIntent(pendingIntent)--点击后要跳转的intent

  --builder.addAction(R.drawable.icon, "弹幕笔记", pendingIntent)--通知上的操作需要Android10
  --builder.addAction(R.drawable.icon, "关闭", pendingIntent)--通知上的操作需要Android10
  if value=="long" then
    builder.setOngoing(true); --常驻通知，为true时用户不可通过清除操作清除该通知
    builder.setAutoCancel(false); --自动取消
   else
    builder.setOngoing(false);
    builder.setAutoCancel(true); --自动取消
  end
  --builder.setContentIntent(pendingIntent); --点击事件
  builder.setPriority(Notification.PRIORITY_HIGH);--通知等级

  --builder.setLargeIcon(BitmapFactory.decodeResource(activity.getResources(), R.drawable.icon));

  if Build.VERSION.SDK_INT >= Build.VERSION_CODES.O
    channel = NotificationChannel("5996773", "弹幕笔记", NotificationManager.IMPORTANCE_DEFAULT);
    channel.enableLights(true);--是否在桌面icon右上角展示小红点
    channel.setLightColor(Color.GREEN);--小红点颜色
    channel.setShowBadge(false); --是否在久按桌面图标时显示此渠道的通知
    mNManager.createNotificationChannel(channel);
  end
  notification=builder.build();

  mNManager.notify(notice_id,notification);
end

function clear_push(notice_id) --传入通知id
  mNManager.cancel(notice_id);--消除相应ID的通知
  --mNotificationMgr.cancleAll(); --清除所有通知
end

--传入文本id，改变文本，1秒后改回来
function get_and_to(name,content)
  local txt=name.getText()
  name.setText(content)
  name.startAnimation(TranslateAnimation(0,0,-name.height,0).setDuration(200))
  task(1500,function()
    name.setText(txt)
    name.startAnimation(TranslateAnimation(0,0,0,-name.height).setDuration(200))
  end)
end

--主动关闭弹幕
function sys_close()
  start_bullet_tip.setText("重新开启弹幕（ ’ - ’ * )")
  start_bullet.setBackgroundColor(Color.parseColor("#84B1ED"))
  Has_up_write_config("bullet_state","","false")
  MD提示("已关闭弹幕",0xFFFFFFFF,0x9C000000,4,10)
end

--中文过滤函数
function filter_spec_chars(s)
  local ss = {}
  for k = 1, #s do
    local c = string.byte(s,k)
    if not c then break end
    if (c>=48 and c<=57) or (c>= 65 and c<=90) or (c>=97 and c<=122) then
      if not string.char(c):find("%w") then
        table.insert(ss, string.char(c))
      end
     elseif c>=228 and c<=233 then
      local c1 = string.byte(s,k+1)
      local c2 = string.byte(s,k+2)
      if c1 and c2 then
        local a1,a2,a3,a4 = 128,191,128,191
        if c == 228 then a1 = 184
         elseif c == 233 then a2,a4 = 190,c1 ~= 190 and 191 or 165
        end
        if c1>=a1 and c1<=a2 and c2>=a3 and c2<=a4 then
          k = k + 2
          table.insert(ss, string.char(c,c1,c2))
        end
      end
    end
  end
  return table.concat(ss)
end

--内容长度判断函数
--传入内容返回字符数量，中文与英文同时处理，为确保长度不过分长，中英文长度同时/2，原中文3，英文1
function count_zh_en(content)
  local count=#filter_spec_chars(content)/2+#content:gsub(filter_spec_chars(content),"")/2
  return count;
end

--后台隐藏

function hidden_progress(s)
  import "android.app.ActivityManager"
  import "android.content.Intent"
  import "android.os.Build"
  import "android.content.Context"
  local activityManager = activity.getSystemService(Context.ACTIVITY_SERVICE)
  local packageName = activity.getPackageName()
  if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) then
    local appTask = activityManager.appTasks
    appTask[0].setExcludeFromRecents(s)
   else
    local intent = Intent(Intent.ACTION_MAIN)
    intent.addCategory(Intent.CATEGORY_HOME)
    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
    activity.startActivity(intent)
  end
end

-- 下滑刷新同步笔记函数

-- 创建同步表时自动同步记录
function create_sync_data(KEY)
  local url="http://bullet.nrhs.eu.org/device/API.php?getNoteData="..KEY
  Http.get(url,nil,"utf-8",nil,function(code,content)
    if code==200 then
      local data=json.decode(content)
      if data["code"]==404 then
        print("未找到笔记本")
        return false
       elseif data["code"]==401 then
        print("未知的KEY")
        return false
       else
        local name=data["data"]["summary"][1]["note_name"]
        local user_name=data["data"]["summary"][1]["name"]
        addtable(name)
        Has_up_write_config(name,"","")
        -- 开始插入数据
        local v=0
        local MAX_order=0
        for _, item in ipairs(data.data.data_list) do
          --print(item.choseWord .. ": " .. item.transWord)
          addrecord(name,item.choseWord,item.transWord)
          MAX_order=item.ID
          v=v+1
        end
        --同步完成
        write_sync(name,edit.Text,user_name,MAX_order,'true')
        提示("已同步"..name.."共"..v.."条笔记")
        --提示(get_current_time())
        update("adp")--更新数据
        return true
      end
     else
      提示("同步失败")
    end
  end)
end;

-- 刷新笔记数据
function sync_record(KEY,ID,name)
  local url="http://bullet.nrhs.eu.org/device/API.php?getNoteData="..KEY.."&MaxID="..ID
  Http.get(url,nil,"utf-8",nil,function(code,content)
    if code==200 then
      local data=json.decode(content)
      local note_key=data["data"]["summary"][1]["note_key"]
      local user_name=data["data"]["summary"][1]["name"]
      -- 如果存在ID>本地的记录，则同步插入
      if data.data and data.data.data_list and #data.data.data_list > 0 then
        local v=0
        local MAX_order=0
        for _, item in ipairs(data.data.data_list) do
          addrecord(name,item.choseWord,item.transWord)
          MAX_order=item.ID
          v=v+1
        end
        --同步完成
        updata_sync(name,name,MAX_order)
        提示(name.." +"..v.."条笔记")
        update("adp")--更新数据
      end
      update("adp")--更新数据
      updata_sync(name,name,-1) --未更新数据也应该更新时间
      return true
     else
      提示("同步失败")
    end
  end)
end;

-- 计数数据库记录，如果记录与云端记录不匹配，则更新id>本地的数据。需要请求时携带ID值

function refresh_sync()
  local sql="select * from sync where bool='true'" --获取同步表中的记录
  if pcall(raw, sql, nil) then
    -- 检查是否有至少一行数据
    if cursor.moveToFirst() then
      repeat
        name = cursor.getString(1)
        key = cursor.getString(2) -- 获取第三列的值
        count_record = cursor.getString(5)
        --print("开始时更新"..name..key..count_record)
        sync_record(key,count_record,name)
      until not cursor.moveToNext()
     else
      print("无同步笔记")
    end
    --cursor.close() -- 关闭数据库
   else
    print("数据库查询失败！")
  end
end

Pulling.onRefresh=function(v)
  update("adp")
  refresh_sync() --刷新方法
  update() --更新adp
  task(1000,function()--设置1秒后执行
    v.refreshFinish(0)
    --[[括号里面的数字0为刷新成功
    1为刷新失败
    2为没有更新的容的提示
    2以后都为加载失败！]]
  end)
end

-- onDropDown=false --状态记录

-- function toZero(to,mode) --还原方法
--   mVy=mView.getY()
--   thread(function(mVy) --线程
--     for i=1,mVy/10 do --循环
--       call("setYdh",mVy) --运行主线程方法
--       this.sleep(1) --间隔
--     end
--   end,mVy)
--   function setYdh(mVy) --用于线程调用
--     if mView.getY()<=30 and mView.getY()>8 then --筏值
--       mView.setY(0) --如果是，直接设置Y为0
--       --提示('下拉同步笔记')
--       onDropDown=false
--       return --结束func
--      else
--       mView.setY(mView.getY()-10) --动画
--     end
--   end
--   mFlashText.Text="" --刷新后
-- end

-- function reNew(to,func) --刷新方法
--   howFar=mView.getY()-to --距离
--   onDropDown=true --状态
--   thread(function(howFar) --线程
--     for i=1,howFar/10 do
--       call("dh",howFar)
--       this.sleep(1)
--       if i+1>=howFar/10 then
--         call("doing")
--       end
--     end
--   end,howFar)
--   function dh(howFar) --动画
--     mView.setY(mView.getY()-10)
--     mFlash.setY(mView.getY()-80)
--   end
--   function doing() --刷新的操作
--     mFlashText.Text="正在同步请稍后..."
--     refresh_sync() --刷新方法
--     if func~=nil then
--       func() --运行操作
--     end
--     task(400,function()
--       update()
--       mFlashText.Text="刷新完成 " --刷新后
--       --MI_tip("同步完成")
--       task(350,function()
--         mFlashText.Text="" --刷新后
--         toZero() --还原
--       end)
--     end)
--   end
-- end

-- mView.setOnTouchListener{
--   onTouch=function(view,event)
--     if onDropDown==true then --如果在刷新
--       return false
--      else
--       if event.getAction()==MotionEvent.ACTION_DOWN then
--         mVy=view.getY() --记录点击时控件的Y
--         moveY=event.RawY --记录点击时Y
--        elseif event.getAction()==MotionEvent.ACTION_MOVE then
--         local far=mVy+(event.RawY-moveY) --记录长度
--         if far>40 and far<900 then --设置筏值 ，判断上下滑
--           view.setY(far) --设置Y
--           mFlash.setY(far-80)
--         end
--        elseif event.getAction()==MotionEvent.ACTION_UP or event.getAction()==ACTION_CANCEL then
--         if view.getY()>350 then --则刷新
--           reNew(350,function() update("adp") end) --更新列表
--          else --＜500则还原
--           toZero()
--         end
--       end
--       return true
--     end
--   end}

-- 通知栏调用
function 通知(标题,内容,id)
  function createNotificationChannel(channelID, channelNAME, level)
    if Build.VERSION.SDK_INT >= Build.VERSION_CODES.O then
      manager = activity.getSystemService(Context.NOTIFICATION_SERVICE)
      channel = NotificationChannel(channelID, channelNAME, level)
      manager.createNotificationChannel(channel)
      return channelID
     else
      return nil
    end
  end
  import "android.content.Intent"
  import "android.content.Context"
  intent = Intent()
  intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK)
  pendingIntent = PendingIntent.getActivity(this, 0, intent, 0)
  channelId = createNotificationChannel(tostring(math.random(0,10000)), "小芒果提示框架", NotificationManager.IMPORTANCE_HIGH)
  notification = Notification.Builder(this, channelId)
  .setContentTitle(标题)
  .setContentText(内容)
  --第2段
  .setSmallIcon(R.drawable.icon)
  --.setContentIntent(pendingIntent)
  --.setPriority(Notification.PRIORITY_MAX)
  --.setDefaults(Notification.DEFAULT_ALL)
  .setAutoCancel(true)
  .build()
  notificationManager = activity.getSystemService(Context.NOTIFICATION_SERVICE)
  notificationManager.notify(id, notification)
end

-- 调用示例： 通知("我是标题","我是内容",114514)

-- 模仿小米消息提示
function MI_tip(...)
  --判断数据类型
  if type(...) == "table" then
    --dump解析一下
    toastText = dump(...)
   else
    --初始化表
    local contentTable = table.pack(...)
    --获取表长度
    local max = table.maxn(contentTable)
    --初始化变量
    toastText = ""
    --初始化第一项内容
    toastText = tostring(contentTable[1])
    --循环遍历表数据
    for i = 2, max do
      --读取数据表内容
      local temp = contentTable[i]
      --判断该项数据类型
      if type(temp) == "table" then
        --dump 解析一下
        local temp = dump(temp)
        --拼接到原有字符串后部
        toastText = toastText.."\n"..temp
       else
        --拼接到原有字符串后部
        toastText = toastText.."\n".. tostring(temp)
      end
    end
  end
  --导入所需类
  local Toast = luajava.bindClass "android.widget.Toast"
  --输出表内容
  Toast.makeText(activity,tostring(toastText),Toast.LENGTH_SHORT).show()
end
