--导出表内容

import "AndLua"
import "android.graphics.Typeface"
import "java.io.File"
function ttf_lay()
  return Typeface.createFromFile(File(activity.getLuaDir().."/res/Alimama_ShuHeiTi_Bold.ttf"))
end
下载弹窗=
{--布局
  LinearLayout;
  orientation="vertical";
  Focusable=true,
  FocusableInTouchMode=true,
  {
    TextView;--文本控件
    gravity='center';--重力
    --左:left 右:right 中:center 顶:top 底:bottom
    layout_width='fill';--宽度
    layout_height='40dp';--高度
    textColor='#ff51acf6';--文字颜色
    text='导出文件';--显示文字
    textSize='20';--文字大小
    layout_weight="1.0";
    layout_marginTop="fill";--边顶
    Typeface=ttf_lay(),
  };
  {
    LinearLayout,--线性布局
    orientation='vertical',--方向
    layout_width='fill',--宽度
    layout_height='wrap',--高度
    background='#00FFFFFF',--背景颜色或图片路径
    {--名称
      LinearLayout;
      orientation='horizontal';--重力属性
      layout_width='fill';--布局宽度
      layout_height='wrap';--布局高度
      gravity='center';--默认居中
      backgroundColor="#FFFFFFFF";--背景色
      {
        CardView;--卡片控件
        layout_marginTop='15dp';--顶距
        layout_margin='8dp';--边距
        layout_gravity='center';--重力
        --左:left 右:right 中:center 顶:top 底:bottom
        elevation='0dp';--阴影
        layout_width='fill';--宽度
        layout_height='12%w';--高度
        CardBackgroundColor='0';--颜色
        radius='8dp';--圆角
        {
          CardView;--卡片控件
          layout_margin='0dp';--边距
          layout_gravity='center|left';--重力
          --左:left 右:right 中:center 顶:top 底:bottom
          elevation='0dp';--阴影
          layout_width='65%w';--宽度
          layout_height='12%w';--高度
          CardBackgroundColor='#2a000000';--颜色
          radius='8dp';--圆角
          {
            EditText;--编辑框控件
            id="edit_txt_name";
            singleLine=true,--设置单行输入
            layout_width='fill';--编辑框宽度
            layout_height='fill';--编辑框高度
            textSize='12sp';--文字大小
            hintTextColor='#FF7D7D7D';--提示文字颜色
            textColor='#FF000000';--输入文字的颜色
            Hint=to_table_name;--设置编辑框为空时的提示文字
            background='#ffefefef';
            layout_marginLeft='0dp';--布局左距
            layout_marginRight='0dp';--布局右距
          };
        };
        {
          TextView;--文本控件
          gravity='center|right';--重力
          --左:left 右:right 中:center 顶:top 底:bottom
          layout_width='fill';--宽度
          layout_marginRight='5%w';--右距
          layout_height='fill';--高度
          textColor='#eb000000';--文字颜色
          text='.txt';--显示文字
          textSize='17dp';--文字大小
          Typeface=ttf_lay(),
          -- textIsSelectable=true--长按复制
        };
      };
    };
  };
  --[[{
    CardView;--卡片控件
    layout_marginRight='6dp';--右距
    layout_margin='8dp';--边距
    elevation='1dp';--阴影
    layout_width='fill';--宽度
    layout_height='3dp';--高度
    CardBackgroundColor='#FFDFDFDF';--颜色
    radius='5dp';--圆角
    id="button";
    {
      TextView;--文本控件
      --gravity='center';--重力
      layout_height='fill';--高度
      background='#FF5B81FE',--背景颜色或图片路径
      text='';--显示文字
      id="jv";
    };
  };]]
  {
    LinearLayout;
    orientation='horizontal';--重力属性
    layout_width='fill';--布局宽度
    layout_height='fill';--布局高度
    {
      TextView;--文本控件
      gravity='center';--重力
      --左:left 右:right 中:center 顶:top 底:bottom
      layout_width='43.5%w';--宽度
      textColor='#ff51acf6';--文字颜色
      text='取消';--显示文字
      textSize='15dp';--文字大小
      style="?android:attr/borderlessButtonStyle";
      onClick=function()
        保存弹窗.dismiss()
      end
    };
    {
      TextView;--文本控件
      layout_gravity='center';--重力
      --左:left 右:right 中:center 顶:top 底:bottom
      layout_width='43.5%w';--宽度
      textColor='#ff51acf6';--文字颜色
      text='导出';--显示文字
      textSize='15dp';--文字大小
      style="?android:attr/borderlessButtonStyle";
      id="start_down";
    };
    {
      TextView;--文本控件
      layout_gravity='center';--重力
      --左:left 右:right 中:center 顶:top 底:bottom
      layout_width='43.5%w';--宽度
      textColor='#ff51acf6';--文字颜色
      text='打开文件';--显示文字
      textSize='15dp';--文字大小
      style="?android:attr/borderlessButtonStyle";
      id="over_down";
      Typeface=ttf_lay(),
    };
  };
};
保存弹窗=AlertDialog.Builder(this).setView(loadlayout(下载弹窗)).show()
保存弹窗.dismiss()
over_down.setVisibility(View.GONE)

--数据库
db = SQLiteDatabase.openOrCreateDatabase(this.getLuaDir() .. "/bullet.db",MODE_PRIVATE, nil);

--execSQL()方法可以执行insert、delete、update和CREATE TABLE之类有更改行为的SQL语句
function exec(sql)
  db.execSQL(sql);
end

--rawQuery()方法用于执行select语句。
function raw(sql,text)
  cursor=db.rawQuery(sql,text)
end

function sqlite_count_down(tablename)
  local sql="select count(*) from "..tablename
  if pcall(raw,sql,nil) then
    cursor.moveToFirst(); --移动指针
    local result_down = cursor.getLong(0);
    return result_down;
   else
    print("数据库查询失败！")
  end
end

function 分享文件(标题,路径)
  import "android.webkit.MimeTypeMap"
  import "android.content.Intent"
  import "android.net.Uri"
  FileName=tostring(File(路径).Name)
  ExtensionName=FileName:match("%.(.+)")
  Mime=MimeTypeMap.getSingleton().getMimeTypeFromExtension(ExtensionName)
  intent=Intent()
  intent.setAction(Intent.ACTION_SEND)
  intent.setType(Mime)
  file=File(路径)
  uri=Uri.fromFile(file)
  intent.putExtra(Intent.EXTRA_STREAM,uri)
  intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
  activity.startActivity(Intent.createChooser(intent,标题))
end

over_down.onClick=function()--点击事件
  保存弹窗.dismiss()
  over_down.setVisibility(View.GONE)
  start_down.setVisibility(View.VISIBLE)
  activity.newActivity("Mlua/edit",{"sdcard/弹幕笔记/导出文件/"..outname..".txt"})--跳转页面
  --分享文件("分享武器","sdcard/弹幕笔记/导出文件/"..outname..".txt")
end;

function down_stop(txt_name)
  MD提示("文件已保存在/弹幕笔记/导出文件/"..outname..".txt!",0xFF2196F3,0xFFFFFFFF,4,10)
  --dialog1.dismiss()--关闭对话框
  start_down.setVisibility(View.GONE)
  over_down.setVisibility(View.VISIBLE)
  task(3000,function()--1000毫秒=1秒
    over_down.setVisibility(View.GONE)
    start_down.setVisibility(View.VISIBLE)
  end)
end

function for_down(txt_name)
  --弹出消息(tostring(txt_name))
  outname=txt_name
  local sql="select * from "..to_table_name
  local w_table=sqlite_count_down(to_table_name)
  if pcall(raw,sql,nil) then
    while (cursor.moveToNext()) do
      local uid = cursor.getInt(0); --获取第一列的值,第一列的索引从0开始
      local cyan = cursor.getString(1);--获取第二列的值
      local remark = cursor.getString(2);--获取第三列的值
      if remark=="" then
        records=cyan
       else
        records=cyan.."┃"..remark
      end
      --弹出消息(records)
      io.open("sdcard/弹幕笔记/导出文件/"..txt_name..".txt","a+"):write(records.."\n"):close()
      start_down.setText(uid.."/"..w_table)
      --jv.Width=s/w_table*1000
    end
    --提示(table.concat(cyan_list,","))
    down_stop(txt_name)
    start_down.setText("导出")
    cursor.close()
   else
    print("数据库数据导出失败！")
  end
end

start_down.onClick=function()
  if 文件是否存在("/sdcard/弹幕笔记/导出文件") then
   else
    创建多级文件夹("/sdcard/弹幕笔记/导出文件")
  end
  if edit_txt_name.Text=="" then
    if 文件是否存在("/sdcard/弹幕笔记/导出文件/"..to_table_name..".txt") then
      --文件存在事件
      AlertDialog.Builder(this)
      .setTitle("已存在["..to_table_name..".txt]文件，是否覆盖？")
      .setMessage("确认将覆盖原内容")
      .setPositiveButton("确认",{onClick=function(v)
          io.open("/sdcard/弹幕笔记/导出文件/"..to_table_name..".txt","w"):write(""):close()
          for_down(to_table_name)
        end})
      .setNeutralButton("",nil)
      .setNegativeButton("取消",nil)
      .show()
      --对话框
     else io.open("/sdcard/弹幕笔记/导出文件/"..to_table_name..".txt","w")
      for_down(to_table_name)
    end
   else
    for_down(edit_txt_name.Text)
    File("sdcard/弹幕笔记/导出文件/"..edit_txt_name.Text..".txt").createNewFile()
  end
end