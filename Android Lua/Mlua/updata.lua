--开源作者爱梅の日常
require "import"
import "android.app.*"
import "android.os.*"
import "android.widget.*"
import "android.view.*"
import "java.io.FileOutputStream"
import "com.androlua.Http"
import "java.io.File"
import "java.net.URL"
import "com.androlua.Http"
import "android.content.Intent"
import "AndLua"

function 弹窗圆角(控件,背景色,上角度,下角度)
  if not 上角度 then
    上角度=25
  end
  if not 下角度 then
    下角度=上角度
  end
  控件.setBackgroundDrawable(GradientDrawable()
  .setShape(GradientDrawable.RECTANGLE)
  .setColor(背景色)
  .setCornerRadii({上角度,上角度,上角度,上角度,下角度,下角度,下角度,下角度}))
end

function 浏览器打开(链接)
  import "android.content.Intent"
  import "android.net.Uri"
  viewIntent = Intent("android.intent.action.VIEW",Uri.parse(链接))
  activity.startActivity(viewIntent)
end
--import "android.graphics.Typeface"

远程链接="https://wds.ecsxs.com/1346.html"--水印君远程链接
appinfo=this.getPackageManager().getApplicationInfo(this.getPackageName(),0)
程序名=this.getPackageManager().getApplicationLabel(appinfo)
packinfo=this.getPackageManager().getPackageInfo(this.getPackageName(),((32552732/2/2-8183)/10000-6-231)/9)
当前版本=tostring(packinfo.versionName)

Http.get(远程链接,nil,nil,nil,function(code,content)
  if code==200 then
    最新版本=content:match("最新版本【(.-)】")
    更新内容=content:match("更新内容【(.-)】"):gsub("%d.","\n\n")
    if 最新版本>当前版本 then
      弹窗=
      {
        FrameLayout,--帧布局
        layout_width='fill',--宽度
        {
          LinearLayout,--线性布局
          orientation='vertical',--方向
          layout_width='fill',--宽度
          {
            LinearLayout,--线性布局
            padding="6dp",--往内部元素的填充一定边距
            layout_gravity='center';--重力
            orientation='vertical',--方向
            layout_width='70%w',--宽度
            layout_height='wrap',--高度
            background='#ffFFFFFF',--背景颜色或图片路径
            {
              LinearLayout,--线性布局
              orientation='horizontal',--方向
              layout_width='fill',--宽度
              {
                LinearLayout,--线性布局
                gravity='left';--卡片重力
                orientation='horizontal',--方向
                layout_width='fill',--宽度
                {
                  TextView;--文本控件
                  textColor='#98000000';--文字颜色
                  text='新的版本:';--显示文字
                  textSize='15dp';--文字大小
                  layout_marginTop='10dp';--顶距
                  layout_marginLeft='2dp';--左距
                  Typeface=Typeface.DEFAULT_BOLD;--字体加粗
                };
                {
                  TextView;--文本控件
                  layout_marginlLeft='5dp';--右距
                  textColor='#85000000';--文字颜色
                  text=当前版本.."→"..最新版本;--文本内容
                  layout_marginTop='10dp';--顶距
                  Typeface=Typeface.DEFAULT_BOLD;--字体加粗
                };
              };
            };
            {
              LinearLayout,--线性布局
              --  gravity='center';--重力
              orientation='vertical',--方向
              layout_width='fill',--宽度
              {
                ScrollView,--纵向滑动
                layout_width='fill';--宽度
                layout_height='wrap';--高度
                VerticalScrollBarEnabled=false;--隐藏纵向滑条
                --布局写这
                {
                  TextView;--文本控件
                  layout_marginLeft='5dp';--左距,--
                  layout_marginRight='5dp';--右距
                  textColor='#85000000';--文字颜色
                  text=更新内容;--显示文字
                  layout_marginTop='10dp';--顶距
                };
              };
            };
          };
          {
            LinearLayout,--线性布局
            layout_gravity='center';--重力
            padding="6dp",--往内部元素的填充一定边距
            orientation='horizontal',--方向
            layout_width='80%w',--宽度
            layout_height='wrap',--高度
            {
              CardView;--卡片控件
              layout_margin='8dp';--边距
              layout_gravity='center';--重力
              elevation='0dp';--阴影
              layout_width='fill';--宽度
              layout_height='wrap';--高度
              CardBackgroundColor='0';--颜色
              radius='0dp';--圆角
              {
                CardView;--卡片控件
                layout_margin='22dp';--边距
                layout_gravity='left|center';--重力
                elevation='0dp';--阴影
                layout_width='100dp';--宽度
                layout_height='40dp';--高度
                CardBackgroundColor='#FFEEEEEE';--颜色
                radius='10dp';--圆角
                id="last_p";
                onClick=function()
                  浏览器打开("http://pan.nrhs.eu.org/K%E7%9A%84%E8%89%BA%E6%9C%AF/Resource/DeskBullet")
                end,
                {
                  TextView;--文本控件
                  gravity='center';--重力
                  --左:left 右:right 中:center 顶:top 底:bottom
                  layout_width='fill';--宽度
                  layout_height='fill';--高度
                  textColor='#eb000000';--文字颜色
                  text='历史版本';--显示文字
                  textSize='14dp';--文字大小
                };
              };
              {
                CardView;--卡片控件
                layout_margin='8dp';--边距
                layout_gravity='right|center';--重力
                elevation='0dp';--阴影
                layout_width='100dp';--宽度
                layout_height='40dp';--高度
                CardBackgroundColor='#FF5187F4';--颜色
                radius='10dp';--圆角
                id="down_p";
                onClick=function()
                  浏览器打开("http://pan.nrhs.eu.org/K%E7%9A%84%E8%89%BA%E6%9C%AF/Resource/DeskBullet/弹幕笔记_"..最新版本..".apk")
                end;
                {
                  TextView;--文本控件
                  gravity='center';--重力
                  layout_width='fill';--宽度
                  layout_height='fill';--高度
                  textColor='#ffffffff';--文字颜色
                  text='上新武器';--显示文字
                  textSize='14dp',--文字大小
                };
              };
            };
          };
        };
      };
      mmtc=AlertDialog.Builder(this)
      --.setCancelable(false)--禁止返回键
      .setView(loadlayout(弹窗))
      .show()
      import "android.view.View$OnFocusChangeListener"
      mmtc.create()
      弹窗圆角(mmtc.getWindow(),0xffffffff)
    end
   else
    提示("服务读取异常")
  end
end)
