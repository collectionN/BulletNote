--简单的编辑器，但并未完全开发。目前仅完成预览功能。

require "import"
import "android.app.*"
import "android.os.*"
import "android.widget.*"
import "android.view.*"
import "com.jsdroid.editor.*"
import "android.util.TypedValue"
import "java.io.File"
import "AndLua"
import "android.content.Context"

layout = {
  LinearLayout,
  orientation = "vertical",
  layout_width = "fill",
  layout_height = "fill",
  {
    CodePane,
    id = "edit",
    layout_width = "fill",
    layout_height = "wrap",
  },
  {
    LuaWebView, --浏览器控件
    id = "browser",
    layout_width = 'fill', --宽度
    layout_height = 'fill', --高度
  },
}

--activity.ActionBar.setTitle('AndroLua+')
activity.setTheme(R.Theme_Blue)
activity.setContentView(loadlayout(layout))
browser.setVisibility(View.INVISIBLE) -- 浏览器默认不显示


local edih = edit.getCodeText()
edih.setCursorColor(0xffec5f67) --设置光标颜色
edih.setLineNumberColor(0xff00a2ff) --行号颜色
edih.setLineNumberBackgroundColor(0x33DCDCDC) --每行背景颜色
edih.setLineNumberSplitColor(0xFFD1D1D1) --行号旁边那条线的颜色
edih.setSelectBackgroundColor(0x33ec5f67) --文本选中时的颜色
edih.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 15); --设置字体大小
edih.setSelection(#edih.getText())

-- 判断笔记是否为英语单词，如果是开启网络加载
function is_all_english(input_string)
  -- 匹配英文和空格的正则表达式
  local pattern = "[%a%s]+"
  -- 判断字符串是否完全匹配英文和空格
  if input_string:match(pattern) == input_string then
    return true
   else
    return false
  end
end

function check_lines(input_string)
  -- 以\n分割字符串，获取行数组
  local lines = {}
  local i = 1
  for line in string.gmatch(input_string, "([^\n]+)\n?") do
    lines[i] = line
    i = i + 1
  end

  -- 检查前两行是否至少有一行为"abc"
  if is_all_english(lines[1]) then
    return lines[1]
   elseif is_all_english(lines[2]) then
    return lines[2]
  end
  return false
end

--main函数为上个activity传入的参数

function main(name)
  if name:find(".txt") then
    edih.setText(io.open(name):read("*a"))
   else
    edih.setText(name)
    is_englis = check_lines(name)
    if is_englis then
      browser.setVisibility(View.VISIBLE) -- 显示浏览器
      browser.loadUrl("https://translator.ai.xiaomi.com/?text=" .. is_englis)
    end
  end
  --content=name
  activity.setTitle("查看笔记")
end

edih.addTextChangedListener {
  onTextChanged = function(所有内容, 开始输入位置, 删除了几个字符, 输入了几个字符)
    --print(所有内容)
  end
}
activity.getSystemService(Context.INPUT_METHOD_SERVICE).hideSoftInputFromWindow(edit.getWindowToken(), 0)
--[[
local m={
  {MenuItem,
    title="撤销",},
  {MenuItem,
    title="恢复",},
  {MenuItem,
    title="关于",}
}
function onCreateOptionsMenu(menu)
  loadmenu(menu,m,nil,2)
end

local edii=PreformEdit(edit.getCodeText());
function onOptionsItemSelected(item)
  local item = tostring(item)
  if item == "撤销" then
    if edii ~= nil then
      edii.undo()
    end
   elseif item == "重做" then
    if edii ~= nil then
      edii.redo()
    end
  end
end
]]
--[[lastclick=os.time()-2
function onKeyDown(e)
  local now=os.time()
  if e==4 then
    if now-lastclick>2 then

    end
  end
end
]]
function 复制文本(content_2)
  import "android.content.*"
  import "android.content.Context"
  activity.getSystemService(Context.CLIPBOARD_SERVICE).setText(content_2)
end

--[[
function onKeyDown(code,event)
  if code==4 then
    if edih.getText() == "" then
      AlertDialog.Builder(this)
      .setTitle("文件为空，继续保存？")
      --.setMessage("确认将覆盖原内容")
      .setPositiveButton("继续保存",{onClick=function(v)
          提示("已保存")
          activity.finish()
        end})
      .setNeutralButton("",nil)
      .setNegativeButton("取消",nil)
      .show()
     elseif edih.getText()~=content then
      AlertDialog.Builder(this)
      .setTitle("是否保存文件？")
      .setMessage(edih.getText())
      .setPositiveButton("继续保存",{onClick=function(v)
          提示("已保存")
          activity.finish()
        end})
      .setNeutralButton("",nil)
      .setNegativeButton("取消",nil)
      .show()
      复制文本(content)
      复制文本(edih.getText())
      return true
     else
      --提示(edih.getText()..content[1])
      activity.finish()
    end
    return true
  end
end]]
