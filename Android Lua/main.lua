--[[
弹幕笔记V1.998完整开源
作者：不旦枕戈，不留下QQ，但你可以通过下述文档留言，讨论区留言等联系我。
请遵循协议：你可以不经作者许可二次修改，分发软件。但你需要留下作者信息，同时，你的软件不得用于商业用途。
开源文档：https://blog.nrhs.eu.org/2022/11/12/BulletNote_open-source/
软件介绍：https://bullet.nrhs.eu.org/
讨论区及使用流程：https://support.qq.com/embed/phone/427645/team/
]]
require "import"
import "AndLua"
import "android.app.*"
import "android.os.*"
import "java.io.File"
import "android.widget.*"
import "android.view.*"
import "java.io.File"
import "android.content.Context"
import "android.media.MediaPlayer"
import "android.view.animation.*"
import "android.animation.ObjectAnimator"
import "android.provider.Settings"
import "android.graphics.drawable.GradientDrawable"
import "android.view.animation.ScaleAnimation"
import "android.view.animation.Animation$AnimationListener"
import "android.view.animation.Animation"
import "android.graphics.Typeface"
import "android.net.Uri"
import "android.content.Intent"
import "android.graphics.Color"
import "android.database.sqlite.*"
import "android.provider.Settings"
import "android.graphics.Typeface"
import "java.net.URLEncoder"
import "java.net.URLDecoder"
import "json"
import "com.baidu.mobstat.StatService"
import "android.graphics.*"
import "android.content.*"

import "android.app.PendingIntent"
import "android.app.NotificationManager"
import "android.os.Bundle"
import "java.lang.String"
import "java.lang.System"
import "android.app.NotificationChannel"

--布局文件引入
import "layout"
import "Mlayout/bullet_lay"
import "Mlayout/xfc"
import "Mlayout/xfq"
--部分lua文件
import "Mlua/paper"
import "Mlua/onclick_card"
import "Mlua/updata"
--import "Mlua/input"
activity.setTheme(R.Theme_Blue)
activity.setTitle("弹幕笔记")
activity.setContentView(loadlayout(layout))
activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
--部分开源作者信息
--由Nimoaix制作（QQ：1650656895）
--由XMSUI1二改(XMSUI1作者QQ：1650656895)
--XMSUI1由悬浮窗3d二改（悬浮窗3d作者QQ：1650656895)

--悬浮权限
function permission()
  if (Build.VERSION.SDK_INT >= 23 and not Settings.canDrawOverlays(this)) then
    return false
   elseif Build.VERSION.SDK_INT < 23 then
    return nil
   else
    return true
  end
end


function get_permission()
  if permission() ~= true then
    AlertDialog.Builder(this).setTitle("权限请求")
    .setMessage("弹幕运行需要显示在其他应用之上，需要悬浮窗权限！")
    .setPositiveButton("去授权",{onClick=function()
        提示("找到[桌面弹幕]并允许.")
        intent = Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION);
        intent.setData(Uri.parse("package:" .. activity.getPackageName()));
        activity.startActivityForResult(intent, 100);
      end})
    .show();
  end
end

参数=0
function onKeyDown(code,event)
  if string.find(tostring(event),"KEYCODE_BACK") ~= nil then
    if 参数+2 > tonumber(os.time()) then
      import "android.content.Intent"
      home=Intent(Intent.ACTION_MAIN);
      home.addCategory(Intent.CATEGORY_HOME);
      activity.startActivity(home);
     else
      Toast.makeText(activity,"将返回桌面，结束程序请点击『我的』." , Toast.LENGTH_SHORT )
      .show()
      参数=tonumber(os.time())
    end
    return true
  end
end

--百度统计服务
StatService()
.setAppKey("253a4d48a4")
.start(this)

import "Mlua/volume"--弹道相关配置文件

--sqlite数据库操作

db = SQLiteDatabase.openOrCreateDatabase(this.getLuaDir() .. "/bullet.db",MODE_PRIVATE, nil);

function raw(sql,text)
  cursor=db.rawQuery(sql,text)
end

function exec(sql)
  db.execSQL(sql);
end

--创建表
function create_config()
  local sql="create table config(name varchar primary key NOT NULL,value varchar ,bool boolean)"
  if pcall(exec,sql) then
   else
    print("创建配置表失败")
  end
end

--创建同步表
function create_sync()
  local sql="create table sync(id INTEGER PRIMARY KEY AUTOINCREMENT,name varchar NOT NULL,key varchar ,user_name varchar,sync_date datetime,count_record INT,bool boolean)"
  if pcall(exec,sql) then
   else
    print("创建同步表失败")
  end
end

local sql="select count(*) from sqlite_master where type='table' and name='config'"
if pcall(raw,sql,nil) then
  cursor.moveToFirst(); --移动指针
  local result = cursor.getLong(0);
  if result==0 then
    create_config()
    create_sync()
  end
 else
  print("数据库查询失败！")
end


intent = Intent()
mNManager = activity.getSystemService(Context.NOTIFICATION_SERVICE)
--pendingIntent = PendingIntent.getActivity(activity, 0, intent, 0)
if Build.VERSION.SDK_INT >= Build.VERSION_CODES.O
  builder= Notification.Builder(this,"5996773");
 else
  builder= Notification.Builder(this);
end


import "Mlua/fun"
import "Mlua/incident" --事件函数

--程序退出后再次执行会从main重新开始执行
--也就导致，退出后弹幕仍然在弹幕，但程序会重新执行
--无法准确确定开始卡片究竟是开启弹幕还是关闭弹幕
--理论上讲，判断activity状态才是优雅的办法，但我不会
--于是直接赋状态，弹幕会执行完当前一条后主动关闭。

if read_config("bullet_state") == true then
  sys_close()
end

--字体
function ttf(id)
  id.setTypeface(Typeface.createFromFile(File(activity.getLuaDir().."/res/Alimama_ShuHeiTi_Bold.ttf")))
end

--首页一言获取
function getcyan()
  Http.get("https://word.rainss.cn/api_users.php?userid=188&type=json&source=true",nil,"utf8",nil,function(code,content)
    if code==200 then
      local yl=json.decode(content).author
      --提示(yl)
      cyan_yy.setText(json.decode(content).txt.."  "..yl)
     else
      --cyan_yy.setText("向前走."..code)
    end
  end)
end

--基础状态确认
function main_fun_a()
  home_view()
  if home_view()~=0 then
    if #home_list_name==1 then
      cars_current_text.setText("当前["..home_list_name[1].."]卡片已上膛.")
     else
      cars_current_text.setText("当前["..home_list_name[1].."]等"..#home_list_name.."个卡片已上膛.")
    end
    book_cannot.setVisibility(View.GONE)
    cars_current_content_text.setText("共装载弹幕:"..home_view().."条")
   else
    --没有弹幕
    book_cannot.setVisibility(View.VISIBLE)
  end
end

--程序启动执行

main_fun_a()
getcyan()
reversal=false--反转动画控制状态
add_list_bullet()--开始获取词库列表
top_tip_linear.setVisibility(View.GONE) --隐藏悬浮窗顶部提示消息
hide_users.setVisibility(View.GONE) --隐藏我的页面信息卡片
隐藏标题栏()
user_name()
tool_summary.setVisibility(View.GONE)
tts_summary.setVisibility(View.GONE)
xfc_trans_en.setVisibility(View.GONE)

ttf(user_title)
--ttf(bullet_text3)
ttf(bullet_text2)
ttf(bullet_text)
ttf(start_bullet_tip)
ttf(标题)
ttf(add_table_text)
ttf(title_one)
ttf(title_two)
ttf(title_three)

--默认关闭悬浮球
Has_up_write_config("fab","","false")
Has_up_write_config("faw","","false")