// ? AppTipClock.cpp
// 提示时钟，支持与windows端Python通过MQTT通信，用于远程提示内容
// 已支持翻译主题，AI初级解释

#include "AppManager.h"
#include <A_Config.h>     //默认，加载WIFI头文件
#include "PubSubClient.h" //默认，加载MQTT库文件

// lbm 图标
static const uint8_t hua_img[] = {
    0x00,
    0xF0,
    0x0F,
    0x00,
    0x00,
    0xFC,
    0x3F,
    0x00,
    0x00,
    0x0F,
    0xF0,
    0x00,
    0xC0,
    0x03,
    0xC0,
    0x03,
    0xE0,
    0x00,
    0x00,
    0x07,
    0x70,
    0x00,
    0x00,
    0x0E,
    0x38,
    0x00,
    0x00,
    0x1C,
    0x18,
    0x42,
    0x12,
    0x18,
    0x0C,
    0xC6,
    0x7F,
    0x30,
    0x0E,
    0x0C,
    0x73,
    0x30,
    0x06,
    0x08,
    0x73,
    0x60,
    0x07,
    0x00,
    0x3E,
    0x60,
    0x03,
    0x00,
    0x1E,
    0x60,
    0x83,
    0x87,
    0xFF,
    0xC0,
    0x03,
    0xE6,
    0xC0,
    0xC1,
    0x03,
    0x04,
    0x0C,
    0xC0,
    0x03,
    0x04,
    0x2D,
    0xC0,
    0x03,
    0x84,
    0xFF,
    0xC0,
    0x03,
    0x04,
    0x0C,
    0xC0,
    0x03,
    0x04,
    0x0C,
    0xC0,
    0x07,
    0xDC,
    0xFF,
    0x60,
    0x06,
    0xDC,
    0xFF,
    0x60,
    0x0C,
    0x0E,
    0x0C,
    0x70,
    0x0C,
    0x04,
    0x0C,
    0x30,
    0x18,
    0x00,
    0x0C,
    0x18,
    0x38,
    0x00,
    0x00,
    0x1C,
    0x70,
    0x00,
    0x00,
    0x0E,
    0xE0,
    0x00,
    0x00,
    0x07,
    0xC0,
    0x03,
    0xC0,
    0x03,
    0x00,
    0x0F,
    0xF0,
    0x00,
    0x00,
    0xFE,
    0x7F,
    0x00,
    0x00,
    0xF0,
    0x0F,
    0x00,
};
// WiFi 图标
static const uint8_t wifiIcon[] = {
    0x00, 0x00, 0xf0, 0x0f, 0xfc, 0x3f, 0x1e, 0x78, 0x07, 0xe0, 0xe0, 0x07,
    0xf8, 0x1f, 0x38, 0x1c, 0x00, 0x00, 0x80, 0x01, 0xc0, 0x03, 0x80, 0x01,
    0x00, 0x00};
// 花的图标
static const uint8_t hua_bits[] = {
    0x00,
    0x00,
    0x00,
    0x00,
    0x00,
    0x00,
    0x00,
    0x00,
    0x00,
    0x80,
    0x01,
    0x00,
    0x00,
    0xC0,
    0x03,
    0x00,
    0x00,
    0x60,
    0x06,
    0x00,
    0x00,
    0x30,
    0x0E,
    0x00,
    0xE0,
    0x3B,
    0xDC,
    0x07,
    0xF0,
    0x1F,
    0xF8,
    0x0F,
    0x30,
    0x0C,
    0x30,
    0x0C,
    0x30,
    0x00,
    0x00,
    0x0C,
    0x30,
    0x00,
    0x00,
    0x0C,
    0x30,
    0x18,
    0x18,
    0x08,
    0x30,
    0x18,
    0x18,
    0x08,
    0x00,
    0x00,
    0x00,
    0x00,
    0x00,
    0x00,
    0x00,
    0x00,
    0x30,
    0xE0,
    0x07,
    0x0C,
    0x60,
    0xC0,
    0x03,
    0x06,
    0x60,
    0x00,
    0x00,
    0x06,
    0xC0,
    0x00,
    0x00,
    0x03,
    0xC0,
    0x01,
    0x80,
    0x03,
    0x80,
    0x03,
    0xC0,
    0x01,
    0x00,
    0x07,
    0xE0,
    0x00,
    0x00,
    0xBE,
    0x7F,
    0x00,
    0x40,
    0xF0,
    0x1F,
    0x00,
    0xE0,
    0x80,
    0x01,
    0x07,
    0xC0,
    0x81,
    0x81,
    0x03,
    0x00,
    0x87,
    0xE1,
    0x00,
    0x00,
    0x8E,
    0x71,
    0x00,
    0x00,
    0x9C,
    0x39,
    0x00,
    0x00,
    0x90,
    0x09,
    0x00,
    0x00,
    0x00,
    0x00,
    0x00,
    0x00,
    0x00,
    0x00,
    0x00,
};

//********************巴法云配置部分*******************//
#define ID_MQTT "bc9d4efe1faa4a57bd73996381cdef6f" // 用户私钥，控制台获取
const char *topic = "HuaClock";                    // 主题名字，可在巴法云控制台自行创建，名称随意
const char *mqtt_server = "bemfa.com";             // 默认，MQTT服务器
const int mqtt_server_port = 9501;                 // 默认，MQTT服务器
static int MQTT_MAX = 3;                           // 设置MQTT最大连接数
WiFiClient espClient;
PubSubClient client(espClient);

// 定义TipClock类
class TipClock : public AppBase
{
private:
  /* data */
public:
  TipClock()
  {
    name = "TipClock";
    title = "花say";
    description = "CTRLCCC翻译";
    image = hua_img;
    peripherals_requested = PERIPHERALS_SD_BIT;
  }
  void setup();
};
// 静态初始化
static TipClock app;
bool flag = true;
extern const char *dayOfWeek[];
static const menu_item settings_menu_TimeDown[] =
    {
        {NULL, "返回"},
        {NULL, "退出"},
        {NULL, "仅时钟"},
        {NULL, NULL},
};
int getNetwork()
{
  HTTPClient http;
  http.begin(String("https://www.baidu.com/")); // HTTP
  http.addHeader("Accept", "*/*");
  http.addHeader("User-Agent", "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.131 Safari/537.36");
  hal.autoConnectWiFi();
  Serial.println("开始检测网络");
  int httpCode = http.GET();
  if (httpCode == HTTP_CODE_OK)
  {
    return 0;
  }
  else
  {
    http.end();
    Serial.println("HTTP错误");
    return -1;
  }
  http.end();
  return -1;
}
// 提示消息展示
int displayTransData(String msg)
{
  buzzer.append(2000, 200); // 蜂鸣器200Hz，200ms
  // 指定最大长度2000字节
  DynamicJsonDocument data(2000);
  deserializeJson(data, msg); // Json 格式解析
  // UI绘制
  if (data["topic"] == "translate") // 主题判别
  {
    display.clearScreen();
    GUI::drawWindowsWithTitle("花的翻译", 0, 0, 296, 128);
    u8g2Fonts.setCursor(10, 30);
    u8g2Fonts.setFontMode(2);
    u8g2Fonts.print("师爷，翻译翻译什么叫:");
    u8g2Fonts.setCursor(10, 50);
    u8g2Fonts.setFontMode(1);
    u8g2Fonts.setFont(u8g2_font_ncenB12_tr); // 普通显示模式
    // u8g2Fonts.printf("划词：%s", data["word"].as<const char *>());
    u8g2Fonts.printf("%s", data["word"].as<const char *>());
    u8g2Fonts.setCursor(10, 70);
    u8g2Fonts.setFontMode(2);
    u8g2Fonts.setFont(u8g2_font_wqy12_t_gb2312); // 普通显示模式
    u8g2Fonts.printf("翻译：%s", data["translateWord"].as<const char *>());
    // Serial.println(data["word"].as<char *>());
    // Serial.println(data["translateWord"].as<char *>());
    // 刷新屏幕
    display.display();
    // 等待10秒后，返回主APP
    delay(10000);
    Serial.print("返回时钟.");
    return 0;
  }          // 其他主题处理
  return -1; // 错误判别
}

void DisplayTime(int connected) // only display time
{
  Serial.printf("\n 刷新指令:参数[%d]", connected);
  int w;
  char timeStr[6];                                                          // 时间字符串长度
  sprintf(timeStr, "%02d:%02d", hal.timeinfo.tm_hour, hal.timeinfo.tm_min); // 格式
  display.clearScreen();                                                    // 清屏
  u8g2Fonts.setFontMode(1);                                                 // 字体设置
  u8g2Fonts.setForegroundColor(0);                                          // 黑色，前景色
  u8g2Fonts.setBackgroundColor(1);                                          // 白色，背景色
  u8g2Fonts.setFont(u8g2_font_logisoso92_tn);                               // 点
  w = u8g2Fonts.getUTF8Width(timeStr);                                      // 获取字符串在屏幕上所需的宽度
  u8g2Fonts.setCursor((296 - w) / 2, 104);                                  // 位置控制
  u8g2Fonts.print(timeStr);                                                 // 显示
  u8g2Fonts.setFont(u8g2_font_wqy12_t_gb2312);                              // 字体族
  display.drawFastHLine(0, 110, 296, 0);                                    // 水平分割线
  u8g2Fonts.setCursor(10, 125);                                             // 位置
  if (connected == 0)                                                       // 判断是否连接到wifi并接入MQTT，服务上线
  {
    u8g2Fonts.printf("%02d月%02d日 星期%s 阿花已链接", hal.timeinfo.tm_mon + 1, hal.timeinfo.tm_mday, dayOfWeek[hal.timeinfo.tm_wday]);
    display.drawXBitmap(215, 112, hua_bits, 30, 16, 0);
    display.drawXBitmap(250, 114, wifiIcon, 16, 13, 0);
  }
  else if (connected == 1) // 连接到wifi但服务未上线
  {
    u8g2Fonts.printf("%02d月%02d日 星期%s 不旦枕戈", hal.timeinfo.tm_mon + 1, hal.timeinfo.tm_mday, dayOfWeek[hal.timeinfo.tm_wday]);
  }
  else
  {
    u8g2Fonts.printf("%02d月%02d日 星期%s 等待链接...", hal.timeinfo.tm_mon + 1, hal.timeinfo.tm_mday, dayOfWeek[hal.timeinfo.tm_wday]);
  }
  // 电池
  display.drawXBitmap(296 - 25, 113, getBatteryIcon(), 20, 16, 0);

  if (hal.timeinfo.tm_min % 20 == 0) // 局刷与全局刷新判断
  {
    display.display(false);
    force_full_update = false;
    // part_refresh_count = 0;
    Serial.printf("\n 全刷");
  }
  else
  {
    display.display(true);
    // part_refresh_count++;
    Serial.printf("\n 局刷");
  }
}
// MqttMsgDisplay() function
void MqttMsgDisplay(char *topic, byte *payload, unsigned int length)
{
  Serial.print("主题Topic:");
  Serial.println(topic);
  String msg = "";
  for (int i = 0; i < length; i++)
  {
    msg += (char)payload[i];
  }
  Serial.print("MQTT接收消息:");
  Serial.println(msg);
  if (displayTransData(msg) == 0)
  {
    Serial.print("回到时钟");
    DisplayTime(0); // 网络成功
  }
  msg = "";
}

int reconnect()
{
  // Loop until we're reconnected
  Serial.print("等待连接MQTT连接");
  // Attempt to connect
  if (client.connect(ID_MQTT))
  {
    Serial.println("连接成功");
    Serial.print("订阅:");
    Serial.println(topic);
    // 订阅主题，如果需要订阅多个主题，可发送多条订阅指令client.subscribe(topic2);client.subscribe(topic3);
    client.subscribe(topic);
    return 0; // 返回正常0
    flag = true;
  }
  else
  {
    if (MQTT_MAX > 0)
    {
      Serial.print("错误 failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
      MQTT_MAX--;
    }
  }
  GUI::msgbox_yn("花say:", "MQTT服务器连接异常.");
  return -1; // 返回异常1
}

void TipClock::setup()
{
  if (flag)
  {
    // DisplayTime(2); // 网络成功
    hal.autoConnectWiFi();
    DisplayTime(2); // 成功
    sleep(5);
    if (getNetwork == 0)
    {
      Serial.print("网络链接成功.");
      if (client.connected())
      {
        Serial.print("MQTT连接成功.");
        DisplayTime(1); // 网络成功
      }
      else
      {
        Serial.print("MQTT连接失败.");
        flag = false;
      }
    }
    else
    {
      Serial.print("网络连接失败.");
      flag = false;
    }
    Serial.begin(115200); // 设置波特率115200
    // 主逻辑开始
    client.setServer(mqtt_server, mqtt_server_port); // 设置mqtt服务器
    if (!client.connected())                         // 如果未连接到MQTT服务器，重新连接3次
    {
      Serial.print("异常重新连接MQTT.");
      DisplayTime(0);
      if (reconnect() == 0) // 连接MQTT服务器成功
      {
        Serial.print("异常重连成功");
        //  mqtt循环体
        int lasttime = hal.timeinfo.tm_min;
        while (1)
        {
          if (digitalRead(PIN_BUTTONC) == LOW)
          {
            if (GUI::waitLongPress(PIN_BUTTONC)) // 中间
            {
              // 菜单项目弹出
              int res = GUI::menu("What Are You Doing? 花:", settings_menu_TimeDown);
              switch (res)
              {
              case 0:
                return;
              case 1:
                appManager.goBack();
                return;
                break;
              case 2:
                // 仅时钟
                ESP.restart();
                return;
                break;
              default:
                break;
              }
            }
          }
          delay(1000);
          // 时间判断更新
          if (hal.timeinfo.tm_min != lasttime)
          {
            Serial.printf("刷新时间.Be:%02d:%02d", lasttime, hal.timeinfo.tm_min);
            DisplayTime(0); // 正常服务
            lasttime = hal.timeinfo.tm_min;
          }
          client.loop();
          client.setCallback(MqttMsgDisplay); // mqtt消息处理
        }
      }
      else
      {
        Serial.print("异常重连MQTT失败.");
        // 仅时钟模式
        DisplayTime(2); // 仅时钟
        flag = false;
      }
    }
    else // 首次连接成功
    {
      // 网络与服务正常建立
      Serial.print("服务开启.");
      DisplayTime(0); // 正常服务
      while (1)
      {
        client.loop();
        client.setCallback(MqttMsgDisplay); // mqtt消息处理
      }
    }
  }
  else
  {
    Serial.print("MQTT服务异常.");
    // 仅时钟模式
    DisplayTime(2); // 仅时钟
  }
  appManager.noDeepSleep = true; // 保持唤醒
}
