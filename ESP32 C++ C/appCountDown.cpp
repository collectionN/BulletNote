#include "AppManager.h"
#include "GUI.h"

static const uint8_t clock_bits[] = {
    0x00,
    0x00,
    0x00,
    0x00,
    0x00,
    0x00,
    0x00,
    0x00,
    0x00,
    0x00,
    0x00,
    0x00,
    0x00,
    0xFC,
    0x7F,
    0x00,
    0x00,
    0xFC,
    0x3F,
    0x00,
    0x00,
    0x80,
    0x03,
    0x00,
    0x00,
    0x80,
    0x03,
    0x00,
    0x00,
    0xF0,
    0x0F,
    0x00,
    0x00,
    0xFC,
    0x3F,
    0x00,
    0x00,
    0x1E,
    0x7C,
    0x00,
    0x00,
    0x07,
    0xE0,
    0x00,
    0x80,
    0x03,
    0xC0,
    0x01,
    0xC0,
    0x01,
    0x80,
    0x03,
    0xC0,
    0x80,
    0x01,
    0x03,
    0xE0,
    0x80,
    0x01,
    0x07,
    0x60,
    0x80,
    0x01,
    0x07,
    0x60,
    0x80,
    0x01,
    0x06,
    0x60,
    0x80,
    0x01,
    0x06,
    0x60,
    0x80,
    0x03,
    0x06,
    0x60,
    0x00,
    0x07,
    0x06,
    0x60,
    0x00,
    0x0E,
    0x06,
    0xE0,
    0x00,
    0x04,
    0x07,
    0x00,
    0x00,
    0x00,
    0x03,
    0x00,
    0x00,
    0x80,
    0x03,
    0x00,
    0x02,
    0xC0,
    0x01,
    0x00,
    0x07,
    0xE0,
    0x00,
    0x00,
    0x3E,
    0x7C,
    0x00,
    0x00,
    0xFC,
    0x3F,
    0x00,
    0x00,
    0xF0,
    0x0F,
    0x00,
    0x00,
    0x00,
    0x00,
    0x00,
    0x00,
    0x00,
    0x00,
    0x00,
    0x00,
    0x00,
    0x00,
    0x00,
};
class AppCountDown : public AppBase
{
private:
    /* data */
public:
    AppCountDown()
    {
        name = "CountDown";
        title = "倒计时";
        description = "倒计时";
        image = clock_bits;
    }
    void setup();
};
// 初始化设置
static AppCountDown app;
extern const char *dayOfWeek[];
int last_minutes = -1; // 用于记录上一次的分钟数
static const menu_item settings_menu_TimeDown[] =
    {
        {NULL, "返回"},
        {NULL, "退出"},
        {NULL, "重置"},
        {NULL, NULL},
};
void flush_screen(int hours, int minutes, int seconds)
{
    Serial.printf("\n刷新\t");
    int w;
    char timeStr[6];
    if (seconds != -1)
    {
        Serial.printf("\n跳秒： %d:%d", minutes, seconds);
        // sprintf(timeStr,"01:25");
        sprintf(timeStr, "%02d:%02d", minutes, seconds);
    }
    else
    {
        sprintf(timeStr, "%02d:%02d", hours, minutes);
        Serial.print("\n 跳分：");
    }
    display.clearScreen();                      // 清屏
    u8g2Fonts.setFontMode(1);                   // 字体设置
    u8g2Fonts.setForegroundColor(0);            // 黑色，前景色
    u8g2Fonts.setBackgroundColor(1);            // 白色，背景色
    u8g2Fonts.setFont(u8g2_font_logisoso92_tn); // 点
    w = u8g2Fonts.getUTF8Width(timeStr);
    u8g2Fonts.setCursor((296 - w) / 3, 104);     // 位置
    u8g2Fonts.print(timeStr);                    // 显示
    u8g2Fonts.setFont(u8g2_font_wqy12_t_gb2312); // 字体族
    display.drawFastHLine(0, 110, 296, 0);       // 水平分割线
    u8g2Fonts.setCursor(10, 125);                // 位置
    u8g2Fonts.printf("%02d月%02d日 星期%s 不旦枕戈的倒计时ing", hal.timeinfo.tm_mon + 1, hal.timeinfo.tm_mday, dayOfWeek[hal.timeinfo.tm_wday]);
    // 电池
    display.drawXBitmap(296 - 25, 111, getBatteryIcon(), 20, 16, 0);

    if ((minutes != last_minutes && minutes <= 10) || minutes == 20 || minutes == 40) // 局刷与全局刷新判断
    {
        display.display(false);
        force_full_update = false;
        // part_refresh_count = 0;
        Serial.printf("\n 全刷");
    }
    else
    {
        display.display(true);
        // part_refresh_count++;
        Serial.printf("\n 局刷");
    }
}
void flush_SetTime(int hours, int minutes, int seconds)
{
    Serial.printf("\n设置时间\t");
    int w;
    char timeStr[10];
    Serial.printf("\n设置时间： %d:%d:%d", hours, minutes, seconds);
    // sprintf(timeStr,"01:25");
    sprintf(timeStr, "%02d:%02d:%02d", hours, minutes, seconds);
    display.clearScreen();                      // 清屏
    u8g2Fonts.setFontMode(1);                   // 字体设置
    u8g2Fonts.setForegroundColor(0);            // 黑色，前景色
    u8g2Fonts.setBackgroundColor(1);            // 白色，背景色
    u8g2Fonts.setFont(u8g2_font_logisoso62_tn); // 点
    w = u8g2Fonts.getUTF8Width(timeStr);
    Serial.printf("\n w=%d", w);
    u8g2Fonts.setCursor((296 - w) / 3, 104);     // 位置
    u8g2Fonts.print(timeStr);                    // 显示
    u8g2Fonts.setFont(u8g2_font_wqy12_t_gb2312); // 字体族
    display.drawFastHLine(0, 110, 296, 0);       // 水平分割线
    u8g2Fonts.setCursor(10, 125);                // 位置
    u8g2Fonts.printf("左减 右加 中确认，长按加速 (x_x)");
    // 电池
    display.drawXBitmap(296 - 25, 111, getBatteryIcon(), 20, 16, 0);

    display.display(true);
    Serial.printf("\n 局刷");
}
void countdown(int hours, int minutes, int seconds)
{
    Serial.printf("\n倒计时结束");
    while (hours >= 0 && minutes >= 0)
    {
        if (digitalRead(PIN_BUTTONC) == LOW)
        {
            if (GUI::waitLongPress(PIN_BUTTONC)) // 中间
            {
                // 菜单项目弹出
                int res = GUI::menu("What Are You Doing? 花:", settings_menu_TimeDown);
                switch (res)
                {
                case 0:
                    return;
                case 1:
                    appManager.goBack();
                    return;
                    break;
                case 2:
                    // 重置时间
                    return;
                    break;
                default:
                    break;
                }
            }
        }
        Serial.printf("\r倒计时循环开始");
        { // 倒计时结束
            if (hours * 60 + minutes >= 10 || seconds == -1)
            { // 剩余时间判断
                if (minutes != last_minutes)
                { // 当分钟数变化时
                    Serial.printf("/r 剩余 Time: %02d:%02d", hours, minutes);
                    flush_screen(hours, minutes, -1);
                }
                // else
                // {
                //     Serial.printf("/n 大于10分钟不刷新秒");
                // }
            }
            else
            {
                Serial.printf("\r 剩余 Time: %02d:%02d:%02d", hours, minutes, seconds);
                flush_screen(hours, minutes, seconds);
            }
            last_minutes = minutes; // 更新上一次的分钟数
                                    // fflush(stdout); // 刷新屏幕
        }
        Serial.printf("\n倒计时 -1");
        seconds--;
        if (seconds < 0)
        {
            seconds = 59;
            minutes--;
        }
        if (minutes < 0)
        {
            minutes = 59;
            hours--;
        }
        delay(800);
    }
    Serial.printf("\n倒计时结束");
}
void AppCountDown::setup()
{
    // 定义小时，分钟，秒
    int hours = 00;
    int minutes = 00;
    int seconds = 10;
    int localTime = 0; // ! 定位时间设置位置
    // 循环校验设置时间，其中，向左为-1，右为+1，中间确认
    // ! 需要注意的是，小时区域最好大于3
    // ? 长按为加速设置，中间为菜单项目
    flush_SetTime(hours, minutes, seconds);
    while (1)
    {
        if (digitalRead(PIN_BUTTONL) == LOW) // 低电平
        {
            // 减
            if (GUI::waitLongPress(PIN_BUTTONL))
            {
                // 长按快速增加
                if (localTime == 0)
                { // 设置小时、
                    Serial.printf("\n Set: hours");
                    if (hours >= 1)
                    {
                        hours--;
                        flush_SetTime(hours, minutes, seconds);
                    }
                    else
                    {
                        hours = 3;
                        flush_SetTime(hours, minutes, seconds);
                    }
                }
                else if (localTime == 1)
                { // 设置分钟
                    Serial.printf("\n Set: mintues");
                    if (minutes >= 10) // 步进10
                    {
                        minutes -= 10;
                        flush_SetTime(hours, minutes, seconds);
                    }
                    else
                    {
                        minutes = 59;
                        flush_SetTime(hours, minutes, seconds);
                    }
                }
                else if (localTime == 2)
                { // 设置秒数
                    Serial.printf("\n Set: seconds");
                    if (seconds >= 10) // 步进10
                    {
                        seconds -= 10;
                        flush_SetTime(hours, minutes, seconds);
                    }
                    else
                    {
                        seconds = 59;
                        flush_SetTime(hours, minutes, seconds);
                    }
                }
            }
            else
            {
                // ? 步进-1
                if (localTime == 0)
                { // 设置小时
                    Serial.printf("\n Set: hours");
                    if (hours >= 1)
                    {
                        hours--;
                        flush_SetTime(hours, minutes, seconds);
                    }
                    else
                    {
                        hours = 3;
                        flush_SetTime(hours, minutes, seconds);
                    }
                }
                else if (localTime == 1)
                { // 设置分钟
                    Serial.printf("\n Set: mintues");
                    if (minutes >= 1) // 步进1
                    {
                        minutes--;
                        flush_SetTime(hours, minutes, seconds);
                    }
                    else
                    {
                        minutes = 59;
                        flush_SetTime(hours, minutes, seconds);
                    }
                }
                else if (localTime == 2)
                { // 设置秒数
                    Serial.printf("\n Set: seconds");
                    if (seconds >= 1) // 步进1
                    {
                        seconds--;
                        flush_SetTime(hours, minutes, seconds);
                    }
                    else
                    {
                        seconds = 59;
                        flush_SetTime(hours, minutes, seconds);
                    }
                }
            }
        }
        else if (digitalRead(PIN_BUTTONR) == LOW) // 右
        {
            // 加
            if (GUI::waitLongPress(PIN_BUTTONR))
            {
                // 长按快速增加
                if (localTime == 0)
                { // 设置小时
                    Serial.printf("\n Set: hours");

                    if (hours < 3)
                    {
                        hours++;
                        flush_SetTime(hours, minutes, seconds);
                    }
                    else
                    {
                        hours = 0;
                        flush_SetTime(hours, minutes, seconds);
                    }
                }
                else if (localTime == 1)
                { // 设置分钟
                    Serial.printf("\n Set: mintues");
                    if (minutes <= 49) // 步进10
                    {
                        minutes += 10;
                        flush_SetTime(hours, minutes, seconds);
                    }
                    else
                    {
                        minutes = 0;
                        flush_SetTime(hours, minutes, seconds);
                    }
                }
                else if (localTime == 2)
                { // 设置秒数
                    Serial.printf("\n Set: seconds");
                    if (seconds <= 49) // 步进10
                    {
                        seconds += 10;
                        flush_SetTime(hours, minutes, seconds);
                    }
                    else
                    {
                        seconds = 0;
                        flush_SetTime(hours, minutes, seconds);
                    }
                }
            }
            else
            {
                if (localTime == 0)
                { // 设置小时
                    Serial.printf("\n Set: hours");
                    if (hours < 3)
                    {
                        hours++;
                        flush_SetTime(hours, minutes, seconds);
                    }
                    else
                    {
                        hours = 0;
                        flush_SetTime(hours, minutes, seconds);
                    }
                }
                else if (localTime == 1)
                { // 设置分钟
                    Serial.printf("\n Set: mintues");
                    if (minutes < 59) // 步进1
                    {
                        minutes++;
                        flush_SetTime(hours, minutes, seconds);
                    }
                    else
                    {
                        minutes = 0;
                        flush_SetTime(hours, minutes, seconds);
                    }
                }
                else if (localTime == 2)
                { // 设置秒数
                    Serial.printf("\n Set: seconds");
                    if (seconds < 59) // 步进1
                    {
                        seconds++;
                        flush_SetTime(hours, minutes, seconds);
                    }
                    else
                    {
                        seconds = 0;
                        flush_SetTime(hours, minutes, seconds);
                    }
                }
            }
        }
        else if (digitalRead(PIN_BUTTONC) == LOW)
        {
            if (GUI::waitLongPress(PIN_BUTTONC)) // 中间
            {
                // 菜单项目弹出
                // 菜单项目弹出
                int res = GUI::menu("花阿花阿花?:", settings_menu_TimeDown);
                switch (res)
                {
                case 0:
                    return;
                case 1:
                    appManager.goBack();
                    return;
                    break;
                case 2:
                    // 重置时间
                    hours = 00;
                    minutes = 00;
                    seconds = 00;
                    localTime = 0;
                    flush_SetTime(hours, minutes, seconds);
                default:
                    break;
                }
            }
            else
            {
                // 确认
                if (localTime == 2)
                {
                    // 开始倒计时
                    Serial.printf("\n 设置完成,倒计时开始.");
                    break;
                }
                else
                {
                    Serial.printf("\n 已确认.");
                    localTime++; // 时间位前进
                }
            }
        }
    }
    // 倒计时循环
    countdown(hours, minutes, seconds);
    Serial.printf("\n程序结束");
    appManager.noDeepSleep = true; // 保持唤醒
}