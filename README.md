# 弹幕笔记

![](https://pic.imgdb.cn/item/663d8b510ea9cb1403ffcd61.gif)

#### 介绍

弹幕笔记 APP 是一款多功能的笔记工具，适用于 Android 平台。

它能够读取笔记记录，并在屏幕上以弹幕形式展示笔记内容，以提醒用户进行记忆复习。同时，它支持悬浮窗功能，用户可以在任何应用界面添加笔记和翻译选定内容，而不会影响手机的其他操作。 

我们还使用 Python 开发了一个 exe 应用程序，用户可以在 Windows 上任意界面使用鼠标划词后按下Ctrl+三次 c，即可翻译选定内容。自动区分查词与翻译，中英文自动切换。同时支持同步到云端或者花的终端设备中。

为了进一步扩展产品的应用场景，我们还设计了一款桌面摆件。设计电路、打样PCB，驱动墨水屏幕和 `ESP32`，我们实现了与其他设备的连接。通过 `MQTT` 物联网协议，其他设备能够将笔记同步至摆件当中。为用户提供更加便捷、新奇的复习体验。

此外，为了更好地管理多平台与笔记内容，我们设计开发了 `Web` 管理平台， 使用 `php` 和 `Vue`，数据库等技术。用户可以便捷登录平台，对笔记进行增删改查 等操作，实现笔记的云端管理。便捷的将三方设备连接起来

#### 软件架构

弹幕笔记APP基于Lua脚本

花的翻译基于Python相关库

花的伴侣基于嘉立创开源[LiClock](https://oshwhub.com/lxu0423/ESP32-duo-gong-neng-mo-shui-ping)项目

花的管理基于Vue+PHP+ElementUI

#### 快速开始

1.  Lua开源文件可直接在AndLua+等支持Lua编译器上运行，也有release版本
2.  花的翻译直接解压到任意文件夹，添加到开始菜单即可
3.  花的伴侣请DIY相关硬件，按照LiClock开源固件编译
4.  花的管理可访问 [预览平台](https://bullet.nrhs.eu.org/)

#### 使用说明

> 弹幕笔记

- V1.998不支持弹幕同步，V2.0结构已发生变化。从1.998升级到2.0将丢失数据，请先导出笔记

- V2.0 目前仅支持单向同步，由PC划词->同步管理->弹幕笔记APP

> 花的翻译

- 仅支持windows10及以上版本
- 解压ZIP到任意路径，添加到开机目录即可。
- 请在管理系统中创建笔记本生成KEY填写在`KEY`文件中
- 需要同步到[伴侣终端]请下载目录下py文件携带[巴法云MQTT](https://cloud.bemfa.com/)Key与C语言文件重新编译
- 同步笔记仅同步单词划定，不支持长句同步

> 伴侣终端

- 请[点击这里](https://oshwhub.com/lxu0423/ESP32-duo-gong-neng-mo-shui-ping)查看开源详情 开源@小李实验室
- 关于MQTT通信协议在[巴法云MQTT](https://cloud.bemfa.com/)中参阅
- 系统固件与ESP32驱动相关参阅[GitHub](https://github.com/diylxy/LiClock)

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

#### 致谢相关

弹幕笔记官网：https://bullet.nrhs.eu.org/ 

弹幕笔记 开源Bolg: https://txc.qq.com/dashboard/team/index 

弹幕笔记下载中心： https://pan.nrhs.eu.org/K%E7%9A%84%E8%89%BA%E6%9C%AF/Resource/DeskBullet

备用下载：https://nrhs.lanzoul.com/b0fa9n2xi 提取码:bullet

弹幕笔记开源 V1.998 (2022)： https://blog.nrhs.eu.org/2022/11/12/BulletNote_open-source/ 

LiClock 嘉立创开源：https://oshwhub.com/lxu0423/ESP32-duo-gong-neng-mo-shui-ping 

LiClock 固件及驱动系统：https://github.com/diylxy/LiClock

#### 效果预览

[![](https://pic.imgdb.cn/item/663d88250ea9cb1403fbf37a.png)](javascript:;)

更多`demo`图片参照`master`分支下`display`预览图
