/*
 Navicat Premium Data Transfer

 Source Server         : ZhouJAli
 Source Server Type    : MySQL
 Source Server Version : 80026
 Source Host           : 39.101.75.136:3306
 Source Schema         : HuaClock

 Target Server Type    : MySQL
 Target Server Version : 80026
 File Encoding         : 65001

 Date: 09/05/2024 13:47:56
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for Users
-- ----------------------------
DROP TABLE IF EXISTS `Users`;
CREATE TABLE `Users`  (
  `ID` int NOT NULL AUTO_INCREMENT,
  `name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `age` int NULL DEFAULT NULL,
  `addrss` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `links` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `user_key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1041 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of Users
-- ----------------------------
INSERT INTO `Users` VALUES (1001, '不旦枕戈', 20, '重庆', '17365281111', 'bcb15f821479b4d5772bd0ca866c00ad5f926e3580720659cc80d39c9d09802a');
INSERT INTO `Users` VALUES (1007, '小花', 55, '重庆市', '19355199167', 'c031696555b41a6a891e6be455e5b494641264208c6289efd92d0e2803d2b705');
INSERT INTO `Users` VALUES (1016, '最后的测试', 80, '重庆市', '12121212144', '0e1b2580dc7ca00fe36ed3c999b0a3a075246cff8d77cd2947b7f65de8dda98a');
INSERT INTO `Users` VALUES (1018, 'zz', 76, '不知道', '19936303655', '92a0b7056184e3121b1d3defe3b9bb05c5b4f57bf23ee8e5e5f9fb0bdc91a0be');
INSERT INTO `Users` VALUES (1019, 'zz', 76, '不知道', '19936303654', 'be351d4bbe09eb20b901f8591e3f4530d585b4c2f0ba711930f77f0978c757e8');
INSERT INTO `Users` VALUES (1020, 'hua', 54, '重庆市', '11111111111', '05c428e837227ab242f0048c20f1a4d55b698066b0d95b8156feff850d3275b9');
INSERT INTO `Users` VALUES (1021, '逻辑', 80, '重庆市', '17367676879', '033be856c96818562eaa27a93f486d6598066e056f410252ca2216d0a84303de');
INSERT INTO `Users` VALUES (1022, '花的', 80, '重庆市', '17356451567', 'c6ce610ba36bdc2dfcd8b71b091bfdc3c2a294c1fc96fed1fc53bdd5598a991b');
INSERT INTO `Users` VALUES (1024, 'hua', 22, 'cq', '2983717613@qq.com', '544e11940fc52457a7b0d945c144aae4b1a759c9ed89863e53b21ed625be9217');
INSERT INTO `Users` VALUES (1025, 'huahua', 80, 'china', '2983723413@qq.com', '2ba031d722f8658f590bb22085043642369b877c2ad3526c05ddacab9439f304');
INSERT INTO `Users` VALUES (1026, 'LOGIC', 23, 'CQ', '3483717613@qq.com', '56b30fd55cc6b34903044090b86feb5fe6e1c6a58397795160460c20e09c9184');
INSERT INTO `Users` VALUES (1027, 'hua', 0, '', '3232717613@qq.com', '78681d706fcfc652b5e6d23b8a2a9fa420fc5a09487c2319697acc5d97e780bc');
INSERT INTO `Users` VALUES (1028, 'hj', 0, 'cq', '1233217613@qq.com', 'b4152b6772b18ffa38bc0ede6694bfdbaef695c0851b40ba0f64e6789e043abf');
INSERT INTO `Users` VALUES (1029, 'hj', 0, '', '3213717613@qq.com', 'd8e09f39695da3fd31e42c116fbef2b113c53afe85fd424efa6e167c9a9a6200');
INSERT INTO `Users` VALUES (1030, 'huj', 0, '', '6683717613@qq.com', '42bef5e687bcee1b5e79db205e54563220eb77a9da3ebe0e619ab9e78ccfbc96');
INSERT INTO `Users` VALUES (1031, '花的', 0, '重庆', '0083717613@qq.com', '0341b1061bbeebfd82309123044191a630605f1b5060f1f4ca168050f410bec5');
INSERT INTO `Users` VALUES (1036, '不是彭思瑶', 0, '', '4444717613', 'bcb15f821479b4d5772bd0ca866c00ad5f926e3580720659cc80d39c9d09802a');
INSERT INTO `Users` VALUES (1037, '阿花', 0, '', '17365282888', 'bcb15f821479b4d5772bd0ca866c00ad5f926e3580720659cc80d39c9d09802a');
INSERT INTO `Users` VALUES (1038, 'hua', 0, '', '12121212121', 'bcb15f821479b4d5772bd0ca866c00ad5f926e3580720659cc80d39c9d09802a');
INSERT INTO `Users` VALUES (1039, 'hua', 0, '', '8983717613@qq.com', 'bcb15f821479b4d5772bd0ca866c00ad5f926e3580720659cc80d39c9d09802a');
INSERT INTO `Users` VALUES (1040, '源', 0, '', '3063272867@qq.com', '4ff3b5e93434de336c0f59292685345ffe4432180654743d2aad7d212a1867df');

-- ----------------------------
-- Table structure for listNote
-- ----------------------------
DROP TABLE IF EXISTS `listNote`;
CREATE TABLE `listNote`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` varchar(4) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `note_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `note_key` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `create_date` date NULL DEFAULT NULL,
  `abstract` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `state` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1038 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of listNote
-- ----------------------------
INSERT INTO `listNote` VALUES (1001, '1001', '四级混淆词', 'K_44444444', '2024-04-29', '收录一部分易混淆的四级词汇', 'normal');
INSERT INTO `listNote` VALUES (1002, '1001', '考研易淆词', 'K_SASASASA', '2024-04-29', NULL, 'normal');
INSERT INTO `listNote` VALUES (1003, '1001', '易混高数公式', 'K_66666666', '2024-04-29', NULL, 'default');
INSERT INTO `listNote` VALUES (1004, '1036', '阿瑶不想学', 'K_YYBXXXXX', '2024-04-30', NULL, 'normal');
INSERT INTO `listNote` VALUES (1034, '1001', '阿瑶', 'K_ABCDEFGH', NULL, '阿瑶啊', 'normal');
INSERT INTO `listNote` VALUES (1035, '1001', '打算', 'K_$Z0Y0VO@', NULL, '', 'normal');
INSERT INTO `listNote` VALUES (1037, '1040', 'lls', 'K_7D13F2E2', NULL, '', 'normal');

-- ----------------------------
-- Table structure for translate
-- ----------------------------
DROP TABLE IF EXISTS `translate`;
CREATE TABLE `translate`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `choseWord` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `transWord` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `create_date` datetime NULL DEFAULT NULL,
  `NoteKey` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 135 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of translate
-- ----------------------------
INSERT INTO `translate` VALUES (71, 'abandon', '放弃', '2024-05-09 09:25:54', 'K_44444444');
INSERT INTO `translate` VALUES (73, 'abundance', '丰富', NULL, 'K_44444444');
INSERT INTO `translate` VALUES (74, 'about', '关于', NULL, 'K_44444444');
INSERT INTO `translate` VALUES (75, 'above', '在...之上', NULL, 'K_44444444');
INSERT INTO `translate` VALUES (76, 'abroad', '在国外', NULL, 'K_44444444');
INSERT INTO `translate` VALUES (77, 'absolute', '绝对的', NULL, 'K_44444444');
INSERT INTO `translate` VALUES (78, 'abundant', '大量的', NULL, 'K_44444444');
INSERT INTO `translate` VALUES (79, 'aboveboard', '公正的', NULL, 'K_44444444');
INSERT INTO `translate` VALUES (80, 'abrasive', '磨蚀性的', NULL, 'K_44444444');
INSERT INTO `translate` VALUES (81, 'abridge', '简化', NULL, 'K_44444444');
INSERT INTO `translate` VALUES (82, 'abrupt', '突然的', NULL, 'K_44444444');
INSERT INTO `translate` VALUES (83, 'absurd', '荒谬的', NULL, 'K_44444444');
INSERT INTO `translate` VALUES (84, 'abysmal', '深不可测的', NULL, 'K_44444444');
INSERT INTO `translate` VALUES (85, 'academic', '学术的', NULL, 'K_44444444');
INSERT INTO `translate` VALUES (86, 'accelerate', '加速', NULL, 'K_44444444');
INSERT INTO `translate` VALUES (87, 'accent', '口音', NULL, 'K_44444444');
INSERT INTO `translate` VALUES (88, 'acclaimed', '受到赞誉的', NULL, 'K_44444444');
INSERT INTO `translate` VALUES (89, 'accommodate', '适应', NULL, 'K_44444444');
INSERT INTO `translate` VALUES (90, 'accomplish', '完成', NULL, 'K_44444444');
INSERT INTO `translate` VALUES (91, 'accord', '一致', NULL, 'K_44444444');
INSERT INTO `translate` VALUES (92, 'account', '账户', NULL, 'K_44444444');
INSERT INTO `translate` VALUES (93, 'accrue', '累积', NULL, 'K_44444444');
INSERT INTO `translate` VALUES (94, 'accurate', '准确的', NULL, 'K_44444444');
INSERT INTO `translate` VALUES (95, 'accused', '被控告的', NULL, 'K_44444444');
INSERT INTO `translate` VALUES (96, 'achieve', '实现', NULL, 'K_44444444');
INSERT INTO `translate` VALUES (97, 'acquire', '获得', NULL, 'K_44444444');
INSERT INTO `translate` VALUES (99, 'actor', '演员', NULL, 'K_44444444');
INSERT INTO `translate` VALUES (100, 'adapt', '适应', NULL, 'K_44444444');
INSERT INTO `translate` VALUES (101, 'addict', '上瘾的', NULL, 'K_44444444');
INSERT INTO `translate` VALUES (102, 'addition', '增加', NULL, 'K_44444444');
INSERT INTO `translate` VALUES (103, 'admit', '承认', NULL, 'K_44444444');
INSERT INTO `translate` VALUES (104, 'admiral', '海军上将', NULL, 'K_44444444');
INSERT INTO `translate` VALUES (105, 'adobe', ' Adobe公司', NULL, 'K_44444444');
INSERT INTO `translate` VALUES (106, 'advance', '前进', NULL, 'K_44444444');
INSERT INTO `translate` VALUES (107, 'advice', '建议', NULL, 'K_44444444');
INSERT INTO `translate` VALUES (108, 'advisor', '顾问', NULL, 'K_44444444');
INSERT INTO `translate` VALUES (109, 'aerobic', '有氧的', NULL, 'K_44444444');
INSERT INTO `translate` VALUES (110, 'aerosol', '气溶胶', NULL, 'K_44444444');
INSERT INTO `translate` VALUES (111, 'affair', '事务', NULL, 'K_44444444');
INSERT INTO `translate` VALUES (112, 'affiliate', '附属机构', NULL, 'K_44444444');
INSERT INTO `translate` VALUES (113, 'affirm', '肯定', NULL, 'K_44444444');
INSERT INTO `translate` VALUES (114, 'afraid', '害怕的', NULL, 'K_44444444');
INSERT INTO `translate` VALUES (115, 'python', 'n. 蚺，巨蟒', '2024-05-07 09:28:56', 'K_YYBXXXXX');
INSERT INTO `translate` VALUES (116, 'translate', 'v. 译，翻译', '2024-05-07 09:30:04', 'K_YYBXXXXX');
INSERT INTO `translate` VALUES (117, 'comprehension', '理解力', '2024-05-07 15:27:06', 'K_YYBXXXXX');
INSERT INTO `translate` VALUES (118, 'read', 'v. 阅读，读懂', '2024-05-07 15:27:06', 'K_YYBXXXXX');
INSERT INTO `translate` VALUES (119, 'File', 'n. 文件夹（或箱、柜等）', '2024-05-07 18:45:59', 'K_YYBXXXXX');
INSERT INTO `translate` VALUES (120, 'install', 'v. 安装，设置', '2024-05-07 19:00:50', 'K_YYBXXXXX');
INSERT INTO `translate` VALUES (121, '花的', 'floral', '2024-05-07 19:02:28', 'K_YYBXXXXX');
INSERT INTO `translate` VALUES (122, 'more_floder', '更多驱动器', '2024-05-07 19:03:27', 'K_YYBXXXXX');
INSERT INTO `translate` VALUES (124, 'Content', 'adj. 满足的，满意的，甘愿的', '2024-05-07 19:03:27', 'K_YYBXXXXX');
INSERT INTO `translate` VALUES (125, 'duration', 'n. 持续，持续时间', '2024-05-07 19:07:00', 'K_YYBXXXXX');
INSERT INTO `translate` VALUES (126, 'decode', 'vt. 破译，译解（密码信息）', '2024-05-08 00:01:54', 'K_YYBXXXXX');
INSERT INTO `translate` VALUES (127, 'data', 'n. 数据，资料', '2024-05-08 00:01:54', 'K_YYBXXXXX');
INSERT INTO `translate` VALUES (128, 'Design', 'n. （Design）（巴、印、俄）德赛（人名）', '2024-05-08 07:27:20', 'K_YYBXXXXX');
INSERT INTO `translate` VALUES (129, 'Hua', 'n．华：一个常见的中文姓氏，也可以用作人名的一部分。', '2024-05-08 07:27:20', 'K_YYBXXXXX');
INSERT INTO `translate` VALUES (130, 'Expiration', 'n. 到期，（一段时间的）结束', '2024-05-08 07:27:20', 'K_YYBXXXXX');
INSERT INTO `translate` VALUES (131, 'CVC', 'abbr. 电流-电压特性（Current -Voltage Characteristic）', '2024-05-08 07:27:20', 'K_YYBXXXXX');
INSERT INTO `translate` VALUES (132, 'reissue', 'v. 重新发行', '2024-05-08 07:27:20', 'K_YYBXXXXX');
INSERT INTO `translate` VALUES (133, 'bora', 'n. [气象]布拉风（吹袭亚德里亚海沿岸的季节性东北冷风）', '2024-05-08 07:27:20', 'K_YYBXXXXX');
INSERT INTO `translate` VALUES (134, 'failed', 'adj. 失败的', '2024-05-08 07:27:20', 'K_YYBXXXXX');

SET FOREIGN_KEY_CHECKS = 1;
