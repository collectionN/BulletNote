import hashlib
import os
import random
import re
import time
from win11toast import toast
import keyboard
import pyperclip
import requests
import json
from hashlib import md5
import urllib.parse
from datetime import datetime

# 翻译函数

# mysql连接池

# mysql function
now = datetime.now()
date_format = "%Y-%m-%d %H:%M:%S"
KEY = ""
ACCOUNT = ""
PASSWD = ""
TOPIC = ""
UID = ""

# Set your own appid/appkey.
appid = '20210518000831618'
appkey = 'lkcA8tGx27kwMTJDOAgK'


def make_md5(s, encoding='utf-8'):
    return md5(s.encode(encoding)).hexdigest()


def SHA_256(data):
    # 创建SHA256哈希对象
    sha256_hash = hashlib.sha256()

    # 对数据进行加密
    sha256_hash.update(data.encode('utf-8'))

    # 获取加密后的哈希值（十六进制格式）
    encrypted_data = sha256_hash.hexdigest()

    return encrypted_data


def is_chinese(text):
    chinese_pattern = re.compile(r'[\u4e00-\u9fa5]')
    if chinese_pattern.findall(text):
        return True
    else:
        return False


def postMsg(uid, topic, word, translateWord):
    # 构造payload_data字典
    payload_data = {
        "uid": uid,
        "topic": topic,
        "type": 1,
        "msg": f"{{\"topic\": \"translate\", \"word\": \"{word}\", \"translateWord\": \"{translateWord}\"}}"
    }

    url = "https://apis.bemfa.com/va/postmsg"
    headers = {
        'Accept': '*/*',
        'Host': 'apis.bemfa.com',
        'Connection': 'keep-alive',
        'Content-Type': 'application/json'  # 将Content-Type更改为application/json
    }

    response = requests.post(url, headers=headers, data=json.dumps(payload_data))  # 直接将payload_data转换为json字符串

    if response.status_code == 200:
        if json.loads(response.text)["code"] == 0:
            print("成功")
            return True
        else:
            print(response.text)
    else:
        print(f"请求失败，状态码：{response.status_code}")


def send_post_request(account, passwd, choseword, transword, key):
    # URL-encode the input fields
    encoded_account = urllib.parse.quote(account)
    encoded_passwd = urllib.parse.quote(passwd)
    encoded_choseword = urllib.parse.quote(choseword)
    encoded_transword = urllib.parse.quote(transword)
    encoded_key = urllib.parse.quote(key)

    # Construct the payload with URL-encoded fields
    payload = f"account={encoded_account}&passwd={encoded_passwd}&choseword={encoded_choseword}&transword={encoded_transword}&Key={encoded_key}"

    # print(payload)
    # print(f"{account},{passwd},{choseword},{transword},{key}")

    # Define the headers for the request
    headers = {
        'User-Agent': 'Apifox/1.0.0 (https://apifox.com)',
        'Accept': '*/*',
        'Host': 'bullet.nrhs.eu.org',
        'Connection': 'keep-alive',
        'Content-Type': 'application/x-www-form-urlencoded'
    }

    # Send the POST request
    response = requests.request("POST", "http://bullet.nrhs.eu.org/device/API.php", headers=headers, data=payload)

    # Return the response text
    code = json.loads(response.text)["code"]
    return code


def translate_baidu(query):
    print("使用百度翻译")
    endpoint = 'http://api.fanyi.baidu.com'
    path = '/api/trans/vip/translate'
    url = endpoint + path

    salt = random.randint(32768, 65536)
    sign = make_md5(appid + query + str(salt) + appkey)

    # Build request
    headers = {'Content-Type': 'application/x-www-form-urlencoded'}
    if is_chinese(query):
        # 选定中文
        # For list of language codes, please refer to `https://api.fanyi.baidu.com/doc/21`
        to_lang = 'en'
    else:
        to_lang = 'zh'
    payload = {'appid': appid, 'q': query, 'from': 'auto', 'to': to_lang, 'salt': salt, 'sign': sign}

    # Send request
    r = requests.post(url, params=payload, headers=headers)
    result = r.json()

    # Show response
    content = json.dumps(result, indent=4, ensure_ascii=False)
    print(content)
    dst_combined = ""
    for item in json.loads(content)["trans_result"]:
        dst_combined += item['dst'] + "\n"
    return dst_combined


def translate(word):
    print("翻译开始")
    content = []
    if len(word) <= 10:
        # 单词翻译
        url = "https://dict.youdao.com/suggest?num=0&ver=3.0&doctype=json&cache=false&le=en&q="
        try:
            response = requests.get(url + str(word), timeout=10)  # Set a timeout for the request
            response.raise_for_status()  # Raise an exception if the HTTP request returned an unsuccessful status code
            data = json.loads(response.text)
            # print(response.text)
            # Check the internal API result code
            if data['result']['code'] == 200:
                # If successful, process the first explanation
                first_explain = data['data'].get('entries', [{}])[0].get('explain', '')
                context = first_explain.replace('; ', '\n')
                content.insert(0, 0)
                content.insert(1, context.replace(';', '\n').replace('；', '\n'))
                return content
            elif data['result']['code'] == 404:
                # 未找到词义 sand
                return 0
            else:
                # 处理失败情况
                return -1

        # 处理异常抛出
        except requests.exceptions.RequestException as e:  # Network-related exceptions
            print(f"Network error: {e}")
        except json.JSONDecodeError as e:  # JSON decoding exceptions
            print(f"JSON decoding error: {e}")
        except Exception as e:  # Catch-all for any other exceptions
            print(f"An error occurred: {e}")
        return -1  # Return -1 for all exception cases

    elif len(word) <= 300:
        print("选词使用百度翻译")
        context = translate_baidu(word)
        content.insert(0, 1)  # 判断位
        content.insert(1, context)
        # 语句翻译
        return content
    else:
        # 跳转翻译
        return 1


class KeyboardListener:
    def __init__(self):
        self.t = 0
        self.c = 0
        self.callback = None

    def set_callback(self, callback):
        self.callback = callback

    def listen_keyboard(self):
        keyboard.hook(self.onKeyEvent)
        keyboard.wait()

    def onKeyEvent(self, key):
        if key.event_type == "down" and key.name.lower() == "c" and self.isCtrlHolding():
            if self.t == 0:
                self.t = time.time()
                self.c += 1
                print("wait for next c", self.c)
                return

            if (time.time() - self.t < 0.5):
                self.t = time.time()
                self.c += 1
                print("wait for next c:", self.c)

            else:
                self.c = 0
                self.t = 0
                print("wait for next c", self.c)

            if self.c >= 3:
                self.c = 0
                self.t = 0
                print("获取剪切板内容")
                toast(f"花的翻译", f"正在翻译...", audio=file_audio)
                if self.callback:
                    self.callback()
                # 获取并打印剪贴板内容
                clipboard_content = pyperclip.paste()

                if clipboard_content:
                    choseWord = clipboard_content.strip()
                    transContent = translate(choseWord)
                    print(transContent)
                    if transContent[1] == -1:
                        toast("不旦枕戈Tell You", "网络嘎了.", icon=file_path)
                    elif transContent[1] == 0:
                        toast(f"划词:{choseWord}", "没找到词义...", icon=file_path)
                    elif transContent[1] == 1:
                        toast(f"跳转翻译", "字符长度过长，点击跳转网站翻译.", on_click=f'https://fanyi.youdao.com/',
                              icon=file_path)
                    else:
                        if transContent[0] == 0:
                            toast(f" {choseWord}", f"🎃:{transContent[1]}", icon=file_path,
                                  on_click=f'https://dict.youdao.com/result?word={choseWord}&lang=en',
                                  audio=file_audio)  # , buttons=buttons,audio=file_audio)
                            if KEY != '' and len(KEY) == 10 and PASSWD != '' and ACCOUNT != '':
                                # 同步到笔记本
                                response = send_post_request(
                                    account=ACCOUNT,
                                    passwd=SHA_256(PASSWD),
                                    choseword=choseWord,
                                    transword=re.split('[;；\n]', transContent[1])[0],
                                    key=KEY
                                )
                                if response == 200:
                                    print(f"数据已同步到:[{KEY}]")
                                else:
                                    print(response)
                                    toast("同步异常", f"错误码:{response}", icon=file_path)
                            if TOPIC != '' and UID != "":
                                print("开始同步到终端")
                                if postMsg(UID, TOPIC, choseWord, re.split('[;；\n]', transContent[1])[0]) is not True:
                                    toast("终端同步异常", f"请检查uid与topic", icon=file_path)
                        else:  # 段落翻译
                            toast("句子翻译", f"🎈:{choseWord[0:40]}...\n🎃:{transContent[1]}", duration='long')
                else:
                    toast("不支持的类型", "只支持划定字符串文本哦", icon=file_path)

    def isCtrlHolding(self):
        return keyboard.is_pressed('ctrl') or keyboard.is_pressed('left ctrl') or keyboard.is_pressed('right ctrl')


# 定义一个回调函数来处理检测到连续三次Ctrl+C的情况
def my_callback():
    print("CTRL+CCC......")


# 获取当前工作目录
current_directory = os.getcwd()

# 构建完整的文件路径
file_path = os.path.join(current_directory, "tx.ico")
file_KEY = os.path.join(current_directory, "KEY")
file_MQTT = os.path.join(current_directory, "MQTT")
file_audio = os.path.join(current_directory, "mla.wav")

# 检查当前目录下是否存在 tx.ico 文件
exists = os.path.exists(file_path)
KEY_file = os.path.exists(file_KEY)
MQTT_file = os.path.exists(file_MQTT)

# 打印当前运行目录
print("当前运行目录:", file_KEY)
if KEY_file:
    with open(file_KEY, 'r') as file:
        # 读取第一行
        ACCOUNT = file.readline().rstrip()
        # 读取第二行
        PASSWD = file.readline().rstrip()
        # 读取第三行
        KEY = file.readline().rstrip()
    print(f"同步账户:{ACCOUNT},KEY:{KEY}")
else:
    print("无同步KEY")

if MQTT_file:
    with open(file_MQTT, 'r') as file:
        # 读取第一行
        UID = file.readline().rstrip()
        # 读取第二行
        TOPIC = file.readline().rstrip()
    print(f"UID:{KEY},Topic:{TOPIC}")
else:
    print("无同步到终端")

# 如果不存在，则下载指定的 ICO 文件
if not exists:
    try:
        # 下载 ICO 文件
        print("下载图标")
        headers = {
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.7',
            'Accept-Language': 'en,zh-CN;q=0.9,zh;q=0.8',
            'Cache-Control': 'no-cache',
            'Connection': 'keep-alive',
            'Pragma': 'no-cache',
            'Upgrade-Insecure-Requests': '1',
            'User-Agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Mobile Safari/537.36',
        }

        response = requests.get('http://nrhs.eu.org/bdzg.ico', headers=headers, verify=False)
        with open(file_path, "wb") as f:
            f.write(response.content)
    except requests.exceptions.ConnectionError as e:
        print("连接错误：", e)

print(f"File '{file_path}' exists: {exists}")
toast("不旦枕戈", "花的翻译已启动...\n划词Ctrl+CCC将触发翻译功能", icon=file_path)

# 创建键盘监听器对象
keyboard_listener = KeyboardListener()

# 设置回调函数
keyboard_listener.set_callback(my_callback)

# 开始监听键盘事件
keyboard_listener.listen_keyboard()
